﻿using PostSharp.Aspects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayM8.Logging.PostSharp.FlowLogger
{    
    [Serializable]
    public class IndentedFlowLogger : OnMethodBoundaryAspect
    {
        #region Fields
        private static Dictionary<int, int> _threadFlowCount = new Dictionary<int, int>();
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileToSaveTo">The path\fileName to save to, each thread of execution will modify this basic file name</param>
        public IndentedFlowLogger(string fileToSaveTo)
        {
            FileToSaveTo = fileToSaveTo;
        }
        #endregion

        #region Properties
        public string FileToSaveTo { get; private set; }
        #endregion

        #region Public Functions
        public override void OnEntry(MethodExecutionArgs args)
        {
            if (_threadFlowCount.ContainsKey(GetCurrentThreadId()) == false)
            {
                _threadFlowCount.Add(GetCurrentThreadId(), 0);
            }
            else
            {
                _threadFlowCount[GetCurrentThreadId()] = _threadFlowCount[GetCurrentThreadId()] + 1;
            }


            string data = "".PadLeft(_threadFlowCount[GetCurrentThreadId()], '\t') + "-->" + args.Method.Name;

            string argValues = string.Empty;

            foreach(object arg in args.Arguments)
            {
                if(arg == null)
                {
                    argValues += "NULL";
                }
                else if(arg.GetType().IsValueType)
                {
                    argValues += arg.ToString();
                }
                else 
                {
                    argValues += "<" + arg.ToString() + ">";
                }
                argValues += ", ";
            }

            if(argValues.Length>0)
            {
                argValues = argValues.Substring(0, argValues.Length - 2);
            }

            if(System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(CreateFilePath(FileToSaveTo)))==false)
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(CreateFilePath(FileToSaveTo)));
            }

            System.IO.File.AppendAllText(CreateFilePath(FileToSaveTo), System.DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + ": " + data + "(" + argValues + ")\r\n");
            

        }

        public override void OnException(MethodExecutionArgs args)
        {

            if (System.IO.File.Exists(CreateFilePath(FileToSaveTo)) == true)
            {
                string data = "".PadLeft(_threadFlowCount[GetCurrentThreadId()], '\t') + "!!!START EXCEPTION!!!! \r\n" + args.Exception.ToString() + "".PadLeft(_threadFlowCount[GetCurrentThreadId()], '\t') + "!!!END EXCEPTION!!!! \r\n";
                System.IO.File.AppendAllText(CreateFilePath(FileToSaveTo), System.DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + ": " + data + "\r\n");
            }

            base.OnException(args);
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            if (System.IO.File.Exists(CreateFilePath(FileToSaveTo)) == true)
            {
                string data = "".PadLeft(_threadFlowCount[GetCurrentThreadId()], '\t') + "<--" + args.Method.Name;
                System.IO.File.AppendAllText(CreateFilePath(FileToSaveTo), System.DateTime.Now.ToString("yyyyMMdd HH:mm:ss") + ": " + data + "\r\n");                
            }
            _threadFlowCount[GetCurrentThreadId()] = _threadFlowCount[GetCurrentThreadId()] - 1;
            base.OnExit(args);
        }
        #endregion

        #region Private Functions
        private string CreateFilePath(string fileToSaveTo)
        {

            string fileNameToWriteTo = System.IO.Path.GetFileNameWithoutExtension(fileToSaveTo);
            fileNameToWriteTo += "_" + GetCurrentThreadId().ToString();

            return System.IO.Path.Combine(System.IO.Path.GetDirectoryName(fileToSaveTo), fileNameToWriteTo + System.IO.Path.GetExtension(fileToSaveTo));
        }
        private int GetCurrentThreadId()
        {
            return System.Threading.Thread.CurrentThread.ManagedThreadId;
        }
        #endregion
    }    
}
