﻿DESCRIPTION
===========
Creates a method name indented log of all the called methods to a selected class library.

Example:
    A class library called MyLibrary has a public function called StartReading() and numerous private functions relating to reading a book.

	public StartReading(string bookName)
	private OpenBook(string bookName);
	        ReadPage(int pageNumber);
			TurnPage();

A resulting log file may look something like this:
20140210 11:04:02: -->StartReading(<Lord of the Flies>)
20140210 11:04:02:   -->OpenBook(<Lord of the Flies>)
20140210 11:04:02:     -->ReadPage(1)
20140210 11:05:15:     <--ReadPage()
...
...
...
20140210 11:18:16:     -->TurnPage()
20140210 11:18:17:     <--TurnPage()
20140210 11:18:17:     -->ReadPage(137)
20140210 11:19:45:     <--ReadPage()
20140210 11:19:45:     -->TurnPage()
20140210 11:19:45:     <--TurnPage()
20140210 11:19:46:     -->ReadPage(138)
20140210 11:20:50:     <--ReadPage()
20140210 11:20:50:   <--OpenBook
20140210 11:20:51: <--StartReading

USAGE:
To log all the methods in the class library add the following attribute to the AssemblyInfo.cs file of the target class library:
[assembly: PayM8.Logging.PostSharp.FlowLogger.IndentedFlowLogger("c:\\Logs\\Logfile.txt")]

Make sure you also added a reference to this logging class library as well as to PostSharp.
When you no longer want to log your class library then you will need to remove the line of code from the AssemblyInfo.cs file, as well as the references added.