﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Diagnostics;
//using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
//using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
//using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
//using System.Collections.Specialized;

//namespace Dimension15.Services.Logging.WcfService
//{
//    [ConfigurationElementType(typeof(CustomFormatterData))]
//    public class LoggingFormatter : ILogFormatter
//    {
//        #region Fields
//        private Dictionary<string, object> _extendedProperties = new Dictionary<string, object>();
//        #endregion

//        public LoggingFormatter(NameValueCollection attributes)
//        {
//            string uri = attributes["uri"].ToString();
//            _extendedProperties.Add("uri", uri);
//        }

//        public string Format(Microsoft.Practices.EnterpriseLibrary.Logging.LogEntry log)
//        {            
//            log.ExtendedProperties = _extendedProperties;
//            return string.Empty;
//        }
//    }
//}