﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Dimension15.Services.Logging.WcfService
{
    /// <summary>
    /// Log data using Logger service
    /// </summary>
    /// <author name="Dean Komen" date="2012/01/20" />
    public class Logger
    {
        #region Fields
        private readonly static object _lock = new object();
        private static bool _allowTraceLogging = true;
        private Dictionary<int, System.Timers.Timer> _watchdogPinger = new Dictionary<int, System.Timers.Timer>();
        private static object _watchdogThreadLocker = "WatchdogThreadLocker";
        #endregion

        #region Properties
        /// <summary>
        /// Enable logging toggle
        /// </summary>
        /// <author name="Dean Komen"date="=">"2012/01/20 />
        ///  </author>
        public static bool AllowTraceLogging
        {
            get
            {
                return _allowTraceLogging;
            }
            set
            {
                _allowTraceLogging = value;
            }
        }
        #endregion

        #region Information Logging
        public static void TraceInformation(string text, int systemId, string traceId, int offset=0)
        {
            Write(text, "", 0, 0, System.Diagnostics.TraceEventType.Information, "", systemId, traceId, offset);
        }
        public static void TraceInformation(string text, int systemId, int offset = 0)
        {
            TraceInformation(text, systemId, string.Empty, offset);
        }
        #endregion

        public static void TraceWatchdog(int systemId)
        {
            string message = Dimension15.Services.Logging.Events.Handlers.WatchDogMessages.GetIsRunningMessageForSystem(systemId);
            if (string.IsNullOrEmpty(message))
            {
                TraceError("Could not find message to indicate service is running", systemId, "Service status unknown");
            }
            else
            {
                TraceInformation(message, systemId, "Service is up");
            }
        }

        #region Warning Logging
        public static void TraceWarning(string text, int systemId, string traceId, int offset = 0)
        {
            Write(text, "", 0, 0, System.Diagnostics.TraceEventType.Warning, "", systemId, traceId, offset);
        }
        public static void TraceWarning(string text, int systemId, int offset = 0)
        {
            TraceWarning(text, systemId, string.Empty, offset);
        }  
        #endregion

        #region Error Logging
        public static void TraceError(string text, int systemId, string traceId, int offset = 0)
        {
            Write(text, "", 0, 0, System.Diagnostics.TraceEventType.Error, "", systemId, traceId, offset);
        }
        public static void TraceError(string text, int systemId, int offset = 0)
        {
            TraceError(text, systemId, string.Empty, offset);
        }  
        #endregion

        private static Dimension15.Services.Logging.Enumerations.LogSeverity MapSeverity(System.Diagnostics.TraceEventType severity)
        {
            return (Dimension15.Services.Logging.Enumerations.LogSeverity)Enum.Parse(typeof(Dimension15.Services.Logging.Enumerations.LogSeverity), severity.ToString());
        }

        /// <summary>
        /// Call this to set up the automatic watchdog reporting system for services
        /// </summary>        
        /// <param name="systemId">The systemId of the system to monitor</param>
        /// <author name="Dean Komen"/>
        public void WatchDogNotificationController(bool activate, int systemId)
        {
            if (activate)
            {
                if (_watchdogPinger.ContainsKey(systemId) == false)
                {
                    System.Timers.Timer newTimer = new System.Timers.Timer((int)((float)Dimension15.Services.Logging.Server.WatchdogTimerInterval * 0.75));
                    newTimer.Elapsed += new System.Timers.ElapsedEventHandler(watchDogTimer_Elapsed);                    
                    _watchdogPinger.Add(systemId, newTimer);
                    watchDogTimer_Elapsed(newTimer, null);                    
                    newTimer.Start();
                }
            }
            else
            {
                if (_watchdogPinger.ContainsKey(systemId))
                {
                    System.Timers.Timer timer;
                    _watchdogPinger.TryGetValue(systemId, out timer);
                    timer.Stop();
                    _watchdogPinger.Remove(systemId);
                }
            }
        }

                
         /// <summary>
        /// Will notify the logging service that the indicated service is up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void watchDogTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (_watchdogThreadLocker)
            {
                //Try get the SystemId associated with this timer
                int? associatedSystemId = null;
                foreach (KeyValuePair<int, System.Timers.Timer> pair in _watchdogPinger)
                {
                    if (pair.Value == (System.Timers.Timer)sender)
                    {
                        associatedSystemId = pair.Key;
                        break;
                    }
                }
                if (associatedSystemId.HasValue)
                {
                    TraceWatchdog(associatedSystemId.Value);
                }
                else
                {
                    TraceError("Could not find watchdog key associated timer in collection (not yet added?)", (int)Services.Logging.Enumerations.SystemID.Dimension15Logging);
                }
            }
        }

        public static void Write(Dimension15.Services.Logging.LogEntry logEntry, int stackOffset)
        {
            if (AllowTraceLogging)
            {
                try
                {
                    Dimension15.Services.Logging.WcfService.ILoggingService service = Dimension15.Services.Logging.WcfService.ServiceClient.GetServiceClient(new System.ServiceModel.EndpointAddress(Dimension15.Settings.Registry.LocalEnvironmentConfiguration.GetLoggingServiceEndPoint()));                    
                    service.Add(logEntry);
                }
                catch (System.ServiceModel.EndpointNotFoundException exx)
                {
                    // Logger service is not listening on expected endpoint... so we cant send this log!
                    // Start the logger service!
                    // ... okay so we do no alternate type of log now either... we just forget it.. sorta as if it never happened.
                    // ...
                    // ...
                    // ...
                    // Hey? What happened?
                }
                catch (Exception ex)
                {
                    //Ouch.. something bad happened, so log the log to log via the logger to a different log! ;o)
                    System.IO.File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location.Substring(0, 1) + ":\\Logs\\FailedLog_" + logEntry.SystemID.ToString() + ".dat", logEntry.DateStamp.ToString() + ": TraceId: " + logEntry.TraceID.ToString() + ": Text: " + logEntry.Message + System.Environment.NewLine);
                    System.IO.File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location.Substring(0, 1) + ":\\Logs\\FailedLog_" + logEntry.SystemID.ToString() + ".dat", logEntry.DateStamp.ToString() + ": TraceId: " + logEntry.TraceID.ToString() + ": Logging exception: " + ex.ToString() + System.Environment.NewLine);
                    if (ex.InnerException != null)
                    {
                        System.IO.File.AppendAllText(System.Reflection.Assembly.GetExecutingAssembly().Location.Substring(0, 1) + ":\\Logs\\FailedLog_" + logEntry.SystemID.ToString() + ".dat", logEntry.DateStamp.ToString() + ": TraceId: " + logEntry.TraceID.ToString() + ": Logging INNER exception: " + ex.ToString() + System.Environment.NewLine);
                    }

                }
            }
        }

        private static void Write(string text, string category, int priority, int eventid, System.Diagnostics.TraceEventType type, string title, int systemId, string traceId, int stackOffset)
        {
            System.DateTime dateTime = System.DateTime.Now;
            Dimension15.Services.Logging.LogEntry logEntry = Dimension15.Services.Logging.LogEntry.CreateLogEntry((Dimension15.Services.Logging.Enumerations.LogSeverity)Enum.Parse(typeof(Dimension15.Services.Logging.Enumerations.LogSeverity), type.ToString(), true), text, traceId, systemId, stackOffset);
            logEntry.DateStamp = DateTime.Now;
            logEntry.ThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
            Write(logEntry, 0);
        }
    }
}
