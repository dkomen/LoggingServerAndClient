﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Dimension15.Services.Logging.WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILoggingService" in both code and config file together.
    [ServiceContract]
    public interface ILoggingService
    {
        [OperationContract]
        void Add(LogEntry logEntry);

        [OperationContract]
        List<LogEntry> RetrieveLogEntries(RetrievalFilter filter, Dimension15.Services.Logging.Enumerations.PersistanceDataStore dataStore);

        [OperationContract]
        void PrepopulateFromBackingStore(RetrievalFilter filter);

        [OperationContract]
        bool PingPong();

        [OperationContract]
        List<Dimension15.Services.Logging.LogPersistance.SystemId> GetAllSystemIds();
    }
}
