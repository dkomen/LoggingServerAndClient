﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Dimension15.Services.Logging.Enumerations;
using Dimension15.Services.Logging.Interfaces;

namespace Dimension15.Services.Logging.WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LoggingService" in code, svc and config file together.
    public class LoggingService : ILoggingService
    {
        private static Dimension15.Services.Logging.Server _server = Server.getInstance();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logEntry"></param>
        /// <author name='Dean Komen' date='2012/03/29' />
        public void Add(LogEntry logEntry)
        {
            _server.Add(logEntry);
        }

        public List<LogEntry> RetrieveLogEntries(RetrievalFilter filter, PersistanceDataStore dataStore)
        {
            List<ILogEntry> results = _server.RetrieveLogEntries(filter, dataStore);
            return results.ConvertAll(t => (LogEntry)t);
        }

        public void PrepopulateFromBackingStore(RetrievalFilter filter)
        {
            try
            {
                _server.PrepopulateFromBackingStore(filter);
            }
            catch(Exception ex)
            {
                Console.WriteLine("ERROR TO CONSOLE: " + ex.ToString());
                throw ex;
            }
        }

        public List<Dimension15.Services.Logging.LogPersistance.SystemId> GetAllSystemIds()
        {
            try
            {
                return _server.GetAllSystemIds();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR TO CONSOLE: " + ex.ToString());
                throw ex;
            }
        }

        public bool PingPong()
        {
            return true;
        }
    }
}
