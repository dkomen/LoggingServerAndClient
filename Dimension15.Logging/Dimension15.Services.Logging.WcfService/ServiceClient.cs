﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace Dimension15.Services.Logging.WcfService
{
    /// <summary>
    /// Allows for the creation of and maintains an open connection to a remote WCF serveice
    /// </summary>
    /// <author name="Dean Komen" date="12 April 2012" />
    public class ServiceClient : IDisposable
    {
        #region Fields
        private static ILoggingService _singletonInstance;
        private static object _threadLocker = "ThreadLocker";
        private static ChannelFactory<ILoggingService> _channelFactory;
        private bool _disposed = false;
        private static System.Timers.Timer _serviceIsUpCheck = new System.Timers.Timer(5000);
        #endregion

        #region Constructors
        //Hide default constructor
        private ServiceClient()
        { }

        /// <summary>
        /// Start checking if communication with server is up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void _serviceIsUpCheck_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _serviceIsUpCheck.Stop();
            try
            {
                _singletonInstance.PingPong();
                ClientCanCommunicate = true;
            }
            catch
            {
                ClientCanCommunicate = false;
            }
            finally 
            {
                _serviceIsUpCheck.Start();
            }
        }
        ~ServiceClient()
        {            
            Dispose(false);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Indicates that the client can actually communicate with the server
        /// </summary>
        public static bool ClientCanCommunicate {get;set;}
        #endregion

        #region Public Functions
        public static ILoggingService GetServiceClient(EndpointAddress endpointAddress)
        {
            return GetServiceClient(endpointAddress, false);
        }
        /// <summary>
        /// Dynamically creates a WCF connection to a remote service
        /// </summary>
        /// <param name="endpointAddress"></param>
        /// <param name="refreshUrlTarget">Will force creation of a new instance of the binding</param>
        /// <returns></returns>
        /// <author name="Dean Komen" date="12 April 2012" />
        public static ILoggingService GetServiceClient(EndpointAddress endpointAddress, bool refreshUrlTarget)
        {            
            if (refreshUrlTarget && _channelFactory != null && _channelFactory.State == CommunicationState.Opened)
            {
                _channelFactory.Close();
                _singletonInstance = null;
                _serviceIsUpCheck.Stop();
            }

            if (_singletonInstance == null || _channelFactory.State!=CommunicationState.Opened)
            {
                lock (_threadLocker)
                {
                    if (endpointAddress.Uri.AbsoluteUri.ToLower().StartsWith("net.tcp"))
                    {
                        #region Duplicate code - AAA!
                        NetTcpBinding newTcpBinding = new NetTcpBinding();
                        newTcpBinding.MaxReceivedMessageSize = 2147483647;
                        newTcpBinding.MaxBufferPoolSize = 2147483647;
                        newTcpBinding.MaxReceivedMessageSize = 2147483647;
                        newTcpBinding.ReceiveTimeout = new TimeSpan(0, 0, 45); //45seconds
                        newTcpBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
                        newTcpBinding.ReaderQuotas.MaxArrayLength = 2147483647;
                        newTcpBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
                        newTcpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
                        if (endpointAddress.Uri.AbsoluteUri.Contains("127.0.0.1"))
                        {
                            newTcpBinding.OpenTimeout = new TimeSpan(0, 0, 0, 0, 350);
                        }
                        else
                        {
                            newTcpBinding.OpenTimeout = new TimeSpan(0, 0, 0, 0, 500);
                        }
                        _channelFactory = new ChannelFactory<ILoggingService>(newTcpBinding, endpointAddress);
                        #endregion
                    }
                    else
                    {
                        #region Duplicate code - AAA!
                        BasicHttpBinding newBasicHttpBinding = new BasicHttpBinding();
                        newBasicHttpBinding.MaxReceivedMessageSize = 2147483647;
                        newBasicHttpBinding.MaxBufferPoolSize = 2147483647;
                        newBasicHttpBinding.MaxReceivedMessageSize = 2147483647;
                        newBasicHttpBinding.ReceiveTimeout = new TimeSpan(0, 0, 45); //45seconds
                        newBasicHttpBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
                        newBasicHttpBinding.ReaderQuotas.MaxArrayLength = 2147483647;
                        newBasicHttpBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
                        newBasicHttpBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
                        if (endpointAddress.Uri.AbsoluteUri.Contains("127.0.0.1"))
                        {
                            newBasicHttpBinding.OpenTimeout = new TimeSpan(0, 0, 0, 0, 350);
                        }
                        else
                        {
                            newBasicHttpBinding.OpenTimeout = new TimeSpan(0, 0, 0, 0, 500);
                        }
                        _channelFactory = new ChannelFactory<ILoggingService>(newBasicHttpBinding, endpointAddress);
                        #endregion
                    }

                    try
                    {
                        _singletonInstance = _channelFactory.CreateChannel(endpointAddress);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Could not connect to remote server!", ex);
                    }

                    _serviceIsUpCheck.Elapsed += new System.Timers.ElapsedEventHandler(_serviceIsUpCheck_Elapsed);
                    _serviceIsUpCheck.Start();

                }
            }            
            return _singletonInstance;
        }
        /// <summary>
        /// Destroys the connection to a remote service
        /// </summary>
        /// <author name="Dean Komen" date="12 April 2012" />
        public void CloseServiceClient()
        {
            Dispose();            
        }
        /// <summary>
        /// Destroys the connection to a remote service
        /// </summary>
        /// <author name="Dean Komen" date="12 April 2012" />
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if(disposing)
                {
                    lock (_threadLocker)
                    {
                        _channelFactory.Close();
                        _singletonInstance = null;
                    }
                }
            }
        }
        #endregion
    }
}