﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dimension15.Services.Logging;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceProcess;

namespace Dimension15.Logging.Service.Windows
{
    public class WindowsService : ServiceBase
    {

        #region Fields
        private bool _stop = false;
        private Dimension15.Services.Logging.WcfService.LoggingService _loggingService = null;
        #endregion


        public void Start(string[] args)
        {
            OnStart(args);

        }

        protected override void OnStart(string[] args)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(StartListening));

        }

        protected override void OnStop()
        {
            _stop = true;
            base.OnStop();
        }

        #region Private Functions
        private void StartListening(object param)
        {
            try
            {
                Console.ForegroundColor = ConsoleColor.Gray;

                Uri baseHttpAddress = new Uri(Dimension15.Settings.Registry.LocalEnvironmentConfiguration.GetLoggingServiceEndPoint());
                Uri baseTcpAddress = new Uri(Dimension15.Settings.Registry.LocalEnvironmentConfiguration.GetLoggingServiceEndPoint().Replace("http", "net.tcp").Replace(":" + baseHttpAddress.Port.ToString(), ":" + (baseHttpAddress.Port + 1).ToString()));
                ServiceHost host = null;
                try
                {
                    host = new ServiceHost(typeof(Dimension15.Services.Logging.WcfService.LoggingService), baseHttpAddress, baseTcpAddress);
                    BasicHttpBinding basicHttpBinding = new BasicHttpBinding();
                    NetTcpBinding netTcpBinding = new NetTcpBinding();
                    host.AddServiceEndpoint(typeof(Dimension15.Services.Logging.WcfService.ILoggingService), basicHttpBinding, baseHttpAddress);
                    host.AddServiceEndpoint(typeof(Dimension15.Services.Logging.WcfService.ILoggingService), netTcpBinding, baseTcpAddress);
                    ((ServiceBehaviorAttribute)host.Description.Behaviors[0]).IncludeExceptionDetailInFaults = true;

                    ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                    smb.HttpGetEnabled = true;
                    smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                    host.Description.Behaviors.Add(smb);

                    Console.WriteLine("---------------------------------------------------------");
                    Console.WriteLine("Start service listening on http and tcp channels with uri's: " + baseHttpAddress.ToString() + " and " + baseTcpAddress.ToString());

                    host.Open();

                    Console.WriteLine("Service is up and running");
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error starting service: " + ex.ToString());
                    if (host != null)
                    {
                        host.Close();
                    }
                    throw;
                }


                Dimension15.Services.Logging.WcfService.ILoggingService service = null;
                string traceId = string.Empty;
                try
                {
                    Console.WriteLine("Now play some ping-pong with service");
                    traceId = "GetServiceClient";
                    service = Dimension15.Services.Logging.WcfService.ServiceClient.GetServiceClient(new EndpointAddress(baseHttpAddress));
                    traceId = "pingpong";
                    service.PingPong();
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Could not play ping-pong with service!, traceID:" + traceId + ": " + ex.ToString());
                    throw;
                }
                try
                {
                    Console.WriteLine("Now preload some log data into local log cache (Retrieve up to 1.5days worth logs from the most recent log file)");
                    service.PrepopulateFromBackingStore(new RetrievalFilter() { StartDate = DateTime.Now.AddDays(-1.5), EndDate = null });
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error on preloading some log data: " + ex.ToString());
                    if (ex.InnerException != null)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("");
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Inner exception: " + ex.InnerException.ToString());
                    }
                    throw;
                }
                Console.WriteLine("Up and running!");
                Console.WriteLine("---------------------------------------------------------");

                //PayM8.Logging.Service.SubscriberService.Service.Start("net.tcp://127.0.0.1:5052/LoggingServiceSubscriber");

                do
                {
                    System.Threading.Thread.Sleep(250);
                } while (_stop == false);
                host.Close();
                Dimension15.Logging.Service.SubscriberService.Service.Stop();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("" + ex.ToString());
                Console.WriteLine("" + ex.ToString());
                Console.WriteLine("" + ex.ToString());
                Console.WriteLine("" + ex.ToString());
                Console.WriteLine("=================================================================================" + ex.ToString());
                Console.WriteLine("Error! " + ex.ToString());
                if (ex.InnerException != null)
                {
                    Console.WriteLine("");
                    Console.WriteLine("");
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Inner exception: " + ex.InnerException.ToString());
                }
                Console.ReadLine();
            }
        }
        #endregion

    }
}
