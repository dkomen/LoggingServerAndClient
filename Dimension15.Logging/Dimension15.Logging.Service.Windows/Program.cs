﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;

namespace Dimension15.Logging.Service.Windows
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (Environment.UserInteractive)
            {
                WindowsService service = new WindowsService();
                service.Start(null);

                Console.WriteLine("Dimension15 Logging Service");
                Console.WriteLine("PRESS ANY KEY TO EXIT");
                Console.ReadLine();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] { new LoggingService() };
                ServiceBase.Run(ServicesToRun);
            }            
        }
    }
}
