﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging
{
    /// <summary>
    /// Any object wishing to be a data store for LogEntry's must implement this interface
    /// </summary>
    /// <author name='Dean Komen' date='' />
    public interface ILogPersistor
    {
        /// <summary>
        /// The connection string to the datastore
        /// </summary>
        /// <author name='Dean Komen' date='' />
        string DataStoreConnection { get; set; }
        /// <summary>
        /// Save a log to the datastore
        /// </summary>
        /// <param name="logEntryToSave"></param>
        /// <author name='Dean Komen' date='' />
        void Save(Interfaces.ILogEntry logEntryToSave);
        /// <summary>
        /// Return all the system id's found in the datastore
        /// </summary>
        /// <returns></returns>
        /// <author name='Dean Komen' date='' />
        List<LogPersistance.SystemId> GetAllSystemIds();
        /// <summary>
        /// Retrieve a range of logs between the supplied date ranges
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        /// <author name='Dean Komen' date='' />
        List<Interfaces.ILogEntry> RetreiveAllByDateRange(DateTime startDate, DateTime? endDate);
        /// <summary>
        /// Retrieve a range of logs based on the supplied DORM filter object
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <author name='Dean Komen' date='' />
        List<Interfaces.ILogEntry> Retreive(DORM.Filter filter);
    }
}
