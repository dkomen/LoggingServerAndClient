using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Dimension15.Services.Logging
{
    /// <summary>
    /// Class used to serialize and deserialize objects
    /// </summary>
    public class Serializer
    {
        private const string INVALIDOBJECT = "Invalid object";
        private const string INVALIDSTRING = "Invalid String";

        #region Public Members

        #region Static

        /// <summary>
        /// Serialize an object into a base64 formatted string
        /// </summary>
        /// <param name="obj">Object to be serialized</param>
        /// <returns>Returns a base64 string if successful, else null </returns>
        public static string Base64Serialization( object obj )
        {
            // check parameter
            if ( obj == null )
            {
                throw new ArgumentNullException( "obj", INVALIDOBJECT );
            }
            string result = null;
            // create a memory stream from the object then seialize it
            MemoryStream memStream = null;
            try
            {
                memStream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize( memStream, obj );
                result = Convert.ToBase64String( memStream.GetBuffer() );
            }
            catch
            {
                throw;
            }
            finally
            {
                memStream.Dispose();
            }
            return result;
        }

        /// <summary>
        /// Deserialize the base64 string and create an instance of the object
        /// </summary>
        /// <param name="base64String">base64 string representing the serialized object</param>
        /// <returns>Returns an instance of the object if successful, else null</returns>
        public static object Base64Deserialization( string base64String )
        {
            return Base64Deserialization( base64String, null );
        }

        /// <summary>
        /// Deserialize the base64 string and create an instance of the object using a specific serialization binder
        /// </summary>
        /// <param name="base64String">base64 string representing the serialized object</param>
        /// <param name="binder">SerializationBinder to use when deserializing</param>
        /// <returns>Returns an instance of the object if successful, else null</returns>
        public static object Base64Deserialization( string base64String, SerializationBinder binder )
        {
            if ( base64String == null )
            {
                throw new ArgumentNullException( "base64String", INVALIDSTRING );
            }
            object result = null;
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Binder = binder;
            // load the string into a memory stream and then create the instance of the object
            byte[] binary = Convert.FromBase64String( base64String );
            MemoryStream memStream = null;
            try
            {
                memStream = new MemoryStream( binary );
                result = formatter.Deserialize( memStream );
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( memStream != null )
                {
                    memStream.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// Serialize an object into a Xml formatted string
        /// </summary>
        /// <param name="type">Type to use when serializing the object</param>
        /// <param name="obj">Object to be serialized</param>
        /// <returns>Returns a Xml formated string if successfule, else null</returns>
        /// <remarks>This function will only work with public classes</remarks>
        public static string XmlSerialization( Type type, object obj )
        {
            if ( obj == null )
            {
                throw new ArgumentNullException( "obj", INVALIDOBJECT );
            }
            string result = null;
            // create a string writer to convert the object into an xml stream
            StringWriter writer = null;
            try
            {
                writer = new StringWriter();
                XmlSerializer xmlSer = new XmlSerializer( type );
                xmlSer.Serialize( writer, obj );
                result = writer.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( writer != null )
                {
                    writer.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// Deserialize the Xml string and create an instance of the object
        /// </summary> 
        /// <param name="type">Type to deserialize the object into</param>     
        /// <param name="xmlString">The Xml formatted string containing the serialized object information</param>
        /// <returns>Returns an instance of the object if successful, else null</returns>
        /// <remarks>This function will only work with public classes</remarks>
        public static object XmlDeserialization( Type type, string xmlString )
        {
            if ( xmlString == null )
            {
                throw new ArgumentNullException( "xmlString", INVALIDSTRING );
            }
            object result = null;
            // load the string into a stream for an instance of the object to be created
            StringReader reader = null;
            try
            {
                reader = new StringReader( xmlString );
                XmlSerializer xmlSer = new XmlSerializer( type );
                result = xmlSer.Deserialize( reader );
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if ( reader != null )
                {
                    reader.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// Serialize an object into a byte array
        /// </summary>
        /// <param name="obj">Object to be serialized</param>
        /// <returns>Returns a byte array of the object if succesful, else null</returns>
        public static byte[] BinarySerialization( object obj )
        {
            if ( obj == null )
            {
                throw new ArgumentNullException( "obj", INVALIDOBJECT );
            }
            byte[] result = null;
            MemoryStream memStream = null;
            try
            {
                memStream = new MemoryStream();
                // create a memory stream of the object and get the serialized data
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize( memStream, obj );
                result = memStream.GetBuffer();
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( memStream != null )
                {
                    memStream.Dispose();
                }
            }
            return result;
        }

        /// <summary>
        /// Deserialize the byte array and create an instance of the object
        /// </summary>
        /// <param name="buffer">byte array containing the serialized object</param>
        /// <returns>Returns an instance of the object if successful, else null</returns>
        public static object BinaryDeserialization( byte[] buffer )
        {
            return BinaryDeserialization( buffer, null );
        }

        /// <summary>
        /// Deserialize the byte array and create an instance of the object using a specific serialization binder
        /// </summary>
        /// <param name="buffer">byte array containing the serialized object</param>
        /// <param name="binder">SerializationBinder to use when deserializing</param>
        /// <returns>Returns an instance of the object if successful, else null</returns>
        public static object BinaryDeserialization( byte[] buffer, SerializationBinder binder )
        {

            object result = null;

            // create a stream from the data and create an instance of the object
            MemoryStream memStream = null;
            try
            {
                memStream = new MemoryStream( buffer, false );
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Binder = binder;
                result = formatter.Deserialize( memStream );
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( memStream != null )
                {
                    memStream.Dispose();
                }
            }

            return result;
        }

        #endregion

        #endregion
    }
}
