﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dimension15.Services.Logging
{
    [Serializable]
    [DataContract]
    public class LogEntryBase : Interfaces.ILogEntryBase
    {
        [DataMember]
        public Enumerations.LogSeverity? LogSeverity
        { get; set; }
        [DataMember]
        public int? SystemID
        { get; set; }
        [DataMember]
        public string ModuleName
        { get; set; }
        [DataMember]
        public string FunctionName
        { get; set; }
        [DataMember]
        public int? ThreadID
        { get; set; }
        [DataMember]
        public string TraceID
        { get; set; }
        [DataMember]
        public string Message
        { get; set; }

        #region Overrides
        public override string ToString()
        {
            string value = string.Empty;
            if(LogSeverity.HasValue){ value = LogSeverity.Value.ToString(); }
            if(SystemID!=null){ if(value.Length>0){value+=", "; }; value += SystemID; }
            if(ModuleName!=null){ if(value.Length>0){value+=", "; }; value += ModuleName; }
            if(FunctionName!=null){ if(value.Length>0){value+=", "; }; value += FunctionName; }
            if(ThreadID.HasValue){ if(value.Length>0){value+=", "; }; value += ThreadID.Value; }
            if(TraceID!=null){ if(value.Length>0){value+=", "; }; value += TraceID; }
            if(Message!=null){ if(value.Length>0){value+=", "; }; value += Message; }

            return value;
        }
        #endregion
    }
}
