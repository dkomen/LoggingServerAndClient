﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Events.Handlers
{
    public delegate void EventTriggerd(LogEntry logEntry, string eventHandlerName);

    public abstract class BaseEventHandler
    {
        #region Constructors
        public BaseEventHandler()
        {
            try
            {
                LogSeverityTriggers = new List<Enumerations.LogSeverity>();
                SystemIdTriggers = new List<int>();
                MessageSnippetTriggers = new List<string>();
                Name = this.GetType().ToString();
            }
            catch(Exception ex)
            {
                throw;
            }
        }
        #endregion

        #region Properties
        public EventTriggerd OnEventTriggered { get; set; }

        /// <summary>
        /// The unique name of this event handler
        /// </summary>
        /// <author name="Dean Komen" date="26/07/2012" />
        public string Name { get; set; }

        /// <summary>
        /// A list of Log Severities that will trigger the event
        /// </summary>
        /// <author name="Dean Komen" date="26/07/2012" />
        protected List<Enumerations.LogSeverity> LogSeverityTriggers { get; set; }

        /// <summary>
        /// A list of System Id's that will trigger the event
        /// </summary>
        /// <author name="Dean Komen" date="26/07/2012" />
        protected List<int> SystemIdTriggers { get; set; }

        /// <summary>
        /// A list of text message snippets that may cause a trigger
        /// </summary>
        /// <author name="Dean Komen" date="19/04/2013" />
        protected List<string> MessageSnippetTriggers { get; set; }

        #endregion

        /// <summary>
        /// Check if this eventhandler specifies that the incoming parameters mat trigger an event
        /// </summary>
        /// <author name="Dean Komen" date="26/07/2012" />
        private bool AreTriggerRulesSatisfied(Enumerations.LogSeverity logSeverity, int systemId, LogEntry logEntry)
        {
            bool mayTrigger = LogSeverityTriggers.Count != 0;
            if (mayTrigger)
            {
                mayTrigger = LogSeverityTriggers.Contains(logSeverity);
            }
            //AND
            
            if (mayTrigger)
            {
                mayTrigger = SystemIdTriggers.Count != 0;
                if (!mayTrigger)
                {
                    mayTrigger = SystemIdTriggers.Contains(systemId);
                }
            }
            //AND the logEntry's message contains any ONE of the text snippets in MessageSnippetTriggers
            if (mayTrigger)
            {
                mayTrigger = MessageSnippetTriggers.Count != 0;
                if (!mayTrigger)
                {
                    foreach (string messageSnippetTrigger in MessageSnippetTriggers)
                    {
                        mayTrigger = logEntry.Message.ToLower().Contains(messageSnippetTrigger.ToLower());
                        if (mayTrigger)
                        {
                            break;
                        }
                    }
                }
            }

            return mayTrigger;
        }

        /// <summary>
        /// Check of this trigger should be fired and if so then fires it
        /// </summary>
        /// <param name="logEntry">The logEntry that caused this trigger query</param>
        /// <returns>Was the event triggered or not?</returns>
        /// <author name="Dean Komen" date="26/07/2012" />
        public virtual bool TryToTriggerEvent(LogEntry logEntry)
        {
            bool areTriggerRulesSatisfied = AreTriggerRulesSatisfied(logEntry.LogSeverity.Value, logEntry.SystemID.Value, logEntry);
            if (areTriggerRulesSatisfied)
            {
                System.Threading.Tasks.Task.Factory.StartNew(() => OnEventTriggered(logEntry, Name));
                return true;
            }

            return areTriggerRulesSatisfied;
        }
    }
}
