﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Events.Handlers
{
    public static class WatchDogMessages
    {

        #region Fields
        public static readonly List<WatchdogMessage> _serviceMessages = new List<WatchdogMessage>();
        public static readonly string PrefixIsRunning = "Service is running: ";
        public static readonly string PrefixNotRunning = "Service is NOT running: ";
        #endregion

        static WatchDogMessages()
        {
            #region Listed services            
            _serviceMessages.Add(new WatchdogMessage() { SystemId = (int)Enumerations.SystemID.MailerCache, IsRunningMessage = PrefixIsRunning + Enumerations.SystemID.MailerCache.ToString(), IsNotRunningMessage = PrefixNotRunning + Enumerations.SystemID.MailerCache.ToString() });
           
            //Add more services here
            //...
            //
            #endregion
        }

        public static string GetIsRunningMessageForSystem(int systemId)
        {
            return GetMessageForSystem(true, systemId);
        }

        public static string GetIsNotRunningMessageForSystem(int systemId)
        {
            return GetMessageForSystem(false, systemId);
        }


        #region Private Functions
        private static string GetMessageForSystem(bool getSuccessMessage, int systemId)
        {
            string foundMessage = string.Empty;
            WatchdogMessage watchdogMessage = GetWatchDogSystemIdMessages(systemId);
            if (watchdogMessage != null)
            {
                if (getSuccessMessage)
                {
                    return watchdogMessage.IsRunningMessage;
                }
                else
                {
                    return watchdogMessage.IsNotRunningMessage;
                }
            }
            return foundMessage;
        }

        /// <summary>
        /// Try to get the messages for the given SystemId
        /// </summary>
        /// <param name="systemId"></param>
        /// <returns></returns>
        private static WatchdogMessage GetWatchDogSystemIdMessages(int systemId)
        {
            foreach (WatchdogMessage watchdogMessage in _serviceMessages)
            {
                if (watchdogMessage.SystemId == systemId)
                {
                    return watchdogMessage;
                }
            }
                return null;
        }
        #endregion
    }
}
