﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Events.Handlers
{

    public class Test
    {
        public void testit()
        {
            TriggerOnAnySeriousError t = new TriggerOnAnySeriousError();
            LogEntry l = new LogEntry();
            l.FunctionName = "fname";
            l.LogSeverity = Enumerations.LogSeverity.Critical;
            l.Message = "message";
            l.ModuleName = "moduleName";
            l.ThreadID = 1;
            l.TraceID = "trace";
            l.SystemID = (int)Dimension15.Services.Logging.Enumerations.SystemID.MailerCache;
            t.TryToTriggerEvent(l);
        }
    }

    public class TriggerOnAnySeriousError: BaseEventHandler, IDisposable
    {
        #region Field
        private List<string> _messagesToBeSent = new List<string>();
        private System.Timers.Timer _sendTimer = new System.Timers.Timer();
        #endregion

        #region Constructors
        public TriggerOnAnySeriousError()
            : base()
        {
            base.Name = "Any Error in system (" + this.GetType().ToString() + ")";
            base.LogSeverityTriggers.Add(Enumerations.LogSeverity.Error);
            base.LogSeverityTriggers.Add(Enumerations.LogSeverity.Critical);

            for (int systemId = 0; systemId <= 100000000;systemId++)
            {
                base.SystemIdTriggers.Add(systemId);
            }

            _sendTimer.Interval = 90000;
            _sendTimer.Elapsed += new System.Timers.ElapsedEventHandler(SendMessages);
            _sendTimer.Enabled = true;
            this.OnEventTriggered += new EventTriggerd(Triggered);
        }

        #endregion

        #region Properties
        public DateTime LastTimeTriggerFired { get; private set; }
        #endregion

        /// <summary>
        /// Fires when an events requrements were satisfied. Place your event trigger code in here.
        /// </summary>
        /// <param name="logSeverity">The log severity that caused this event to trigger</param>
        /// <param name="systemId">The System Id that caused this event to trigger</param>
        /// <param name="message">The original log message</param>
        private void Triggered(LogEntry logEntry, string eventHandlerName)
        {              
            _messagesToBeSent.Add
                (
                    "<b><u>Originating computer:</u></b> " + System.Environment.MachineName + "(" + System.Environment.OSVersion + ")<br />" +
                    "<b>Eventhandler name:</b> " + eventHandlerName + "<br />" +
                    "<br /><b>SystemId:</b> " + logEntry.SystemID.Value.ToString() +
                    "<br /><b>LogSeverity:</b> <font color='red'>" + logEntry.LogSeverity.Value.ToString() + "</font>" +
                    "<br /><b>Module:</b> " + logEntry.ModuleName +
                    "<br /><b>Function:</b> " + logEntry.FunctionName +
                    "<br /><b>TraceId:</b> " + logEntry.TraceID +
                    "<br /><b>Approximate time:</b> " + (logEntry.DateStamp.HasValue ? logEntry.DateStamp.Value.ToString("dd MMM yyyy HH:mm:ss") : System.DateTime.Now.ToString("dd MMM yyyy HH:mm:ss")) +
                    "<br /><br /><b>Message</b><br/>" + logEntry.Message
                );
            SendMessages(null,null);
        }

        private void SendMessages(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_messagesToBeSent.Count > 0)
            {
                try
                {
                    if (Dimension15.Services.Logging.Properties.Settings.Default.EventAllErrorsMailRecipients.Trim() != string.Empty)
                    {
                        System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage(Dimension15.Services.Logging.Properties.Settings.Default.EventSmtpServerAuthUserName, Dimension15.Services.Logging.Properties.Settings.Default.EventAllErrorsMailRecipients);
                        foreach (string messageToBeSent in _messagesToBeSent)
                        {
                            mailMessage.Body += messageToBeSent + "<br /><br /><br /><br />";
                        }

                        mailMessage.Subject = "Dimension15 log event(s): " + base.Name;
                        mailMessage.IsBodyHtml = true;

                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Dimension15.Services.Logging.Properties.Settings.Default.EventSmtpServer, int.Parse(Dimension15.Services.Logging.Properties.Settings.Default.EventSmtpServerPort.ToString()));

                        smtp.Credentials = new System.Net.NetworkCredential(Dimension15.Services.Logging.Properties.Settings.Default.EventSmtpServerAuthUserName, Dimension15.Services.Logging.Properties.Settings.Default.EventSmtpServerAuthPassword);
                        smtp.Send(mailMessage);
                        _messagesToBeSent.Clear();
                    }
                }
                catch(Exception ex)
                {
                    string m = ex.ToString();
                    //Ignore this error for now, eish, yes
                }
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_sendTimer != null)
                    {
                        _sendTimer.Dispose();
                    }
                }
                _disposed = true;
            }
        }
        #endregion
    }
}
