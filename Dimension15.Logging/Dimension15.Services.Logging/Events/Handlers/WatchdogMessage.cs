﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Events.Handlers
{
    public class WatchdogMessage
    {
        #region Properties
        public int SystemId { get; set; }
        public string IsRunningMessage { get; set; }
        public string IsNotRunningMessage { get; set; }
        #endregion
    }
}
