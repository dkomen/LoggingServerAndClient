﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Diagnostics;

namespace Dimension15.Services.Logging
{
    /// <summary>
    /// Accepts logging data from consumers and persists it. A local cache is also maintained for consumers to be able to report on current log data
    /// </summary>
    /// <author name='Dean Komen' date='2012/03/29' />
    public class Server: IDisposable
    {
        #region Fields
        private ServiceHost _host;
        private static Server _serverInstance = null;
        private static object _locker = "ThreadLocker";
        private bool _running = false;
        private string _dataConnection = string.Empty;
        private List<Events.Handlers.BaseEventHandler> _eventHandlers = new List<Events.Handlers.BaseEventHandler>();
        public static double WatchdogTimerInterval = 60000*2;//Every 2 minutes
        private System.Timers.Timer _watchdog = new System.Timers.Timer(WatchdogTimerInterval);
        private string _logPersistorClassName = "";
        private List<LogPersistance.SystemId> _allSystemIds;
        /// <summary>
        /// A list of services that were not found during the last check (a check for which services are up or not)
        /// </summary>
        /// <author name="Dean Komen" />
        private Dictionary<int, DateTime> _servicesNotFound = new Dictionary<int, DateTime>();

        private static string _performaceCounterCatagoryName = "Dimension15 Logging Service";
        private PerformanceCounter _attemptsPerSecondCounter;
        private PerformanceCounter _succesfullyLoggedAttemptsPerSecond;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseUrl"></param>
        private Server(string subscriberServiceUrl)
        {

            CreatePerformanceMonitors();

            _attemptsPerSecondCounter = new PerformanceCounter(_performaceCounterCatagoryName, "Attempted logs per second", false);
            _succesfullyLoggedAttemptsPerSecond = new PerformanceCounter(_performaceCounterCatagoryName, "Successful logs per second", false);

            #region Add all event handlers to the event handlers list
            Type[] allTypes = this.GetType().Assembly.GetTypes();

            foreach (Type type in allTypes)
            {
                if (type.BaseType != null)
                {
                    if (type.BaseType.Equals(typeof(Events.Handlers.BaseEventHandler)))
                    {
                        _eventHandlers.Add((Events.Handlers.BaseEventHandler)Activator.CreateInstance(type));
                    }
                }
            }

            #endregion

            _dataConnection = Dimension15.Settings.Registry.LocalEnvironmentConfiguration.GetDatabaseConnectionString();
            _logPersistorClassName = Dimension15.Services.Logging.Properties.Settings.Default.LogPersistorClassName;
            Console.WriteLine("Log persistor: " + _logPersistorClassName);

            #region Watchdog service
            _watchdog.Elapsed += new System.Timers.ElapsedEventHandler(_watchdog_Elapsed);
            _watchdog.Start();
            #endregion

            #region SubscriberService
            Dimension15.Logging.Service.SubscriberService.Service.Start(subscriberServiceUrl);            
            #endregion

        }

        /// <summary>
        /// Check for entries in log of type 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _watchdog_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _watchdog.Stop();
            try
            {
                RetrievalFilter filter = new RetrievalFilter(false) { BreakOnAnyMatchFound=false};
                filter.StartDate = System.DateTime.Now.AddMilliseconds(-(_watchdog.Interval));
                filter.EndDate = System.DateTime.Now;
                foreach (Dimension15.Services.Logging.Events.Handlers.WatchdogMessage watchdogMessage in Dimension15.Services.Logging.Events.Handlers.WatchDogMessages._serviceMessages)
                {
                    int systemToMonitor = watchdogMessage.SystemId;
                    filter.FilterItems.Clear();
                    filter.AddFilterEntry(new LogEntryBase() { LogSeverity = Enumerations.LogSeverity.Information, SystemID = systemToMonitor,});

                    IList<Interfaces.ILogEntry> entries = _serverInstance.RetrieveLogEntries(filter, Enumerations.PersistanceDataStore.LocalCache);
                    bool foundService = entries.Count > 0;
                    if (foundService == false)
                    {
                        #region Create the log
                        LogEntry newLogEntry = new LogEntry();
                        newLogEntry.LogSeverity = Enumerations.LogSeverity.Critical;
                        newLogEntry.SystemID = systemToMonitor;
                        newLogEntry.Message = watchdogMessage.IsNotRunningMessage;
                        newLogEntry.DateStamp = System.DateTime.Now;
                        newLogEntry.ThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
                        newLogEntry.TraceID = "Service down";
                        #endregion

                        if (!_servicesNotFound.ContainsKey(systemToMonitor))
                        {
                            _servicesNotFound.Add(systemToMonitor, System.DateTime.Now.AddMilliseconds((double)((float)_watchdog.Interval * 2)));
                            _serverInstance.Add(newLogEntry);
                        }
                        else //Service already detected as bad
                        {
                            System.DateTime nextSendNotificationTime;
                            _servicesNotFound.TryGetValue(systemToMonitor, out nextSendNotificationTime);
                            if (System.DateTime.Now >= nextSendNotificationTime)
                            {
                                _servicesNotFound[systemToMonitor] = System.DateTime.Now.AddMilliseconds(_watchdog.Interval * 12);
                                _serverInstance.Add(newLogEntry);
                            }
                        }
                    }
                    else
                    {
                        _servicesNotFound.Remove(systemToMonitor);
                    }
                }
            }
            catch (Exception ex)
            {
                ///hmmmm we should never get here!
                throw;
            }
            finally
            {
                _watchdog.Start();
            }
        }
      #endregion

        #region Singleton Instance
        public static Server getInstance()
        {
            if(_serverInstance==null)
            {
                lock(_locker)
                {
                    string s = Dimension15.Settings.Registry.LocalEnvironmentConfiguration.GetLoggingServiceEndPoint();
                    Uri uri = new Uri(s);
                    _serverInstance = new Server("net.tcp://" + uri.Host + ":5052/LoggingServiceSubscriber");
                }
            }
            return _serverInstance;
        }
        #endregion

        #region Public Functions
        public void Add(Interfaces.ILogEntry logEntry)
        {
            _attemptsPerSecondCounter.Increment();
            if (MayLog(_allSystemIds, logEntry))
            {
                _succesfullyLoggedAttemptsPerSecond.Increment();
                //LogStatistics.LogStatistics.AddLogAccepted();    
                System.Threading.ThreadPool.QueueUserWorkItem(new System.Threading.WaitCallback(AddThreaded), logEntry);
            }
        }

        /// <summary>
        /// May the logEntry be logged?'
        /// </summary>
        /// <param name="allSystemIds">All system ids.</param>
        /// <param name="logEntry">The log entry.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// allSystemIds is null
        /// or
        /// logEntry is null
        /// </exception>
        public bool MayLog(List<LogPersistance.SystemId> allSystemIds, Interfaces.ILogEntry logEntry)
        {
            if (allSystemIds == null) throw new Exception("allSystemIds is null");
            if (logEntry== null) throw new Exception("logEntry is null");

            foreach (LogPersistance.SystemId currentSystemId in allSystemIds)
            {
                if (currentSystemId.Id == logEntry.SystemID)
                {
                    return ((currentSystemId.LogLevel & (int)logEntry.LogSeverity.Value) != 0);
                }
            }
            return true;
        }

        /// <summary>
        /// Adds the threaded.
        /// </summary>
        /// <param name="logEntry">The log entry.</param>
        /// <author name='DKomen' date='2013/10/31' />
        public void AddThreaded(object logEntry)
        {
            foreach (Events.Handlers.BaseEventHandler eventHandler in _eventHandlers)
            {
                Interfaces.ILogEntryBase logEntryConverted = (Interfaces.ILogEntryBase)logEntry;
                eventHandler.TryToTriggerEvent((LogEntry)logEntryConverted);
            }
            LogPersistance.LogPersistance.getInstance(_dataConnection, _logPersistorClassName).Add((Interfaces.ILogEntry)logEntry);

            string log = Dimension15.Services.Logging.Serializer.Base64Serialization(logEntry);
            new Dimension15.Logging.Service.SubscriberService.PubPubService().PublishNewLog(new Dimension15.Logging.Service.SubscriberService.ObjectToPass() { CustomObject = log });
            //Dimension15.Logging.Service.SubscriberService.Client.getInstance("", null).Publish(new Dimension15.Logging.Service.SubscriberService.ObjectToPass() { CustomObject=log});
        }

        /// <summary>
        /// Retrieve a list of entries based on the given filters
        /// </summary>
        /// <param name="filter">A filter to apply when retreiving log entries</param>
        /// <param name="dataStore">Datastore where to retrieve log entries from</param>
        /// <author name='Dean Komen' date='2012/03/29' />
        public List<Interfaces.ILogEntry> RetrieveLogEntries(RetrievalFilter filter, Enumerations.PersistanceDataStore dataStore)
        {
            List<Interfaces.ILogEntry> logEntriesFound = LogPersistance.LogPersistance.getInstance(_dataConnection, _logPersistorClassName).RetrieveLogEntries(filter, dataStore);
            return logEntriesFound;
        }
        
        public List<LogPersistance.SystemId> GetAllSystemIds()
        {
            List<LogPersistance.SystemId> systemIdsFound = LogPersistance.LogPersistance.getInstance(_dataConnection, typeof(LogPersistance.LogPersistorDatabase).Name).GetAllSystemIds();
            _allSystemIds = systemIdsFound;
            return systemIdsFound;
        }

        public void PrepopulateFromBackingStore(RetrievalFilter filter)
        {
            LogPersistance.LogPersistance.getInstance(_dataConnection, _logPersistorClassName).RetrieveLogEntries(filter, Enumerations.PersistanceDataStore.ExternalDataStore);
        }
        #endregion

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_watchdog != null)
                    {
                        _watchdog.Dispose();
                    }
                }
                _disposed = true;
            }
        }
        #endregion

        #region Private Functions
        private void CreatePerformanceMonitors()
        {
            try
            {
                //System.Diagnostics.PerformanceCounterCategory.Delete(_performaceCounterCatagoryName);
                if (!System.Diagnostics.PerformanceCounterCategory.Exists(_performaceCounterCatagoryName))
                {
                    System.Diagnostics.CounterCreationDataCollection CounterDatas = new System.Diagnostics.CounterCreationDataCollection();
                    // Create the counters and set their properties.
                    System.Diagnostics.CounterCreationData attemptsPerSecond = new System.Diagnostics.CounterCreationData();
                    attemptsPerSecond.CounterName = "Attempted logs per second";
                    attemptsPerSecond.CounterHelp = "Number of logs that were sent to the logging service (not necessarily actually logged though)";
                    attemptsPerSecond.CounterType = System.Diagnostics.PerformanceCounterType.RateOfCountsPerSecond32;

                    System.Diagnostics.CounterCreationData succesfullyLoggedAttemptsPerSecond = new System.Diagnostics.CounterCreationData();
                    succesfullyLoggedAttemptsPerSecond.CounterName = "Successful logs per second";
                    succesfullyLoggedAttemptsPerSecond.CounterHelp = "Number of logs that were sent to the logging service and actually logged";
                    succesfullyLoggedAttemptsPerSecond.CounterType = System.Diagnostics.PerformanceCounterType.RateOfCountsPerSecond32;

                    // Add both counters to the collection.
                    CounterDatas.Add(attemptsPerSecond);
                    CounterDatas.Add(succesfullyLoggedAttemptsPerSecond);

                    System.Diagnostics.PerformanceCounterCategory.Create(_performaceCounterCatagoryName, "Dimension15 Logging Service performance counters", PerformanceCounterCategoryType.SingleInstance, CounterDatas);
                }
            }
            catch(Exception ex)
            {
                ex = ex;
            }

        }
        #endregion
    }
}
