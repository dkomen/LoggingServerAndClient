﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Enumerations
{
    /// <summary>
    /// Where data is stored
    /// </summary>
    /// <author name='Dean Komen' date='2012/03/29' />
    public enum PersistanceDataStore
    {
        /// <summary>
        /// The local in memory data cache
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        LocalCache=1,
        /// <summary>
        /// An external datasource such as a database
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        ExternalDataStore=2
    }
}
