﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Enumerations
{
    /// <summary>
    /// The system ID of the system initiating the log event
    /// </summary>
    /// <author name='Dean Komen' date='2012/03/30' />
    [Serializable]
    public enum SystemID
    {
        /// <summary>
        /// An unknown System
        /// </summary>
        /// <author name='Dean Komen' date='2011/10/8' />
        UnknownSystem = 0,
        /// <summary>
        /// SystemId's for General Usage
        /// </summary>
        GeneralSystemId_1 = 1,
        GeneralSystemId_2 = 2,
        GeneralSystemId_3 = 3,
        Dimension15Logging = 100,


        MailerCache=200

    }
}
