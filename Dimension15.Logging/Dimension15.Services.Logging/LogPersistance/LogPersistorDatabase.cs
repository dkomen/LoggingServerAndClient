﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.LogPersistance
{
    /// <summary>
    /// Persist logs to a database
    /// </summary>
    public class LogPersistorDatabase : ILogPersistor
    {
        #region Fields
        private DORM.ConcreteAccessors.FluentNhibernate.Accessor<LogEntry> _dal=null;
        private int _maximumRecordsToRetrieve = 50000;
        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datastoreConnection">Connection string for the database connection</param>
        public LogPersistorDatabase(string datastoreConnection)
        {
            if (_dal == null)
            {
                DataStoreConnection = datastoreConnection;
                _dal = new DORM.ConcreteAccessors.FluentNhibernate.Accessor<LogEntry>(DataStoreConnection,DORM.Enumerations.DatastoreType.MsSql, true);
                _dal.CreateSessionFactory(true, DataStoreConnection, DORM.Enumerations.DatastoreType.MsSql);

                AddEnumSystemIdsToDatastore();

            }
        }
        /// <summary>
        /// Make sure that any SystemIds listd in the enumeration also occure in the datastore list of SystemIds
        /// </summary>
        private void AddEnumSystemIdsToDatastore()
        {
            using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
            {

                foreach (int id in Enum.GetValues(typeof(Enumerations.SystemID)))
                {
                    DORM.Filter filter = new DORM.Filter();                    

                    string field = DORM.HelperMethods.GetPropertyName(() => new Dimension15.Services.Logging.LogPersistance.SystemId().Id);
                    filter.FilterItem = filter.Create(field, DORM.Enumerations.FilterCriteriaComparitor.Equals, (long)id);
                    if(transaction.RetrieveFirst<SystemId>(filter)==null)
                    {
                        string description = Enum.GetName(typeof(Enumerations.SystemID), id);
                        transaction.AddOrUpdate(new SystemId() { Description = description, Id = (long)id, LogLevel = 3 });
                    }
                }
                transaction.Commit();
            }
        }
        #endregion

        #region ILogPersistor
        /// <summary>
        /// Connection string for the database connection
        /// </summary>
        public string DataStoreConnection { get; set; }

        /// <summary>
        /// Persist a log entry to the database
        /// </summary>
        /// <param name="logEntryToSave"></param>
        public void Save(Interfaces.ILogEntry logEntryToSave)
        {
            using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
            {
                transaction.AddOrUpdate(logEntryToSave);
                transaction.Commit();
            }
        }

        /// <summary>
        /// Retrieve a set of log entries from the database
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<Interfaces.ILogEntry> RetreiveAllByDateRange(DateTime startDate, DateTime? endDate)
        {
            using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Create(DORM.HelperMethods.GetPropertyName(()=>new LogEntry().DateStamp),DORM.Enumerations.FilterCriteriaComparitor.NotSmallerThan,startDate);
                if (endDate != null)
                {
                    filter.Create(DORM.HelperMethods.GetPropertyName(() => new LogEntry().DateStamp), DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan, endDate);
                }
                IList<LogEntry> r = transaction.Retrieve<LogEntry>(filter, _maximumRecordsToRetrieve+1);
                List<Interfaces.ILogEntry> l = new List<Interfaces.ILogEntry>(r);

                if (r.Count > _maximumRecordsToRetrieve)
                {
                    RetrievalFilter.AddAchtung(l, _maximumRecordsToRetrieve);
                }

                return l;
            }
        }

        public List<Interfaces.ILogEntry> Retreive(DORM.Filter filter)
        {
            using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
            {
                IList<LogEntry> r = transaction.Retrieve<LogEntry>(filter, _maximumRecordsToRetrieve+1);
                List<Interfaces.ILogEntry> l = new List<Interfaces.ILogEntry>(r);
                return l;
            }
        }

        /// <summary>
        /// Get all the SystemId's in the datastore
        /// </summary>
        /// <returns></returns>
        public List<SystemId> GetAllSystemIds()
        {
            using (DORM.Interfaces.ITransaction transaction = _dal.TransactionCreate())
            {
                IList<SystemId> r = transaction.Retrieve<SystemId>(null);
                List<SystemId> l = new List<SystemId>(r);
                transaction.Commit();
                return l;
            }
        }
        #endregion
    }
}
