﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Dynamic;

namespace Dimension15.Services.Logging.LogPersistance
{
    /// <summary>
    /// An log entry item that can be added to the logging system
    /// </summary>
    /// <author name='Dean Komen' date='2012/03/29' />
    public class LogPersistance
    {
        #region Fields
        /// <summary>
        /// A small local cache of log entries that may be used for reporting on. This cache exists so that high speed
        /// reporting on log entires may be done.. vs slower speed by first retreiving data from the datastore\database
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        private List<Interfaces.ILogEntry> _logEntryCache = new List<Interfaces.ILogEntry>();
        private int _maximimNumberOfCachedEntries = 250000;
        private static LogPersistance _instance;
        private static object _threadLocker = "ThreadLocker";
        private static string _logFilePath = string.Empty;
        //private static string _logFilePath = "E:\\Logs\\Dimension15Logging.dat";
        #endregion

        #region Constructors
        private LogPersistance(ILogPersistor logPersistor) 
        {
            LogPersistor = logPersistor;
        }
        #endregion

        #region Singleton
        public static LogPersistance getInstance(string datastoreConnection, string LogPersistorClassName)
        {
            if(_instance==null)
            {
                lock(_threadLocker)
                {
                    if (LogPersistorClassName == "LogPersistorDatabase")
                    {
                        _instance = new LogPersistance(new LogPersistorDatabase(datastoreConnection));
                    }
                    else if (LogPersistorClassName == "LogPersistorFlatFile")
                    {
                        _logFilePath = System.IO.Path.Combine(datastoreConnection, "Dimension15Logging.dat");
                         _instance = new LogPersistance(new LogPersistorFlatFile(_logFilePath));
                    }                    
                     else
                     {
                         throw new Exception("Unknown log persistor type!!!!!!!");
                     }
                }
            }

            return _instance;
        }
        #endregion

        #region Properties
        public ILogPersistor LogPersistor { get; set; }
        #endregion

        #region Public Functions
        /// <summary>
        /// Add a new entry to be persisted
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        public void Add(Interfaces.ILogEntry logEntry)
        {
            lock (_threadLocker)
            {
                //First add to local cache
                RemoveEntriesFromCacheIfNeeded();
                _logEntryCache.Add(logEntry);
                try
                {
                    LogPersistor.Save(logEntry);
                }
                catch (Exception ex)
                {
                    LogEntry errorLogEntry = new LogEntry();
                    errorLogEntry.DateStamp = System.DateTime.Now;
                    errorLogEntry.FunctionName = "public void Add(Interfaces.ILogEntry logEntry)";
                    errorLogEntry.Id = 1;
                    errorLogEntry.LogSeverity = Enumerations.LogSeverity.Warning;
                    errorLogEntry.Message = "Error persisting log: " + ex.ToString();
                    if(ex.InnerException!=null)
                    {
                        errorLogEntry.Message += "\r\nInner exception: " + ex.InnerException.ToString();
                    }
                    errorLogEntry.ModuleName = "none";
                    errorLogEntry.SystemID = (int)Enumerations.SystemID.Dimension15Logging;
                    errorLogEntry.ThreadID = 0;
                    errorLogEntry.TraceID = "";
                    _logEntryCache.Add(errorLogEntry);
                    #region Tell all subscribers that there was an error log created
                    string log = Dimension15.Services.Logging.Serializer.Base64Serialization(errorLogEntry);
                    new Dimension15.Logging.Service.SubscriberService.PubPubService().PublishNewLog(new Dimension15.Logging.Service.SubscriberService.ObjectToPass() { CustomObject = log });
                    #endregion
                }

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <author name='Dean Komen' date='2012/03/29' />
        public List<SystemId> GetAllSystemIds()
        {
            return LogPersistance.getInstance(_logFilePath, "LogPersistorDatabase").LogPersistor.GetAllSystemIds();
        }

        /// <summary>
        /// Retrieve a list of entries based on the given filters
        /// </summary>
        /// <param name="filter">A filter to apply when retreiving log entries</param>
        /// <param name="dataStore">Datastore where to retrieve log entries from</param>
        /// <author name='Dean Komen' date='2012/03/29' />
        public List<Interfaces.ILogEntry> RetrieveLogEntries(RetrievalFilter filter, Enumerations.PersistanceDataStore dataStore)
        {
            List<Interfaces.ILogEntry> retrievedLogEntries = new List<Interfaces.ILogEntry>();

            switch (dataStore)
            {
                case Enumerations.PersistanceDataStore.LocalCache:
                    retrievedLogEntries = filter.ApplyFilter(_logEntryCache);                    
                    break;
                case Enumerations.PersistanceDataStore.ExternalDataStore:
                    if (filter.FilterItems.Count == 0)
                    {
                        _logEntryCache = LogPersistance.getInstance(_logFilePath, "LogPersistorDatabase").LogPersistor.RetreiveAllByDateRange(filter.StartDate, filter.EndDate);
                        retrievedLogEntries = _logEntryCache;
                    }
                    else
                    {
                        DORM.Filter dalFilter = BuildDalFilter(filter);

                        retrievedLogEntries = LogPersistance.getInstance(_logFilePath, "LogPersistorDatabase").LogPersistor.Retreive(dalFilter);
                    }
                    
                    break;
            }

            return retrievedLogEntries;
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Builds a PayM8 Dal filterwhich is based on a RetrievalFilter. Only ptoperties relating to DateStamp, SystemId, TraceId and LogSeverity are checked
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        /// <author name='Dean Komen' date='' />
        private DORM.Filter BuildDalFilter(RetrievalFilter filter)
        {
            DORM.Filter dalFilter = new DORM.Filter();

            if (filter.StartDate != null)
            {
                dalFilter.FilterItem.Add(DORM.HelperMethods.GetPropertyName(() => new LogEntry().DateStamp), DORM.Enumerations.FilterCriteriaComparitor.NotSmallerThan, filter.StartDate);
            }
            if (filter.EndDate != null)
            {
                dalFilter.FilterItem.Add(DORM.HelperMethods.GetPropertyName(() => new LogEntry().DateStamp), DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan, filter.EndDate);
            }
            for (int filterIndexCounter = 0; filterIndexCounter < filter.FilterItems.Count; filterIndexCounter++)
            {
                //SystemId
                if (filter.FilterItems[filterIndexCounter].SystemID.HasValue)
                {
                    dalFilter.FilterItem.Add(DORM.HelperMethods.GetPropertyName(() => new LogEntry().SystemID), DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan, filter.FilterItems[filterIndexCounter].SystemID.Value);
                }
                //TraceId
                if (filter.FilterItems[filterIndexCounter].TraceID != null)
                {
                    dalFilter.FilterItem.Add(DORM.HelperMethods.GetPropertyName(() => new LogEntry().TraceID), DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan, filter.FilterItems[filterIndexCounter].TraceID);
                }
                //logSeverity
                if (filter.FilterItems[filterIndexCounter].LogSeverity.HasValue)
                {
                    dalFilter.FilterItem.Add(DORM.HelperMethods.GetPropertyName(() => new LogEntry().LogSeverity), DORM.Enumerations.FilterCriteriaComparitor.NotGreaterThan, filter.FilterItems[filterIndexCounter].LogSeverity.Value);
                }
            }

            return dalFilter;
        }
        /// <summary>
        /// If there are too many entries in the local cache then remove a few
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        private bool RemoveEntriesFromCacheIfNeeded()
        {
            lock (_threadLocker)
            {
                bool entriesWereDeleted = false;

                if (_logEntryCache.Count > _maximimNumberOfCachedEntries)
                {
                    //To many entries so remove a few old ones
                    for (int loopCounter = 0; loopCounter < 100; loopCounter++)
                    {
                        _logEntryCache.RemoveAt(_logEntryCache.Count - 1);
                    }
                    entriesWereDeleted = true;
                }

                return entriesWereDeleted;
            }
        }
        #endregion
    }
}
