﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.LogPersistance
{
    /// <summary>
    /// A of unique System Id used to indicate the various systems that the logger may log items for
    /// </summary>
    /// <author name="Dean Komen" />
    public class SystemId: DORM.Entity
    {
        #region Properties
        public virtual long Id { get; set; }
        public virtual string Description { get; set; }
        /// <summary>
        /// 0 = No logging, look at the loglevels indicated in PayM8.Services.Logging.Enumerations.LogSeverity, bit manip 'OR' them up
        /// </summary>
        public virtual int LogLevel { get; set; }
        #endregion

        #region Public overrides
        public override string ToString()
        {
            return Id.ToString() + ":" + Description;
        }
        #endregion
    }
}