﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Dimension15.Services.Logging.LogPersistance.DatastoreEntitiesMapping
{
    /// <summary>
    /// The datastore mapping of unique System Id used to indicate the various systems that the logger may log items for
    /// </summary>
    /// <author name="Dean Komen" />
    public class SystemIdMapping : FluentNHibernate.Mapping.ClassMap<SystemId>
    {
        public SystemIdMapping()
        {
            Id(x => x.Id).GeneratedBy.Assigned();
            Map(x => x.Description).Nullable().Length(250);
            Map(x => x.LogLevel).Not.Nullable();         
        }
    }
}