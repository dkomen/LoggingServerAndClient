﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Dimension15.Services.Logging.LogPersistance.DatastoreEntitiesMapping
{
    public class LogEntryMapping: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<LogEntry>
    {
        public LogEntryMapping()
        {
            Map(x => x.DateStamp).Not.Nullable().Index("IDX_" + this.GetType().Name);
            Map(x => x.SystemID).Nullable().Index("IDX_" + this.GetType().Name);
            Map(x => x.ThreadID).Nullable();
            Map(x => x.ModuleName).Nullable().Length(512);
            Map(x => x.TraceID).Nullable();
            Map(x => x.FunctionName).Nullable().Length(512);
            Map(x => x.LogSeverity).Nullable().Index("IDX_" + this.GetType().Name);
            Map(x => x.Message).Nullable().Length(8000);                 
        }
    }
}
