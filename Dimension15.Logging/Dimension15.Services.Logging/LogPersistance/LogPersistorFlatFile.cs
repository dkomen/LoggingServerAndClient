﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.LogPersistance
{
    public class LogPersistorFlatFile: ILogPersistor
    {
        #region Fields
        private object _threadLocker = "ThreadLocker";
        #endregion

        #region Properties
        public string DataStoreConnection
        {
           get;set;
        }
        #endregion

        #region Constructors
        public LogPersistorFlatFile(string datatoreConnection)
        {
            DataStoreConnection = datatoreConnection;
        }
        #endregion

        public void Save(Interfaces.ILogEntry logEntryToSave)
        {
            lock(_threadLocker)
            {
                try
                {
                    //TODO : DeanK - must still persist to DB too
                    if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(DataStoreConnection)) == false)
                    {
                        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(DataStoreConnection));
                    }

                    if (System.IO.File.Exists(DataStoreConnection))
                    {
                        if (new System.IO.FileInfo(DataStoreConnection).Length > (17.5 * 1024 * 1024)) //17.5Mb
                        {
                            string newName = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(DataStoreConnection), (System.IO.Path.GetFileNameWithoutExtension(DataStoreConnection) + "-" + System.DateTime.Now.ToString("ddMMyyyy hhmmss") + System.IO.Path.GetExtension(DataStoreConnection)));
                            System.IO.File.Move(DataStoreConnection, newName);
                        }
                    }

                    System.IO.TextWriter writer = new System.IO.StreamWriter(DataStoreConnection, true);

                    writer.WriteLine(LogEntry.ToRawString((LogEntry)logEntryToSave)); //outch, bad cast

                    writer.Close();
                }
                catch (Exception ex)
                {
                    //Could write log!
                    throw;
                }
            }
        }
        public List<Interfaces.ILogEntry> RetreiveAllByDateRange(DateTime startDate, DateTime? endDate)
        {

            List<Interfaces.ILogEntry> logEntriesFound = new List<Interfaces.ILogEntry>();
            try{
                if (System.IO.File.Exists(DataStoreConnection) == true)
                {
                    string[] fileEntries = new string[0];

                    System.IO.TextReader reader = new System.IO.StreamReader(DataStoreConnection, true);

                    fileEntries = reader.ReadToEnd().Split(new string[] { "||--\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                    reader.Close();

                    DateTime dateToEndOn = endDate.HasValue ? endDate.Value : System.DateTime.Now;

                    foreach (string entry in fileEntries)
                    {
                        LogEntry logEntry = null;
                        try
                        {
                            logEntry = LogEntry.FromRawString(entry);
                            if (logEntry.DateStamp >= startDate && logEntry.DateStamp <= dateToEndOn)
                            {
                                logEntriesFound.Add(logEntry);
                                //if (logEntriesFound.Count == 10)
                                //{
                                //    return logEntriesFound;
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            int t = 1;
                        }
                    }                
                }
            }
            catch (Exception ex)
            {
                //File was locked!!
                logEntriesFound = new List<Interfaces.ILogEntry>();
                LogEntry logEntry = new LogEntry();
                logEntry.DateStamp = System.DateTime.Now;
                logEntry.FunctionName = "none";
                logEntry.Id = 1;
                logEntry.LogSeverity = Enumerations.LogSeverity.Warning;
                logEntry.Message = "Error reading log file: " + ex.Message;
                logEntry.ModuleName = "none";
                logEntry.SystemID = (int)Enumerations.SystemID.Dimension15Logging;
                logEntry.ThreadID = 0;
                logEntry.TraceID = "";
                logEntriesFound.Add(logEntry);
            }
            return logEntriesFound;   
        }

        public List<Interfaces.ILogEntry> Retreive(DORM.Filter filter)
        {
            throw new NotImplementedException();
        }
        public List<SystemId> GetAllSystemIds()
        {
            return null;
        }        
    }
}
