﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Dimension15.Services.Logging
{
    /// <summary>
    ///   A log entry item that can be added to the logging system
    /// </summary>
    /// <author name='DKomen' date='' />
    [Serializable]
    [DataContract]
    public class LogEntry : Dimension15.Services.Logging.Interfaces.ILogEntry
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="LogEntry"/> class.
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        public LogEntry()
        {
            //DateStamp = System.DateTime.Now;
            //Random rnd = new Random();
            //Id = long.Parse(DateStamp.Value.ToString("yyMMddHHmmssffff") + rnd.Next(10,99));
        }
        #endregion

        #region Properties
        /// <summary>
        /// The datetime stamp of event
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual DateTime? DateStamp
        { get; set; }

        /// <summary>
        /// The unique ID of this log event
        /// </summary>        
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual long Id
        { get; set; }

        /// <summary>
        /// The severity of the event
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual Enumerations.LogSeverity? LogSeverity
        { get; set; }

        /// <summary>
        /// The name of the system from which the event was logged
        /// </summary>
        /// <example>
        ///     RPI Submission engine/Collect business layer/ etc
        /// </example>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual int? SystemID
        { get; set; }

        /// <summary>
        /// The name of the Module from which the event was logged
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual string ModuleName
        { get; set; }

        /// <summary>
        /// The name of the function from which the event was logged
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual string FunctionName
        { get; set; }

        /// <summary>
        /// The ID of the thread in which this event was logged
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual int? ThreadID
        { get; set; }

        /// <summary>
        /// A value that may be used to group multiple logs.
        /// </summary>
        /// <example>
        ///     ClientID when wishing to trace the flow of all logs related to a specific client
        /// </example>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual string TraceID
        { get; set; }

        /// <summary>
        /// The log message
        /// </summary>
        /// <author name='DKomen' date='2013/03/29' />
        [DataMember]
        public virtual string Message
        { get; set; }
        #endregion

        #region Public Functions
        #region Create log entry
        /// <summary>
        /// Creates the log entry.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="traceID">The trace identifier.</param>
        /// <param name="systemID">The system identifier.</param>
        /// <returns></returns>
        /// <author name='DKomen' date='2013/03/29' />
        public static LogEntry CreateLogEntry(Exception ex, string traceID, int systemID)
        {
            return CreateLogEntry(ex, traceID, systemID, 0);
        }
        /// <summary>
        /// Creates the log entry.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="traceID">The trace identifier.</param>
        /// <param name="systemID">The system identifier.</param>
        /// <param name="stackframeOffset">The stackframe offset.</param>
        /// <returns></returns>
        /// <author name='DKomen' date='2013/03/29' />
        public static LogEntry CreateLogEntry(Exception ex, string traceID, int systemID, int stackframeOffset)
        {
            return CreateLogEntry(Enumerations.LogSeverity.Error, ex.Message, traceID, systemID, stackframeOffset);
        }


        /// <summary>
        /// Creates the log entry.
        /// </summary>
        /// <param name="logSeverity">The log severity.</param>
        /// <param name="logMessage">The log message.</param>
        /// <param name="traceID">The trace identifier.</param>
        /// <param name="systemID">The system identifier.</param>
        /// <returns></returns>
        /// <author name='DKomen' date='2013/03/29' />
        public static LogEntry CreateLogEntry(Enumerations.LogSeverity logSeverity, string logMessage, string traceID, int systemID)
        {
            return CreateLogEntry(logSeverity, logMessage, traceID, systemID, 0);
        }
        /// <summary>
        /// Creates the log entry.
        /// </summary>
        /// <param name="logSeverity">The log severity.</param>
        /// <param name="logMessage">The log message.</param>
        /// <param name="traceID">The trace identifier.</param>
        /// <param name="systemID">The system identifier.</param>
        /// <param name="stackframeOffset">The stackframe offset.</param>
        /// <returns></returns>
        /// <author name='DKomen' date='2013/03/29' />
        public static LogEntry CreateLogEntry(Enumerations.LogSeverity logSeverity, string logMessage, string traceID, int systemID, int stackframeOffset)
        {
            LogEntry logEntry = new LogEntry();
            ModuleInformation info = StackFrame.GetExecutingMethod(stackframeOffset);
            logEntry.DateStamp = DateTime.Now;
            logEntry.LogSeverity = logSeverity;
            logEntry.FunctionName = info.MethodName;
            logEntry.ModuleName = info.DeclaringType;
            logEntry.Message = logMessage;
            logEntry.SystemID = systemID;
            logEntry.ThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
            logEntry.TraceID = traceID;

            return logEntry;
        }
        #endregion

        /// <summary>
        /// Return a list of all the fields that reports filtering may use.. as well as their datatypes
        /// </summary>
        /// <returns></returns>
        /// <author name="Dean Komen" date="2012/03/30" />
        public static List<FieldType> GetFilterFieldNames()
        {
            List<FieldType> fieldNames = new List<FieldType>();

            System.Reflection.PropertyInfo[] properties = typeof(LogEntryBase).GetProperties();
            foreach (System.Reflection.PropertyInfo property in properties)
            {
                fieldNames.Add(new FieldType() {  Name = property.Name, type=property.PropertyType});
            }

            return fieldNames;
        }

        public static string ToRawString(LogEntry logEntry)
        {
            string fieldId = "||>";
            string fieldIdEnd = "||--";

            return fieldId + "||>" +
                logEntry.DateStamp.Value.ToString("dd MMM yyyy HH:mm:ss") + fieldId +
                logEntry.ThreadID.ToString() + fieldId +
                logEntry.LogSeverity.ToString() + fieldId +
                logEntry.SystemID + fieldId +
                logEntry.ModuleName + fieldId +
                logEntry.FunctionName + fieldId +
                logEntry.TraceID.ToString() + fieldId +
                logEntry.Message + fieldId +
                logEntry.Id + fieldIdEnd;
        }
        /// <summary>
        /// Froms the raw string.
        /// </summary>
        /// <param name="rawData">The raw data.</param>
        /// <returns></returns>
        /// <author name='DKomen' date='2013/03/29' />
        public static LogEntry FromRawString(string rawData)
        {
            int pos = 0;
            int posNext = 0;
            string fieldId = "||>";
            string fieldIdEnd = "||--";

            LogEntry logEntry = new LogEntry();
            pos = rawData.IndexOf(fieldId)+3;
            posNext = rawData.IndexOf(fieldId, pos);

            if (rawData.Substring(pos, posNext - pos) != string.Empty)
            {
                logEntry.Id = long.Parse(rawData.Substring(pos, posNext - pos));
            }

            //DateStamp
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            logEntry.DateStamp = DateTime.Parse(rawData.Substring(pos, posNext - pos));

            //ThreadId
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            logEntry.ThreadID = int.Parse(rawData.Substring(pos, posNext - pos));

            //LogSeverity
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            logEntry.LogSeverity = (Enumerations.LogSeverity)Enum.Parse(typeof(Enumerations.LogSeverity),rawData.Substring(pos, posNext - pos));

            //SystemId
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            int systemId = (int)Enumerations.SystemID.UnknownSystem;
            if (rawData.Substring(pos, posNext - pos) != string.Empty)
            {
                systemId = int.Parse(rawData.Substring(pos, posNext - pos));
            }
            logEntry.SystemID = systemId;

            //ModuleName
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            logEntry.ModuleName = rawData.Substring(pos, posNext - pos);

            //FunctionName
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            logEntry.FunctionName = rawData.Substring(pos, posNext - pos);

            //TraceId
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            logEntry.TraceID = rawData.Substring(pos, posNext - pos);

            //Message
            bool endOfLogEntry = false;
            pos = rawData.IndexOf(fieldId, posNext) + 3;
            posNext = rawData.IndexOf(fieldId, pos);
            if (posNext < 1)
            {
                posNext = rawData.Length - pos;
                endOfLogEntry = true;
                logEntry.Message = rawData.Substring(pos, posNext);
            }
            else
            {
                logEntry.Message = rawData.Substring(pos, posNext - pos);
            }
            
            //Log Entry Id
            if (!endOfLogEntry)
            {
                pos = rawData.IndexOf(fieldId, posNext) + 3;
                posNext = rawData.IndexOf(fieldIdEnd, pos);
                if (posNext < 1)
                {
                    posNext = rawData.Length - pos;
                }
                logEntry.Id = int.Parse(rawData.Substring(pos, posNext));
            }

            return logEntry;

        }
        #endregion

        #region Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        /// <author name='DKomen' date='2013/10/29' />
        public override string ToString()
        {
            return Id.ToString() + " " + DateStamp.Value.ToString("dd MMM yyyy hh:mm:ss") + " (" + ThreadID.ToString().PadLeft(4,' ') + ") " + LogSeverity.ToString() + " <" + SystemID 
                + "> <" + ModuleName + ":" + FunctionName + ">  " + TraceID.ToString() + " => " + Message;
        }
        #endregion
    }
}