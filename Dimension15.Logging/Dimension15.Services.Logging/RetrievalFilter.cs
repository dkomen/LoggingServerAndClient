﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dimension15.Services.Logging
{
    /// <summary>
    /// A set of filter items that together define a filter to apply to list of log entries
    /// </summary>
    /// <author name='Dean Komen' date='2012/02/29' />
    [Serializable]
    [DataContract]
    public class RetrievalFilter
    {
        #region Fields
        private List<LogEntryBase> _filterItems = new List<LogEntryBase>();
        #endregion

        #region Constructors
        /// <summary>
        /// Defaults property BreakOnAnyMatchFound==true
        /// </summary>
        /// <author name="Dean Komen"/>
        public RetrievalFilter()
        {
            BreakOnAnyMatchFound = true;
        }
        public RetrievalFilter(bool breakOnAnyMatchFound)
        {
            BreakOnAnyMatchFound = breakOnAnyMatchFound;
        }
        #endregion

        #region Properties
        [DataMember]
        public List<LogEntryBase> FilterItems
        {
            get
            {
                return _filterItems;
            }
            set
            {
                _filterItems = value;
            }
        }
        /// <summary>
        /// Difference between AND'ing the filter criteria together or OR'ing 'em
        /// </summary>
        /// <remarks>True==OR, FALSE=AND</remarks>
        [DataMember]
        public bool BreakOnAnyMatchFound { get; set; }
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public DateTime? EndDate { get; set; }
        #endregion

        #region Public Functions
        public static void AddAchtung(List<Interfaces.ILogEntry> listOfEntries, int maxRecordsToReturn)
        {
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "=========================================================", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "ACHTUNG!", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "WARNING   ACHTUNG", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "ACHTUNG   WARNING   ACHTUNG", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "WARNING   ACHTUNG   WARNING   ACHTUNG: A limit of " + maxRecordsToReturn.ToString() + " has been imposed on the number of log entries that may be retrieved from the logging system, change your search filter criteria to return fewer records.", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "ACHTUNG   WARNING   ACHTUNG", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "WARNING   ACHTUNG", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "ACHTUNG!", "", (int)Enumerations.SystemID.Dimension15Logging));
            listOfEntries.Add(LogEntry.CreateLogEntry(Enumerations.LogSeverity.Warning, "=========================================================", "", (int)Enumerations.SystemID.Dimension15Logging));
        }
        /// <summary>
        /// Add a new filter entry item to the set of filters
        /// </summary>
        /// <author name='Dean Komen' date='2012/02/29' />
        public void AddFilterEntry(LogEntryBase filterEntry)
        {
            _filterItems.Add(filterEntry);
        }

        /// <summary>
        /// Apply the filter to a set of data
        /// </summary>
        /// <author name='Dean Komen' date='2012/02/29' />
        private static object _threadLocker = "ThreadLocker";
        public List<Interfaces.ILogEntry> ApplyFilter(List<Interfaces.ILogEntry> dataToApplyFilterToo)
        {
            lock (_threadLocker)
            {
                int maxRecordsToReturn = 75000;
                Dictionary<string, List<object>> filterItems = BuilderFilterTree();
                List<Interfaces.ILogEntry> result = new List<Interfaces.ILogEntry>();

                //For each logged item
                foreach (Interfaces.ILogEntry logEntry in dataToApplyFilterToo)
                {
                    //Check if its datestamp is >= startdate
                    if (logEntry.DateStamp >= StartDate)
                    {
                        //Then check if its datestamp <= EndDate OR is null (Null means get all records till last added record in data set)
                        if ((logEntry.DateStamp <= EndDate) || (EndDate.HasValue == false))
                        {
                            //If we have additional filter items then filter on them too
                            if (filterItems.Count != 0)
                            {
                                if (MatchFilter(logEntry, filterItems))
                                {
                                    result.Add(logEntry);
                                }
                            }
                            else //We have no additional filter items except the date ranges so only worry about them
                            {
                                result.Add(logEntry);
                            }
                            if (result.Count == maxRecordsToReturn)
                            {
                                AddAchtung(result, maxRecordsToReturn);
                                break;
                            }
                        }
                        else
                        {
                            //Rows should be sorted by date, so is okay to exit now
                            break;
                        }
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// Does the given log entry match the all filter data criteria?
        /// </summary>
        /// <author name='Dean Komen' date='2012/02/29' />
        private bool MatchFilter(Interfaces.ILogEntry logEntry, Dictionary<string, List<object>> filterItems)
        {
            bool matchedAllCriteria = false;
            foreach (KeyValuePair<string, List<object>> filterItem in filterItems)
            {
                System.Reflection.PropertyInfo property = logEntry.GetType().GetProperty(filterItem.Key);
                object propertyValue = property.GetValue(logEntry, null);
                foreach (object value in filterItem.Value)
                {
                    matchedAllCriteria = false;
                    string lcaseValue = value.ToString().ToLower();
                    string lcasePropertyValue = propertyValue.ToString().ToLower();

                    if (propertyValue.Equals(value))
                    {
                        matchedAllCriteria = true;
                    }
                    else if (value.ToString().StartsWith("\\%"))
                    {
                        if (lcasePropertyValue.Contains(lcaseValue.Substring(2, lcaseValue.Length - 2)))
                        {
                            matchedAllCriteria = true;
                        }
                    }
                    else if (value.ToString().StartsWith("\\>"))
                    {
                        if (lcasePropertyValue.StartsWith(lcaseValue.Substring(2, lcaseValue.Length - 2)))
                        {
                            matchedAllCriteria = true;
                        }
                    }
                    else if (value.ToString().StartsWith("\\<"))
                    {
                        if (lcasePropertyValue.EndsWith(lcaseValue.Substring(2, lcaseValue.Length - 2)))
                        {
                            matchedAllCriteria = true;
                        }
                    }

                    if (BreakOnAnyMatchFound)
                    {
                        if (matchedAllCriteria)
                        {
                            return true;
                        }
                    }
                    else if (!matchedAllCriteria) //Any check failed so cancel this filter check
                    {
                        return false;
                    }
                }
            }

            return matchedAllCriteria;
        }
        private Dictionary<string, List<object>> BuilderFilterTree()
        {
            Dictionary<string, List<object>> filterItems = new Dictionary<string, List<object>>();

            //Extract all the individual distinct fields and find all there repspective filter values
            foreach (Interfaces.ILogEntryBase filterEntry in _filterItems)
            {
                System.Reflection.PropertyInfo[] fields = ((Interfaces.ILogEntryBase)filterEntry).GetType().GetProperties();
                foreach (System.Reflection.PropertyInfo field in fields)
                {                    
                    object value = field.GetValue(filterEntry, null);
                    if (value != null)
                    {
                        if (filterItems.ContainsKey(field.Name) == false)
                        {
                            filterItems.Add(field.Name, new List<object>());
                        }
                        filterItems[field.Name].Add(value);
                    }                    
                }
            }

            return filterItems;
        }
        /// <summary>
        /// Inspect the retrieval filter and generate a SQL version there of
        /// </summary>
        /// <author name='Dean Komen' date='2012/02/29' />
        public string GenerateSQLFilterStatement()
        {
            string sqlStatement = string.Empty;
            Dictionary<string, List<object>> filterItems = BuilderFilterTree();

            //Now using the extracted fields and all there respective values.. build the sql string
            foreach (KeyValuePair<string, List<object>> filterField in filterItems)
            {
                if(sqlStatement!=string.Empty)
                {
                    sqlStatement+= " AND ";
                }
                sqlStatement += filterField.Key + " = '";

                string values = string.Empty;
                foreach (object value in filterField.Value)
                {
                    if (values != string.Empty)
                    {
                        values += ',';
                    }
                    values += value.ToString();
                }
                sqlStatement += filterField.Key + values + "'";
            }

            return sqlStatement;
        }
        #endregion
    }
}
