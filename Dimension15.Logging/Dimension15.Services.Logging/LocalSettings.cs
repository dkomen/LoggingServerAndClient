﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dimension15.Settings.File;
using Dimension15.Settings.File.Attributes;

namespace Dimension15.Services.Logging
{
    /// <summary>
    /// 
    /// </summary>
    /// <author name="Dean Komen" date="" />
    class LocalSettings: BaseManager
    {
        #region Fields
        private static string _eventSmtpServerPort = "eventSmtpServerPort";
        private static string _eventSmtpServer = "eventSmtpServer";
        private static string _eventSmtpServerAuthUserName = "eventSmtpServerAuthUserName";
        private static string _eventSmtpServerAuthPassword = "eventSmtpServerAuthPassword";
        private static string _eventAllErrorsMailRecipients = "eventAllErrorsMailRecipients";
        private static string _eventServiceDownMailRecipients = "eventServiceDownMailRecipients";
        private static string _logPersistorClassName = "logPersistorClassName";
        #endregion

        #region Constructors
        /// <summary>
        /// Default public constructor
        /// </summary>
        /// <author name="Dean Komen" date="" />  
        public LocalSettings()
        { 
            
        }
        #endregion

        #region Properties
        [DefaultSettingValue("")]
        public static string EventSmtpServer
        {
            get
            {
                if (_manager.SettingExists(_eventSmtpServer))
                {
                    return _manager.GetSettingValue(_eventSmtpServer);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("EventSmtpServer");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_eventSmtpServer, value);
            }
        }

        [DefaultSettingValue("")]
        public static string EventSmtpServerPort
        {
            get
            {
                if (_manager.SettingExists(_eventSmtpServerPort))
                {
                    return _manager.GetSettingValue(_eventSmtpServerPort);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("EventSmtpServerPort");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_eventSmtpServerPort, value);
            }
        }

        [DefaultSettingValue("")]
        public static string EventSmtpServerAuthUserName
        {
            get
            {
                if (_manager.SettingExists(_eventSmtpServerAuthUserName))
                {
                    return _manager.GetSettingValue(_eventSmtpServerAuthUserName);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("EventSmtpServerAuthUserName");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_eventSmtpServerAuthUserName, value);
            }
        }
       
        [DefaultSettingValue("")]
        public static string EventSmtpServerAuthPassword
        {
            get
            {
                if (_manager.SettingExists(_eventSmtpServerAuthPassword))
                {
                    return _manager.GetSettingValue(_eventSmtpServerAuthPassword);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("EventSmtpServerAuthPassword");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_eventSmtpServerAuthPassword, value);
            }
        }

        [DefaultSettingValue("")]
        public static string EventAllErrorsMailRecipients
        {
            get
            {
                if (_manager.SettingExists(_eventAllErrorsMailRecipients))
                {
                    return _manager.GetSettingValue(_eventAllErrorsMailRecipients);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("EventAllErrorsMailRecipients");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_eventAllErrorsMailRecipients, value);
            }
        }

        [DefaultSettingValue("")]
        public static string EventServiceDownMailRecipients
        {
            get
            {
                if (_manager.SettingExists(_eventServiceDownMailRecipients))
                {
                    return _manager.GetSettingValue(_eventServiceDownMailRecipients);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("EventServiceDownMailRecipients");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_eventServiceDownMailRecipients, value);
            }
        }

        [DefaultSettingValue("LogPersistorDatabase")]
        public static string LogPersistorClassName
        {
            get
            {
                if (_manager.SettingExists(_logPersistorClassName))
                {
                    return _manager.GetSettingValue(_logPersistorClassName);
                }
                else
                {
                    System.Reflection.PropertyInfo property = (typeof(LocalSettings)).GetProperty("LogPersistorClassName");
                    return Dimension15.Settings.File.Attributes.DefaultSettingValueAttribute.DefaultPropertyValue(property);
                }
            }
            set
            {
                _manager.SetSettingValue(_logPersistorClassName, value);
            }
        }
        #endregion

        #region Public Functions
        #endregion

        #region Private Functions
        #endregion
    }
}
