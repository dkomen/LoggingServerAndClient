﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging
{
    public class FieldType
    {
        #region Properties
        public string Name { get; set; }
        public Type type { get; set; }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return Name;
        }
        #endregion


    }
}
