﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging
{
    public class StackFrame
    {

        public static ModuleInformation GetExecutingMethod()
        {
            return GetExecutingMethod(0);
        }

        public static ModuleInformation GetExecutingMethod(int stackframeOffset)
        {
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
            string callingMethodName = string.Empty;
            ModuleInformation info = new ModuleInformation();
            int traverseStackFrameIndex = stackframeOffset;
            foreach (System.Diagnostics.StackFrame frame in stackTrace.GetFrames())
            {
                if (frame.GetMethod().DeclaringType != null)
                {
                    if ((!frame.GetMethod().DeclaringType.FullName.ToLower().StartsWith("Dimension15.services.logging")
                        && !frame.GetMethod().DeclaringType.FullName.ToLower().StartsWith("microsoft.practices.enterpriseLibrary.logging"))
                        && (!frame.GetMethod().DeclaringType.FullName.ToLower().Contains(".logging")))
                    {
                        if (traverseStackFrameIndex == 0)
                        {
                            info = new ModuleInformation() { DeclaringType = frame.GetMethod().DeclaringType.FullName, MethodName = frame.GetMethod().Name };
                            break;
                        }
                        traverseStackFrameIndex--;
                    }
                }
            }

            return info;
        }

        public static string GetStackFrameContents()
        {
            StringBuilder result = new StringBuilder();
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
            string callingMethodName = string.Empty;
            ModuleInformation info = new ModuleInformation();
            foreach (System.Diagnostics.StackFrame frame in stackTrace.GetFrames())
            {
                //if ((!frame.GetMethod().DeclaringType.FullName.ToLower().StartsWith("Dimension15.services.logging")
                //    && !frame.GetMethod().DeclaringType.FullName.ToLower().StartsWith("microsoft.practices.enterpriseLibrary.logging"))
                //    && (!frame.GetMethod().DeclaringType.FullName.ToLower().Contains(".logging")))
                {
                    result.AppendLine(frame.GetMethod().DeclaringType.FullName + ":" + frame.GetMethod().Name + ", " + frame.GetMethod().DeclaringType.Assembly.Location);
                }
            }

            return result.ToString();
        }


    }

    public class ModuleInformation
    {
        public string DeclaringType { get; set; }
        public string MethodName { get; set; }
    }
}
