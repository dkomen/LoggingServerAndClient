﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Dimension15.Services.Logging.Interfaces
{
    /// <summary>
    /// An log entry item that can be added to the logging system
    /// </summary>
    /// <author name='Dean Komen' date='2012/03/29' />    
    public interface ILogEntry :Interfaces.ILogEntryBase, DORM.Interfaces.IEntity
    {
        
        /// <summary>
        /// The datetime stamp of event
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        System.DateTime? DateStamp { get; }
        /// <summary>
        /// The unique ID of this log event
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        /// <revision name='Dean Komen' date='2012/10/31'>
        /// Changed from LogId to Id as defined by Dimension15.Dal.Interfaces.IEntity interface
        /// </revision>
        long Id { get; set; }        
    }
}
