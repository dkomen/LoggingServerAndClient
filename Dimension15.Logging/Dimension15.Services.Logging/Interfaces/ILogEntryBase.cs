﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging.Interfaces
{
    /// <summary>
    /// An log entry item that can be added to the logging system or used for report filtering purposes
    /// </summary>
    /// <author name='Dean Komen' date='2012/03/29' />
    public interface ILogEntryBase
    {        
        /// <summary>
        /// The severity of the event
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        Logging.Enumerations.LogSeverity? LogSeverity { get; set; }
        /// <summary>
        /// The name of the system from which the event was logged
        /// </summary>
        /// <example>
        /// RPI Submission engine/Collect business layer/ etc
        /// </example>
        /// <author name='Dean Komen' date='2012/03/29' />
        int? SystemID { get; set; }
        /// <summary>
        /// The name of the Module from which the event was logged
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        string ModuleName { get; set; }
        /// <summary>
        /// The name of the function from which the event was logged
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        string FunctionName { get; set; }
        /// <summary>
        /// The ID of the thread in which this event was logged
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        int? ThreadID { get; set; }
        /// <summary>
        /// A value that may be used to group multiple logs.
        /// </summary>
        /// <example>
        /// ClientID when wishing to trace the flow of all logs related to a specific client
        /// </example>
        /// <author name='Dean Komen' date='2012/03/29' />
        string TraceID { get; set; }
        /// <summary>
        /// The log message
        /// </summary>
        /// <author name='Dean Komen' date='2012/03/29' />
        string Message { get; set; }
    }
}
