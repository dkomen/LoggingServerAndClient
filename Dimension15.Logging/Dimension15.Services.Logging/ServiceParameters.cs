﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Services.Logging
{
    public class ServiceParameters
    {
        #region Properties
        public int ListeningPortNumber { get; set; }
        public string ServiceName { get; set; }
        #endregion
    }
}
