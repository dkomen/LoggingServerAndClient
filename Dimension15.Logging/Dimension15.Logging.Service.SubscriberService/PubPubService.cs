﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Dimension15.Logging.Service.SubscriberService
{

    public class ServiceEventArgs : EventArgs
    {
        public ObjectToPass Pass;
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class PubPubService: IPubSubService
    {
        public delegate void NewLogEventHandler(object sender, ServiceEventArgs e);
        public static event NewLogEventHandler NewLogEvent;

        IPubSubContract ServiceCallback = null;
        NewLogEventHandler NewLogHandler = null;

        public void Subscribe()
        {
            ServiceCallback = OperationContext.Current.GetCallbackChannel<IPubSubContract>();
            NewLogHandler = new NewLogEventHandler(PublishNewLogHandler);
            NewLogEvent += NewLogHandler;
        }

        public void Unsubscribe()
        {
            NewLogEvent -= NewLogHandler;
        }

        public void PublishNewLog(ObjectToPass pass)
        {
            if (NewLogEvent != null)
            {
                ServiceEventArgs se = new ServiceEventArgs();
                se.Pass = pass;
                NewLogEvent(this, se);
            }
        }
        public void PublishNewLogHandler(object sender, ServiceEventArgs se)
        {
            try
            {
                ServiceCallback.NewLog(se.Pass);
            }
            catch 
            {
                Unsubscribe();//DeanK: A dead service client died or did not unsubsrcibe.. so we will do the unsubscribe ourselffffffs
            }
        }
    }
}
