﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

namespace Dimension15.Logging.Service.SubscriberService
{
    public class Service
    {
        #region Fields
        private static bool _stop = false;
        private static Service _instance = null;
        private static object _threadLocker = new object();
        private static ServiceHost _host = null;
        #endregion

        #region Constructors
        private Service(string netTcpUrl)
        {
            Console.ForegroundColor = ConsoleColor.Gray;

            Uri baseTcpAddress = new Uri(netTcpUrl);            
            try
            {
                _host = new ServiceHost(typeof(PubPubService), baseTcpAddress);
                NetTcpBinding netTcpBinding = new NetTcpBinding();
                netTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
                netTcpBinding.ReliableSession = new OptionalReliableSession();
                netTcpBinding.ReliableSession.Ordered = true;
                netTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

                _host.AddServiceEndpoint(typeof(IPubSubService), netTcpBinding, baseTcpAddress);

                Console.WriteLine("---------------------------------------------------------");
                Console.WriteLine("Start subscriber service listening on tcp channel with uri: " + baseTcpAddress.ToString());
                _host.Open();
                Console.WriteLine("Subscriber service is up and running");
                Console.WriteLine("---------------------------------------------------------");
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error starting subscriber service: " + ex.ToString());
                if (_host != null)
                {
                    _host.Close();
                }
                throw;
            }
            finally
            {
                Console.WriteLine("---------------------------------------------------------");
            }                        
        }
        #endregion

        public static void Start(string netTcpUrl)
        {
            if (_instance == null)
            {
                lock (_threadLocker)
                {
                    _instance = new Service(netTcpUrl);
                }
            }
            else
            {
                throw new Exception("Can't start listening as an instance of the subscriber service class is already active!");
            }
        }

        public static void Stop()
        {
            if(_instance!=null)
            {
                _host.Close();
            }
        }
    }
}
