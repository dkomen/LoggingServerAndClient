﻿using System;
namespace Dimension15.Logging.Service.SubscriberService
{
    public interface IObjectToPass
    {
        object CustomObject { get; set; }
    }
}
