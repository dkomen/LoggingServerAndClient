﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Dimension15.Logging.Service.SubscriberService
{

    public interface IPubSubContract
    {
        [OperationContract(IsOneWay = true)]
        void NewLog(ObjectToPass pass);
    }    

    [ServiceContract(Namespace = "http://ListPublishSubscribe.Service", SessionMode = SessionMode.Required, CallbackContract = typeof(IPubSubContract))]
    public interface IPubSubService
    {
        [OperationContract(IsOneWay = false, IsInitiating = true)]
        void Subscribe();
        [OperationContract(IsOneWay = false, IsInitiating = true)]
        void Unsubscribe();
        [OperationContract(IsOneWay = false)]
        void PublishNewLog(ObjectToPass pass);
    }
}
