﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Logging.Service.SubscriberService
{
    [Serializable]
    public class ObjectToPass : IObjectToPass
    {
        public object CustomObject { get; set; }
    }
}
