﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace Dimension15.Logging.Service.SubscriberService
{
    public delegate void EventCallbackDelegate(IObjectToPass pass);
    public class Client : IDisposable
    {        
        #region Fields
        public static event EventCallbackDelegate MyEventCallbackEvent;
        private EventCallbackDelegate _callbackMethod = null;
        private IPubSubService _contract = null;
        #endregion

        public Client(string netTcpUrl, EventCallbackDelegate callbackMethod)
        {
            if (netTcpUrl != string.Empty)
            {
                _callbackMethod = callbackMethod;

                InstanceContext context = new InstanceContext(new ServiceCallback());

                NetTcpBinding newTcpBinding = new NetTcpBinding();
                newTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
                newTcpBinding.ReliableSession = new OptionalReliableSession();
                newTcpBinding.ReliableSession.Ordered = true;
                newTcpBinding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;


                EndpointAddress endpointAddress = new EndpointAddress(netTcpUrl);
                ChannelFactory<IPubSubService> _channelFactory = new DuplexChannelFactory<IPubSubService>(context, newTcpBinding, endpointAddress);

                EventCallbackDelegate callbackHandler = new EventCallbackDelegate(Callback);
                MyEventCallbackEvent += callbackHandler;

                _contract = _channelFactory.CreateChannel(endpointAddress);

                _contract.Subscribe();
            }
        }

        [CallbackBehaviorAttribute(UseSynchronizationContext = false, ConcurrencyMode = ConcurrencyMode.Multiple)]
        public class ServiceCallback : IPubSubContract  
        {
            public void NewLog(ObjectToPass pass)
            {
                Client.MyEventCallbackEvent(pass);
            }
        }

        public void Callback(IObjectToPass pass)
        {
            if (_callbackMethod != null)
            {
                _callbackMethod(pass);
            }
        }

        public void Publish(Dimension15.Logging.Service.SubscriberService.ObjectToPass pass)
        {
            if (_contract != null)
            {
                _contract.PublishNewLog(pass);
            }
        }

        #region IDisposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_contract!=null)
                    {
                        try
                        {
                            _contract.Unsubscribe();
                            _contract = null;
                        }
                        catch{}
                    }
                }
                _disposed = true;
            }
        }
        #endregion
    }
}

