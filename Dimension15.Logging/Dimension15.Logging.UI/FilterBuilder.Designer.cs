﻿namespace Dimension15.Logging.UI
{
    partial class FilterBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.uxFilterFields = new System.Windows.Forms.ListBox();
            this.uxAddFilterField = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.uxFilterFieldValues = new System.Windows.Forms.ListBox();
            this.Cancel = new System.Windows.Forms.Button();
            this.uxOk = new System.Windows.Forms.Button();
            this.uxDeleteFilterField = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.uxFilterFields);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(317, 323);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filter field";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // uxFilterFields
            // 
            this.uxFilterFields.FormattingEnabled = true;
            this.uxFilterFields.HorizontalScrollbar = true;
            this.uxFilterFields.Location = new System.Drawing.Point(6, 19);
            this.uxFilterFields.Name = "uxFilterFields";
            this.uxFilterFields.Size = new System.Drawing.Size(305, 290);
            this.uxFilterFields.TabIndex = 0;
            this.uxFilterFields.Click += new System.EventHandler(this.uxFilterFields_Click);
            // 
            // uxAddFilterField
            // 
            this.uxAddFilterField.Location = new System.Drawing.Point(335, 117);
            this.uxAddFilterField.Name = "uxAddFilterField";
            this.uxAddFilterField.Size = new System.Drawing.Size(75, 23);
            this.uxAddFilterField.TabIndex = 1;
            this.uxAddFilterField.Text = "-->";
            this.uxAddFilterField.UseVisualStyleBackColor = true;
            this.uxAddFilterField.Click += new System.EventHandler(this.uxAddFilterField_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.uxFilterFieldValues);
            this.groupBox2.Location = new System.Drawing.Point(416, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(317, 323);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filter field values";
            // 
            // uxFilterFieldValues
            // 
            this.uxFilterFieldValues.FormattingEnabled = true;
            this.uxFilterFieldValues.HorizontalScrollbar = true;
            this.uxFilterFieldValues.Location = new System.Drawing.Point(6, 19);
            this.uxFilterFieldValues.Name = "uxFilterFieldValues";
            this.uxFilterFieldValues.Size = new System.Drawing.Size(305, 290);
            this.uxFilterFieldValues.TabIndex = 0;
            this.uxFilterFieldValues.Click += new System.EventHandler(this.uxFilterFieldValues_Click);
            this.uxFilterFieldValues.SelectedIndexChanged += new System.EventHandler(this.uxFilterFieldValues_SelectedIndexChanged);
            this.uxFilterFieldValues.DoubleClick += new System.EventHandler(this.uxFilterFieldValues_DoubleClick);
            this.uxFilterFieldValues.KeyDown += new System.Windows.Forms.KeyEventHandler(this.uxFilterFieldValues_KeyDown);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(658, 345);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 7;
            this.Cancel.Text = "&Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // uxOk
            // 
            this.uxOk.Location = new System.Drawing.Point(12, 345);
            this.uxOk.Name = "uxOk";
            this.uxOk.Size = new System.Drawing.Size(75, 23);
            this.uxOk.TabIndex = 6;
            this.uxOk.Text = "&Ok";
            this.uxOk.UseVisualStyleBackColor = true;
            this.uxOk.Click += new System.EventHandler(this.uxOk_Click);
            // 
            // uxDeleteFilterField
            // 
            this.uxDeleteFilterField.Location = new System.Drawing.Point(335, 151);
            this.uxDeleteFilterField.Name = "uxDeleteFilterField";
            this.uxDeleteFilterField.Size = new System.Drawing.Size(75, 23);
            this.uxDeleteFilterField.TabIndex = 8;
            this.uxDeleteFilterField.Text = "<--";
            this.uxDeleteFilterField.UseVisualStyleBackColor = true;
            this.uxDeleteFilterField.Click += new System.EventHandler(this.uxDeleteFilterField_Click);
            // 
            // FilterBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(746, 380);
            this.Controls.Add(this.uxDeleteFilterField);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.uxOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.uxAddFilterField);
            this.Controls.Add(this.groupBox1);
            this.Name = "FilterBuilder";
            this.Text = "FilterBuilder";
            this.Load += new System.EventHandler(this.FilterBuilder_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox uxFilterFields;
        private System.Windows.Forms.Button uxAddFilterField;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox uxFilterFieldValues;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Button uxOk;
        private System.Windows.Forms.Button uxDeleteFilterField;
    }
}