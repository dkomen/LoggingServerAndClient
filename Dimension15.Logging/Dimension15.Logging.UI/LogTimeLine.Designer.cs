﻿namespace Dimension15.Logging.UI
{
    partial class LogTimeLine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.uiCanvas = new System.Windows.Forms.PictureBox();
            this.uiCurrentTimeMarker = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiCanvas)).BeginInit();
            this.SuspendLayout();
            // 
            // uiCanvas
            // 
            this.uiCanvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uiCanvas.BackColor = System.Drawing.Color.Black;
            this.uiCanvas.Location = new System.Drawing.Point(0, 0);
            this.uiCanvas.Name = "uiCanvas";
            this.uiCanvas.Size = new System.Drawing.Size(749, 56);
            this.uiCanvas.TabIndex = 0;
            this.uiCanvas.TabStop = false;
            this.uiCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.uiCanvas_Paint);
            this.uiCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.uiCanvas_MouseMove);
            // 
            // uiCurrentTimeMarker
            // 
            this.uiCurrentTimeMarker.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiCurrentTimeMarker.AutoSize = true;
            this.uiCurrentTimeMarker.Location = new System.Drawing.Point(4, 59);
            this.uiCurrentTimeMarker.Name = "uiCurrentTimeMarker";
            this.uiCurrentTimeMarker.Size = new System.Drawing.Size(49, 13);
            this.uiCurrentTimeMarker.TabIndex = 1;
            this.uiCurrentTimeMarker.Text = "00:00:00";
            // 
            // LogTimeLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.uiCurrentTimeMarker);
            this.Controls.Add(this.uiCanvas);
            this.Name = "LogTimeLine";
            this.Size = new System.Drawing.Size(749, 74);
            this.Load += new System.EventHandler(this.LogTimeLine_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiCanvas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox uiCanvas;
        private System.Windows.Forms.Label uiCurrentTimeMarker;
    }
}
