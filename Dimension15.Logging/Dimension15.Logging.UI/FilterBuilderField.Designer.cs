﻿namespace Dimension15.Logging.UI
{
    partial class FilterBuilderField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uxName = new System.Windows.Forms.TextBox();
            this.uxValue = new System.Windows.Forms.TextBox();
            this.uxOk = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.uxEnumOptions = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Field name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Value";
            // 
            // uxName
            // 
            this.uxName.Location = new System.Drawing.Point(76, 12);
            this.uxName.Name = "uxName";
            this.uxName.ReadOnly = true;
            this.uxName.Size = new System.Drawing.Size(367, 20);
            this.uxName.TabIndex = 2;
            // 
            // uxValue
            // 
            this.uxValue.Location = new System.Drawing.Point(76, 38);
            this.uxValue.Name = "uxValue";
            this.uxValue.Size = new System.Drawing.Size(367, 20);
            this.uxValue.TabIndex = 3;
            // 
            // uxOk
            // 
            this.uxOk.Location = new System.Drawing.Point(13, 67);
            this.uxOk.Name = "uxOk";
            this.uxOk.Size = new System.Drawing.Size(75, 23);
            this.uxOk.TabIndex = 4;
            this.uxOk.Text = "&Ok";
            this.uxOk.UseVisualStyleBackColor = true;
            this.uxOk.Click += new System.EventHandler(this.uxOk_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(368, 67);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 5;
            this.Cancel.Text = "&Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // uxEnumOptions
            // 
            this.uxEnumOptions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uxEnumOptions.FormattingEnabled = true;
            this.uxEnumOptions.Location = new System.Drawing.Point(90, 38);
            this.uxEnumOptions.Name = "uxEnumOptions";
            this.uxEnumOptions.Size = new System.Drawing.Size(367, 21);
            this.uxEnumOptions.TabIndex = 6;
            this.uxEnumOptions.SelectedIndexChanged += new System.EventHandler(this.uxEnumOptions_SelectedIndexChanged);
            // 
            // FilterBuilderField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 102);
            this.Controls.Add(this.uxEnumOptions);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.uxOk);
            this.Controls.Add(this.uxValue);
            this.Controls.Add(this.uxName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FilterBuilderField";
            this.Text = "Filter field";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox uxName;
        private System.Windows.Forms.TextBox uxValue;
        private System.Windows.Forms.Button uxOk;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.ComboBox uxEnumOptions;
    }
}