﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dimension15.Logging.UI
{
    public partial class FilterBuilder : Form
    {

        #region Fields
        public Dimension15.Services.Logging.RetrievalFilter filterItems = new Dimension15.Services.Logging.RetrievalFilter();
        #endregion

        public FilterBuilder()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void FilterBuilder_Load(object sender, EventArgs e)
        {
            List<Dimension15.Services.Logging.FieldType> filterFields = Dimension15.Services.Logging.LogEntry.GetFilterFieldNames();
            foreach (Dimension15.Services.Logging.FieldType field in filterFields)
            {
                uxFilterFields.Items.Add(field);
            }
        }

        private void uxAddFilterField_Click(object sender, EventArgs e)
        {
            FilterBuilderField builderField = new FilterBuilderField();
            //System.Reflection.PropertyInfo propertyType = typeof()
            builderField.Field = new FilterBuilderFieldItem() { Name = uxFilterFields.SelectedItem.ToString(), Value = "", ValueType = ((Dimension15.Services.Logging.FieldType)uxFilterFields.SelectedItem).type };
            if (builderField.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                uxFilterFieldValues.Items.Add(builderField);

                Dimension15.Services.Logging.LogEntryBase filterItem = new Dimension15.Services.Logging.LogEntryBase();

                System.Reflection.PropertyInfo property = filterItem.GetType().GetProperty(builderField.Field.Name);
                Type propertyType = property.PropertyType;

                if (propertyType.BaseType == typeof(ValueType)) //Is numeric
                {
                    if (IsNullableEnum(propertyType))
                    {
                        object eo = Enum.Parse(Nullable.GetUnderlyingType(propertyType), builderField.Field.Value.ToString());
                        property.SetValue(filterItem, eo, null);
                    }
                    else
                    {
                        property.SetValue(filterItem, int.Parse(builderField.Field.Value.ToString()), null);
                    }
                }
                else if (propertyType == typeof(string)) //Is string
                {
                    property.SetValue(filterItem, builderField.Field.Value.ToString(), null);
                }

                filterItems.AddFilterEntry(filterItem);
                
                SetCurrentFieldValueList(builderField.Field.Name);
            }
        }

        public static bool IsNullableEnum(Type t)
        {
            return t.IsGenericType &&
                   t.GetGenericTypeDefinition() == typeof(Nullable<>) &&
                   t.GetGenericArguments()[0].IsEnum;
        }

        private void SetCurrentFieldValueList(string fieldName)
        {
            uxFilterFieldValues.Items.Clear();
            foreach (Dimension15.Services.Logging.LogEntryBase item in filterItems.FilterItems)
            {
                System.Reflection.PropertyInfo field = item.GetType().GetProperty(fieldName);
                if (field.GetValue(item, null) != null)
                {
                    uxFilterFieldValues.Items.Add(item);
                }
            }
        }

        private void uxFilterFields_Click(object sender, EventArgs e)
        {
            SetCurrentFieldValueList(uxFilterFields.SelectedItem.ToString());
        }

        private void uxOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Visible = false;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Visible = false;
        }

        private void uxFilterFieldValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void uxFilterFieldValues_Click(object sender, EventArgs e)
        {
            
        }

        private void uxDeleteFilterField_Click(object sender, EventArgs e)
        {
            FilterBuilderField builderField = new FilterBuilderField();
            Dimension15.Services.Logging.LogEntryBase filterItem = (Dimension15.Services.Logging.LogEntryBase)uxFilterFieldValues.SelectedItem;
            if (filterItem != null)
            {
                filterItems.FilterItems.Remove(filterItem);
                uxFilterFieldValues.Items.Remove(uxFilterFieldValues.SelectedItem);
            }
        }

        private void uxFilterFieldValues_DoubleClick(object sender, EventArgs e)
        {
            if (uxFilterFieldValues.SelectedItem != null)
            {
                FilterBuilderField builderField = new FilterBuilderField();
                Dimension15.Services.Logging.LogEntryBase filterItem = (Dimension15.Services.Logging.LogEntryBase)uxFilterFieldValues.SelectedItem;
                System.Reflection.PropertyInfo property = filterItem.GetType().GetProperty(uxFilterFields.SelectedItem.ToString());

                builderField.Field = new FilterBuilderFieldItem() { Name = property.Name, Value = property.GetValue(filterItem, null), ValueType = property.PropertyType };

                if (builderField.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    property.SetValue(filterItem, builderField.Field.Value, null);
                    SetCurrentFieldValueList(builderField.Field.Name);
                }
            }
        }

        private void uxFilterFieldValues_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                uxDeleteFilterField_Click(null,null);
                e.Handled = true;
            }
        }

    }
}
