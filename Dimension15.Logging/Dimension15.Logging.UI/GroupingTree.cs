﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.CSharp;
using System.CodeDom.Compiler;
using System.Reflection;
using System.Reflection.Emit;

namespace Dimension15.Logging.UI
{
    public partial class GroupingTree : Form
    {
        #region Fields
        private List<Dimension15.Services.Logging.LogEntry> _logEntries = null;
        private MVP.LogView _presenter = null;
        #endregion

        #region Properties
        public List<Dimension15.Services.Logging.LogEntry> LogEntries
        {
            set
            {
                _logEntries = value;
                uxRegenerate_Click(null, null);
            }
        }
        #endregion

        public GroupingTree()
        {
            InitializeComponent();

            _presenter = new MVP.LogView(TreeOfGroups, uiCollapseExpandeAll);

            #region Group on custom text
            string sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """"; 
    string searchText = ""error""; //THE TEXT TO SEARCH FOR! Example, 'error'
    if (textToSearchForMatch.ToLower().Contains(searchText)) // found key
    {
        
        key = searchText;
        
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on custom text", SourceCode = sourceCode });
            #endregion            

            #region Group on TraceId
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(""<tId>.*</tId>"");
    System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
    if (match.Success) // found key
    {
        
        key = match.Value;
        
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on TraceId", SourceCode = sourceCode });
            #endregion

            #region Group on Thread Id
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(""<threadId>.*</threadId>"");
    System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
    if (match.Success) // found key
    {
        
        key = match.Value;
        
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on Thread Id", SourceCode = sourceCode });
            #endregion

            #region Group on Assembly name
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(""<mod>.*</mod>"");
    System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
    if (match.Success) // found key
    {
        
        key = match.Value;
        
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on Assembly name", SourceCode = sourceCode });
            #endregion

            #region Group on Function name
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(""<-.*->"");
    System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
    if (match.Success) // found key
    {
        
        key = match.Value;
        
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on Function name", SourceCode = sourceCode });
            #endregion

            #region Group on all possible errors
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    if(textToSearchForMatch.Contains(""critical"") || textToSearchForMatch.Contains(""error"") || textToSearchForMatch.Contains(""warning""))
    {
       key=""Errors"";
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on all possible errors", SourceCode = sourceCode });
            #endregion

            #region Group on error types
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    if(textToSearchForMatch.ToLower().Contains(""critical""))
    {
       key=""Critical"";
    }
    if(textToSearchForMatch.ToLower().Contains(""error""))
    {
       key=""Error"";
    }
    if(textToSearchForMatch.ToLower().Contains(""warning""))
    {
       key=""Warning"";
    }
    return key;
}";
            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "Group on error types", SourceCode = sourceCode });
            #endregion

            #region RPI Instruction flow
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{
    string key = """";
    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(""(key:[ ]{0,1}[0-9]+)|(Id:[ ]{0,1}[0-9]+)"");
    System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
    if (match.Success) // found key
    {
        regex = new System.Text.RegularExpressions.Regex(""[0-9]+"");
        match = regex.Match(match.Value);
        if (match.Success)
        {
            key = match.Value;
        }
    }
    return key;
}";

            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "(Custom)RPI Instruction flow", SourceCode = sourceCode });
            #endregion

            #region RPI responses received from FNB
            sourceCode = @"//Method must have signature as: public static string DoGroupBuilding(string textToSearchForMatch)
// The return value is the value on which to group each log entry
public static string DoGroupBuilding(string textToSearchForMatch)
{    
    string key = """";
    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(""Response: .*[,]"");
    System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
    if (match.Success) // found key
    {
        
        key = match.Value;
        
    }
    return key;
}";

            uiPredefinedGroupings.Items.Add(new Grouping() { Name = "(Custom)RPI responses received from FNB", SourceCode = sourceCode });
            #endregion

            uiPredefinedGroupings.SelectedIndex = 3;


        }

        private void GroupingTree_Load(object sender, EventArgs e)
        {

        }

        #region Private Functions
        private void BuildGroupingTree(TreeView treeView, List<Dimension15.Services.Logging.LogEntry> items)
        {
            treeView.BackColor = System.Drawing.Color.FromArgb(230, 230, 210); ;// System.Drawing.Color.FromArgb(20, 20, 10);
            treeView.ForeColor = System.Drawing.Color.Green;// System.Drawing.Color.Green;
            treeView.Font = new System.Drawing.Font("Courier New", 8, FontStyle.Regular);
            treeView.Visible = false;
            treeView.Nodes.Clear();

            CompilerRunner compileRunner = null;
            Dictionary<string, List<Dimension15.Services.Logging.LogEntry>> groupingTree = new Dictionary<string, List<Dimension15.Services.Logging.LogEntry>>();
            foreach (Dimension15.Services.Logging.LogEntry logEntry in items)
            {
                string groupValue = MatchForGrouping("<-" + logEntry.FunctionName + "-> " + logEntry.LogSeverity.ToString() + " " + logEntry.Message + " " + logEntry.LogSeverity + " <mod>" + logEntry.ModuleName + "</mod> " + logEntry.SystemID + " <tId>" + logEntry.TraceID + "</tId> <threadId>" + logEntry.ThreadID + "</threadId>", ref compileRunner);
                if (groupValue != string.Empty)
                {
                    if (!groupingTree.ContainsKey(groupValue))
                    {
                        groupingTree.Add(groupValue, new List<Dimension15.Services.Logging.LogEntry>());
                    }
                    groupingTree[groupValue].Add(logEntry);
                }
            }

            foreach(KeyValuePair<string, List<Dimension15.Services.Logging.LogEntry>> group in groupingTree)
            {
                TreeNode rootNode = new TreeNode(group.Key);
                int lineCounter=0;
                foreach (Dimension15.Services.Logging.LogEntry logEntry in group.Value)
                {
                    lineCounter++;
                    rootNode.Nodes.Add(TreeNodeBuilder.CreateNode(lineCounter, logEntry));
                }
                treeView.Nodes.Add(rootNode);
            }

            uiCollapseExpandeAll_Click(null,null);


            treeView.Visible = true;
        }

        public class NotDynamicClass
        {
            public void SearchForMatch(string textToSearchForMatch)
            {
                
            }
        }

        private string GetCodeToCompile()
        {
            return @"using System;
                    using System.Text;
                      public class GroupingFunctions
                    {
                        
                       " + uxGroupingCode.Text + @"
                    }";
        }

        private string MatchForGrouping(string textToSearchForMatch, ref CompilerRunner compileRunner)
        {
            try
            {

                string code = GetCodeToCompile();


                bool compiled = true;
                if (compileRunner == null)
                {
                    compileRunner = new CompilerRunner();
                    compiled = compileRunner.Compile(code);
                    if (!compiled)
                    {
                        compileRunner = null;
                    }
                }
                if (compiled)
                {
                    return compileRunner.Run("GroupingFunctions", "DoGroupBuilding", new[] { textToSearchForMatch }).ToString();
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return DoGroupBuilding(textToSearchForMatch);
        }

        public string DoGroupBuilding(string textToSearchForMatch)
        {
            string key = string.Empty;
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("(key:[ ]{0,1}[0-9]+)|(Id:[ ]{0,1}[0-9]+)");
            System.Text.RegularExpressions.Match match = regex.Match(textToSearchForMatch);
            if (match.Success) // found key
            {
                regex = new System.Text.RegularExpressions.Regex("[0-9]+");
                match = regex.Match(match.Value);
                if (match.Success)
                {
                    key = match.Value;
                }
            }
            return key;
        }

        #endregion

        private void uxRegenerate_Click(object sender, EventArgs e)
        {

            CompilerRunner compileRunner = new CompilerRunner();
            bool compiled = compileRunner.Compile(GetCodeToCompile());
            if (!compiled)
            {
                MessageBox.Show("The given sourcecode cant compile!", "Compilation error");
            }
            else
            {
                BuildGroupingTree(TreeOfGroups, _logEntries);
                this.Text = "Grouping generated on: " + DateTime.Now.ToString("dd MMM yyyy hh:mm:ss");
            }
        }

        private void uiPredefinedGroupings_SelectedIndexChanged(object sender, EventArgs e)
        {
            uxGroupingCode.Text = ((Grouping)uiPredefinedGroupings.SelectedItem).SourceCode;
        }

        private void uiCollapseExpandeAll_Click(object sender, EventArgs e)
        {
            _presenter.CollapseNodes = !_presenter.CollapseNodes;
        }

        private void TreeOfGroups_AfterSelect(object sender, TreeViewEventArgs e)
        {
            uxSelectedNodeText.Text = TreeOfGroups.SelectedNode.Text;
        }
    }
    #region Classes
    class Grouping
    {
        public string Name { get; set; }
        public string SourceCode { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
    #endregion
}
