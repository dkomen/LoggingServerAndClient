﻿namespace Dimension15.Logging.UI
{
    partial class GroupingTree
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TreeOfGroups = new System.Windows.Forms.TreeView();
            this.uxGroupingCode = new System.Windows.Forms.TextBox();
            this.uxRegenerate = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.uxSelectedNodeText = new System.Windows.Forms.TextBox();
            this.uiCollapseExpandeAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.uiPredefinedGroupings = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TreeOfGroups
            // 
            this.TreeOfGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeOfGroups.HideSelection = false;
            this.TreeOfGroups.Location = new System.Drawing.Point(3, 15);
            this.TreeOfGroups.Name = "TreeOfGroups";
            this.TreeOfGroups.Size = new System.Drawing.Size(924, 360);
            this.TreeOfGroups.TabIndex = 0;
            this.TreeOfGroups.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeOfGroups_AfterSelect);
            // 
            // uxGroupingCode
            // 
            this.uxGroupingCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxGroupingCode.Location = new System.Drawing.Point(3, 35);
            this.uxGroupingCode.Multiline = true;
            this.uxGroupingCode.Name = "uxGroupingCode";
            this.uxGroupingCode.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.uxGroupingCode.Size = new System.Drawing.Size(924, 162);
            this.uxGroupingCode.TabIndex = 1;
            // 
            // uxRegenerate
            // 
            this.uxRegenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxRegenerate.Location = new System.Drawing.Point(13, 644);
            this.uxRegenerate.Name = "uxRegenerate";
            this.uxRegenerate.Size = new System.Drawing.Size(134, 23);
            this.uxRegenerate.TabIndex = 2;
            this.uxRegenerate.Text = "Regenerate groupings";
            this.uxRegenerate.UseVisualStyleBackColor = true;
            this.uxRegenerate.Click += new System.EventHandler(this.uxRegenerate_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Location = new System.Drawing.Point(13, 13);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.uxSelectedNodeText);
            this.splitContainer1.Panel1.Controls.Add(this.uiCollapseExpandeAll);
            this.splitContainer1.Panel1.Controls.Add(this.TreeOfGroups);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.uiPredefinedGroupings);
            this.splitContainer1.Panel2.Controls.Add(this.uxGroupingCode);
            this.splitContainer1.Size = new System.Drawing.Size(932, 625);
            this.splitContainer1.SplitterDistance = 423;
            this.splitContainer1.TabIndex = 3;
            // 
            // uxSelectedNodeText
            // 
            this.uxSelectedNodeText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uxSelectedNodeText.BackColor = System.Drawing.Color.Black;
            this.uxSelectedNodeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.uxSelectedNodeText.Location = new System.Drawing.Point(1, 372);
            this.uxSelectedNodeText.Multiline = true;
            this.uxSelectedNodeText.Name = "uxSelectedNodeText";
            this.uxSelectedNodeText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.uxSelectedNodeText.Size = new System.Drawing.Size(926, 47);
            this.uxSelectedNodeText.TabIndex = 20;
            // 
            // uiCollapseExpandeAll
            // 
            this.uiCollapseExpandeAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiCollapseExpandeAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiCollapseExpandeAll.Location = new System.Drawing.Point(814, 3);
            this.uiCollapseExpandeAll.Name = "uiCollapseExpandeAll";
            this.uiCollapseExpandeAll.Size = new System.Drawing.Size(98, 21);
            this.uiCollapseExpandeAll.TabIndex = 19;
            this.uiCollapseExpandeAll.Text = "Collapse nodes";
            this.uiCollapseExpandeAll.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.uiCollapseExpandeAll.UseVisualStyleBackColor = true;
            this.uiCollapseExpandeAll.Click += new System.EventHandler(this.uiCollapseExpandeAll_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Predefined groupings";
            // 
            // uiPredefinedGroupings
            // 
            this.uiPredefinedGroupings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.uiPredefinedGroupings.FormattingEnabled = true;
            this.uiPredefinedGroupings.Location = new System.Drawing.Point(116, 8);
            this.uiPredefinedGroupings.Name = "uiPredefinedGroupings";
            this.uiPredefinedGroupings.Size = new System.Drawing.Size(314, 21);
            this.uiPredefinedGroupings.TabIndex = 2;
            this.uiPredefinedGroupings.SelectedIndexChanged += new System.EventHandler(this.uiPredefinedGroupings_SelectedIndexChanged);
            // 
            // GroupingTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 672);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.uxRegenerate);
            this.Name = "GroupingTree";
            this.Text = "GroupingTree";
            this.Load += new System.EventHandler(this.GroupingTree_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView TreeOfGroups;
        private System.Windows.Forms.TextBox uxGroupingCode;
        private System.Windows.Forms.Button uxRegenerate;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox uiPredefinedGroupings;
        private System.Windows.Forms.Button uiCollapseExpandeAll;
        private System.Windows.Forms.TextBox uxSelectedNodeText;
    }
}