﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dimension15.Logging.UI
{
    public partial class FilterBuilderField : Form
    {

        #region Fields
        private FilterBuilderFieldItem _field;
        #endregion

        #region Properties
        public FilterBuilderFieldItem Field
        {
            get
            {
                return _field;
            }
            set
            {
                _field = value;
                uxName.Text = _field.Name;
                if (FilterBuilder.IsNullableEnum(_field.ValueType))
                {
                    uxValue.Visible = false;
                    uxEnumOptions.Visible = true;
                    uxEnumOptions.Left = uxValue.Left;
                    string[] enumItems = Enum.GetNames(_field.ValueType.GetGenericArguments()[0]);
                    uxEnumOptions.Items.AddRange(enumItems);
                    uxEnumOptions.SelectedIndex = 0;
                    _field.ValueType = _field.ValueType;
                    uxEnumOptions.SelectedItem = _field.Value.ToString();
                }
                else if (_field.Name.ToLower() == "systemid")
                {
                    uxValue.Visible = false;
                    uxEnumOptions.Visible = true;
                    uxEnumOptions.Left = uxValue.Left;
                    string[] enumItems = new string[LogView.AllSystemIds.Count];

                    int index = 0;
                    foreach (Dimension15.Services.Logging.LogPersistance.SystemId systemId in LogView.AllSystemIds)
                    {
                        enumItems[index] = systemId.Description + ":" + systemId.Id;
                        index++;
                    }

                    uxEnumOptions.Items.AddRange(enumItems);
                    uxEnumOptions.SelectedIndex = 0;
                    _field.ValueType = _field.ValueType;
                    uxEnumOptions.SelectedItem = _field.Value.ToString();
                }
                else
                {
                    uxEnumOptions.Visible = false;
                    uxValue.Text = _field.Value.ToString();
                }
            }
        }
        #endregion

        public FilterBuilderField()
        {
            InitializeComponent();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void uxOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            _field.Name = uxName.Text;
            if (uxEnumOptions.Visible)
            {
                if (uxEnumOptions.SelectedItem.ToString().Contains(":"))
                {
                    _field.Value = (uxEnumOptions.SelectedItem.ToString().Split(':'))[1];
                }
                else
                {
                    _field.Value = Enum.Parse(_field.ValueType.GetGenericArguments()[0], uxEnumOptions.SelectedItem.ToString());
                }
            }
            else
            {
                _field.Value = uxValue.Text;
            }
            this.Visible = false;
        }

        private void uxEnumOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            uxValue.Text = uxEnumOptions.SelectedItem.ToString();
        }
    }
}
