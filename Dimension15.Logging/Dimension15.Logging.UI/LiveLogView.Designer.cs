﻿namespace Dimension15.Logging.UI
{
    partial class LiveLogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uxSelectedNodeText = new System.Windows.Forms.TextBox();
            this.uxClose = new System.Windows.Forms.Button();
            this.uxStopStart = new System.Windows.Forms.Button();
            this.uxClearAll = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.uiIgnoredSystemIds = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.uiCollapseExpandeAll = new System.Windows.Forms.Button();
            this.TreeOfGroups = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addThisSystemIdToIgnoreListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uxToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // uxSelectedNodeText
            // 
            this.uxSelectedNodeText.BackColor = System.Drawing.Color.Black;
            this.uxSelectedNodeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSelectedNodeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.uxSelectedNodeText.Location = new System.Drawing.Point(0, 0);
            this.uxSelectedNodeText.Multiline = true;
            this.uxSelectedNodeText.Name = "uxSelectedNodeText";
            this.uxSelectedNodeText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.uxSelectedNodeText.Size = new System.Drawing.Size(982, 131);
            this.uxSelectedNodeText.TabIndex = 23;
            // 
            // uxClose
            // 
            this.uxClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uxClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uxClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxClose.ForeColor = System.Drawing.Color.Navy;
            this.uxClose.Location = new System.Drawing.Point(906, 578);
            this.uxClose.Name = "uxClose";
            this.uxClose.Size = new System.Drawing.Size(75, 22);
            this.uxClose.TabIndex = 24;
            this.uxClose.Text = "Close";
            this.uxClose.UseVisualStyleBackColor = true;
            this.uxClose.Click += new System.EventHandler(this.uxClose_Click);
            // 
            // uxStopStart
            // 
            this.uxStopStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxStopStart.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxStopStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxStopStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uxStopStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxStopStart.ForeColor = System.Drawing.Color.Navy;
            this.uxStopStart.Location = new System.Drawing.Point(0, 578);
            this.uxStopStart.Name = "uxStopStart";
            this.uxStopStart.Size = new System.Drawing.Size(75, 22);
            this.uxStopStart.TabIndex = 25;
            this.uxStopStart.Text = "Stop";
            this.uxStopStart.UseVisualStyleBackColor = true;
            this.uxStopStart.Click += new System.EventHandler(this.uxStopStart_Click);
            // 
            // uxClearAll
            // 
            this.uxClearAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxClearAll.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxClearAll.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxClearAll.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uxClearAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxClearAll.ForeColor = System.Drawing.Color.Navy;
            this.uxClearAll.Location = new System.Drawing.Point(81, 578);
            this.uxClearAll.Name = "uxClearAll";
            this.uxClearAll.Size = new System.Drawing.Size(75, 22);
            this.uxClearAll.TabIndex = 26;
            this.uxClearAll.Text = "Clear all";
            this.uxClearAll.UseVisualStyleBackColor = true;
            this.uxClearAll.Click += new System.EventHandler(this.uxClearAll_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.uxSelectedNodeText);
            this.splitContainer1.Size = new System.Drawing.Size(982, 572);
            this.splitContainer1.SplitterDistance = 437;
            this.splitContainer1.TabIndex = 28;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer2.Size = new System.Drawing.Size(982, 437);
            this.splitContainer2.SplitterDistance = 150;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.uiIgnoredSystemIds);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(150, 437);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ignored systems";
            // 
            // uiIgnoredSystemIds
            // 
            this.uiIgnoredSystemIds.BackColor = System.Drawing.Color.Black;
            this.uiIgnoredSystemIds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiIgnoredSystemIds.ForeColor = System.Drawing.Color.Yellow;
            this.uiIgnoredSystemIds.FormattingEnabled = true;
            this.uiIgnoredSystemIds.IntegralHeight = false;
            this.uiIgnoredSystemIds.Location = new System.Drawing.Point(3, 16);
            this.uiIgnoredSystemIds.Name = "uiIgnoredSystemIds";
            this.uiIgnoredSystemIds.Size = new System.Drawing.Size(144, 418);
            this.uiIgnoredSystemIds.TabIndex = 28;
            this.uxToolTip.SetToolTip(this.uiIgnoredSystemIds, "A quick filter of SystemId\'s to ignore (Double-Click an item to remove)");
            this.uiIgnoredSystemIds.DoubleClick += new System.EventHandler(this.uiIgnoredSystemIds_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.uiCollapseExpandeAll);
            this.groupBox2.Controls.Add(this.TreeOfGroups);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(828, 437);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Live log view";
            // 
            // uiCollapseExpandeAll
            // 
            this.uiCollapseExpandeAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiCollapseExpandeAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiCollapseExpandeAll.Location = new System.Drawing.Point(708, 3);
            this.uiCollapseExpandeAll.Name = "uiCollapseExpandeAll";
            this.uiCollapseExpandeAll.Size = new System.Drawing.Size(98, 21);
            this.uiCollapseExpandeAll.TabIndex = 24;
            this.uiCollapseExpandeAll.Text = "Collapse nodes";
            this.uiCollapseExpandeAll.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.uiCollapseExpandeAll.UseVisualStyleBackColor = true;
            this.uiCollapseExpandeAll.Click += new System.EventHandler(this.uiCollapseExpandeAll_Click);
            // 
            // TreeOfGroups
            // 
            this.TreeOfGroups.BackColor = System.Drawing.Color.Black;
            this.TreeOfGroups.ContextMenuStrip = this.contextMenuStrip1;
            this.TreeOfGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeOfGroups.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeOfGroups.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.TreeOfGroups.HideSelection = false;
            this.TreeOfGroups.LineColor = System.Drawing.Color.DarkGray;
            this.TreeOfGroups.Location = new System.Drawing.Point(3, 16);
            this.TreeOfGroups.Name = "TreeOfGroups";
            this.TreeOfGroups.Size = new System.Drawing.Size(822, 418);
            this.TreeOfGroups.TabIndex = 22;
            this.uxToolTip.SetToolTip(this.TreeOfGroups, "List of logs (Double-Click an item to add SystemId to ignore filter)");
            this.TreeOfGroups.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeOfGroups_AfterSelect);
            this.TreeOfGroups.DoubleClick += new System.EventHandler(this.TreeOfGroups_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addThisSystemIdToIgnoreListToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(239, 26);
            // 
            // addThisSystemIdToIgnoreListToolStripMenuItem
            // 
            this.addThisSystemIdToIgnoreListToolStripMenuItem.Name = "addThisSystemIdToIgnoreListToolStripMenuItem";
            this.addThisSystemIdToIgnoreListToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.addThisSystemIdToIgnoreListToolStripMenuItem.Text = "Add this SystemId to ignore list";
            this.addThisSystemIdToIgnoreListToolStripMenuItem.Click += new System.EventHandler(this.addThisSystemIdToIgnoreListToolStripMenuItem_Click);
            // 
            // LiveLogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 605);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.uxClearAll);
            this.Controls.Add(this.uxStopStart);
            this.Controls.Add(this.uxClose);
            this.Name = "LiveLogView";
            this.Text = "Live log feed";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LiveLogView_FormClosing);
            this.Load += new System.EventHandler(this.LiveLogView_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox uxSelectedNodeText;
        private System.Windows.Forms.Button uxClose;
        private System.Windows.Forms.Button uxStopStart;
        private System.Windows.Forms.Button uxClearAll;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolTip uxToolTip;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox uiIgnoredSystemIds;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TreeView TreeOfGroups;
        private System.Windows.Forms.Button uiCollapseExpandeAll;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addThisSystemIdToIgnoreListToolStripMenuItem;
    }
}