﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Logging.UI.MVP
{
    public delegate void AddNewLogEntryDelegate(Services.Logging.LogEntry newLog);
    public class LogView : ILogView
    {
        #region Fields
        public bool _collapseNodes = true;
        public System.Windows.Forms.TreeView _logView = null;
        public System.Windows.Forms.Button _expandCollapseLogViewButton = null;
        public static object _threadLocker = new object();
        #endregion

        #region Constructors
        public LogView(System.Windows.Forms.TreeView logView, System.Windows.Forms.Button expandCollapseLogViewButton)
        {
            _logView = logView;
            _expandCollapseLogViewButton = expandCollapseLogViewButton;
        }
        #endregion

        /// <summary>
        /// Collapse or expand all the nodes in the log TreeView
        /// </summary>
        /// <author name='DKomen' date='2013/11/12' />
        public bool CollapseNodes
        {
            get
            {
                return _collapseNodes;
            }
            set
            {
                _collapseNodes = value;
                _logView.Visible = false;
                if (_collapseNodes)
                {
                    _logView.CollapseAll();
                    TextForExpandCollapseButton = "Expand nodes";
                }
                else
                {
                    _logView.ExpandAll();
                    TextForExpandCollapseButton = "Collapse nodes";
                }
                if (_logView.Nodes.Count != 0)
                {
                    _logView.Nodes[0].EnsureVisible();
                }
                _logView.Visible = true;
            }
        }

        /// <summary>
        /// Add a new log to the log TreeView
        /// </summary>
        /// <param name="newLog">The log to add</param>
        /// <param name="addNewLogEntryFunction">The delegate to the function that will do the actual adding to the log TreeView</param>
        /// <author name='DKomen' date='2013/11/12' />
        public void AddNewLog(Services.Logging.LogEntry newLog, AddNewLogEntryDelegate addNewLogEntryFunction)
        {
            lock (_threadLocker)
            {
                addNewLogEntryFunction(newLog);
            }
        }

        /// <summary>
        /// The text to display on the button that a user may click on to expand or collapse all the nodes of the log TreeView
        /// </summary>
        /// <author name='DKomen' date='2013/11/12' />
        public string TextForExpandCollapseButton 
        {
            set
            {
                _expandCollapseLogViewButton.Text = value;
            }
        }

        /// <summary>
        /// Export the current logView to file
        /// </summary>
        /// <remarks>
        /// Expects the original Log object which was added as nodes to the log TreeView to have also been added
        /// as the tag object of the individual nodes.
        /// </remarks>
        /// <author name='DKomen' date='2013/11/12' />
        public void ExportLogToFile()
        {
            System.Windows.Forms.SaveFileDialog saveFileDialogue = new System.Windows.Forms.SaveFileDialog();
            saveFileDialogue.CheckPathExists = true;
            if (saveFileDialogue.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StringBuilder logsInTextFormat = new StringBuilder();
                lock (_threadLocker)
                {
                    foreach (System.Windows.Forms.TreeNode logNode in _logView.Nodes)
                    {
                        logsInTextFormat.AppendLine(Dimension15.Services.Logging.LogEntry.ToRawString(((Dimension15.Services.Logging.LogEntry)logNode.Tag)));
                    }
                }
                System.IO.File.WriteAllText(saveFileDialogue.FileName, logsInTextFormat.ToString());
            }
        }

        /// <summary>
        /// Remove all nodes from the view with the supplied SystemId
        /// </summary>
        /// <param name="systemId"></param>
        /// <author name='DKomen' date='2013/11/12' />
        public void RemoveAllNodesWith_SystemId(string systemId)
        {
            lock (_threadLocker)
            {
                List<System.Windows.Forms.TreeNode> nodesToRemoveFromLogTreeView = new List<System.Windows.Forms.TreeNode>();
                foreach (System.Windows.Forms.TreeNode logNode in _logView.Nodes)
                {
                    if (((Dimension15.Services.Logging.LogEntry)logNode.Tag).SystemID.ToString() == systemId)
                    {
                        nodesToRemoveFromLogTreeView.Add(logNode);
                    }
                }

                foreach (System.Windows.Forms.TreeNode logNodeToRemove in nodesToRemoveFromLogTreeView)
                {
                    _logView.Nodes.Remove(logNodeToRemove);
                }
            }
        }
    }
}
