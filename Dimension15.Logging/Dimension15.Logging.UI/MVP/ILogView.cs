﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Logging.UI.MVP
{
    interface ILogView
    {
        /// <summary>
        /// Collapse or expand all the nodes in the log TreeView
        /// </summary>
        /// <author name='DKomen' date='2013/11/12' />
        bool CollapseNodes { get; set; }
        /// <summary>
        /// Add a new log to the log TreeView
        /// </summary>
        /// <param name="newLog">The log to add</param>
        /// <param name="addNewLogEntryFunction">The delegate to the function that will do the actual adding to the log TreeView</param>
        /// <author name='DKomen' date='2013/11/12' />
        void AddNewLog(Services.Logging.LogEntry newLog, AddNewLogEntryDelegate addNewLogEntryFunction);
        /// <summary>
        /// The text to display on the button that a user may click on to expand or collapse all the nodes of the log TreeView
        /// </summary>
        /// <author name='DKomen' date='2013/11/12' />
        string TextForExpandCollapseButton { set; }

        /// <summary>
        /// Export the current logView to file
        /// </summary>
        /// <remarks>
        /// Expects the original Log object which was added as nodes to the log TreeView to have also been added
        /// as the tag object of the individual nodes.
        /// </remarks>
        /// <author name='DKomen' date='2013/11/12' />
        void ExportLogToFile();

        /// <summary>
        /// Remove all nodes from the view with the supplied SystemId
        /// </summary>
        /// <param name="systemId"></param>
        /// <author name='DKomen' date='2013/11/12' />
        void RemoveAllNodesWith_SystemId(string systemId);
    }
}
