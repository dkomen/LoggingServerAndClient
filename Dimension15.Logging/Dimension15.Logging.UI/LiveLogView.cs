﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dimension15.Logging.UI
{
    public partial class LiveLogView : Form
    {
        #region Fields
        private bool _isRunning = true;
        private Dimension15.Logging.Service.SubscriberService.Client _subscriberServiceClient = null;
        private string _subscriberUrl = string.Empty;
        private MVP.LogView _presenter = null;
        #endregion

        #region Constructors
        public LiveLogView(string subscriberUrl)
        {
            InitializeComponent();

            _presenter = new MVP.LogView(TreeOfGroups, uiCollapseExpandeAll);

            uiCollapseExpandeAll.Tag = false;
            TreeOfGroups.BackColor = System.Drawing.Color.FromArgb(230, 230, 210); ;// System.Drawing.Color.FromArgb(20, 20, 10);
            TreeOfGroups.ForeColor = System.Drawing.Color.Green;// System.Drawing.Color.Green;
            TreeOfGroups.Font = new System.Drawing.Font("Courier New", 8, FontStyle.Regular);
            _subscriberUrl = subscriberUrl;
        }
        #endregion

        #region Form Events
        private void LiveLogView_Load(object sender, EventArgs e)
        {
            try
            {
                _subscriberServiceClient = new Dimension15.Logging.Service.SubscriberService.Client(_subscriberUrl, new Dimension15.Logging.Service.SubscriberService.EventCallbackDelegate(LiveLogHandler));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                if (ex.InnerException != null)
                {
                    MessageBox.Show(ex.InnerException.ToString());
                }
            }
        }       

        private void uiCollapseExpandeAll_Click(object sender, EventArgs e)
        {
            _presenter.CollapseNodes = !_presenter.CollapseNodes;
        }
        
        private void LiveLogView_FormClosing(object sender, FormClosingEventArgs e)
        {
            _subscriberServiceClient.Dispose();
        }
        
        private void uxStopStart_Click(object sender, EventArgs e)
        {
            _isRunning = !_isRunning;
            if (_isRunning)
            {
                uxStopStart.Text = "Stop";
            }
            else
            {
                uxStopStart.Text = "Start";
            }            
        }

        private void uxClose_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void uxClearAll_Click(object sender, EventArgs e)
        {
            TreeOfGroups.Nodes.Clear();
        }

        private void uiIgnoredSystemIds_DoubleClick(object sender, EventArgs e)
        {
            uiIgnoredSystemIds.Items.Remove(uiIgnoredSystemIds.SelectedItem);
        }

        #region TreeView events
        private void TreeOfGroups_AfterSelect(object sender, TreeViewEventArgs e)
        {
            uxSelectedNodeText.Text = TreeOfGroups.SelectedNode.Text;
        }

        private void TreeOfGroups_DoubleClick(object sender, EventArgs e)
        {
            if (!uiIgnoredSystemIds.Items.Contains(TreeOfGroups.SelectedNode.Tag.ToString())) 
            {
                if (TreeOfGroups.SelectedNode.Tag.GetType() == typeof(Dimension15.Services.Logging.LogEntry))
                {
                    uiIgnoredSystemIds.Items.Add(((Dimension15.Services.Logging.LogEntry)TreeOfGroups.SelectedNode.Tag).SystemID);
                }
            }
        }
        #endregion

        #endregion        

        #region Private Functions
        /// <summary>
        /// Is triggered when a new log to add to the log TreeView arrives from the remote Wcf server to which we have subscribed
        /// </summary>
        /// <param name="pass"></param>
        static int _pauseCounter = 0;
        static object _pauseLocker = "_pauseLocker";
        public void LiveLogHandler(Dimension15.Logging.Service.SubscriberService.IObjectToPass pass)
        {
            try
            {
                if (_isRunning)
                {
                    lock (_pauseLocker)
                    {
                        Dimension15.Services.Logging.LogEntry log = (Dimension15.Services.Logging.LogEntry)Dimension15.Services.Logging.Serializer.Base64Deserialization(pass.CustomObject.ToString());
                        if (!uiIgnoredSystemIds.Items.Contains(log.SystemID.ToString()))
                        {
                            _presenter.AddNewLog(log, AddNewLog);
                        }
                        _pauseCounter++;
                        if (_pauseCounter > 100)
                        {
                            System.Threading.Thread.Sleep(750);
                            _pauseCounter = 0;
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        private void AddNewLog(Dimension15.Services.Logging.LogEntry newLog)
        {
            if (!uiIgnoredSystemIds.Items.Contains(newLog.SystemID))
            {
                int lineCounter = TreeOfGroups.Nodes.Count + 1;
                TreeOfGroups.Invoke((MethodInvoker)(() =>
                {
                    TreeOfGroups.Nodes.Add(TreeNodeBuilder.CreateNode(lineCounter, newLog));
                    TreeOfGroups.Nodes[lineCounter - 1].Tag = newLog;
                    _presenter.CollapseNodes = _presenter.CollapseNodes;
                    TreeOfGroups.Nodes[lineCounter - 1].EnsureVisible();
                    #region Dont allow more than 1000 items in list
                    if (TreeOfGroups.Nodes.Count > 5000)
                    {
                        TreeOfGroups.Nodes.RemoveAt(0);
                    }
                    #endregion
                }));
            }
        }

        private void BuildGroupingTree(TreeView treeView, List<Dimension15.Services.Logging.LogEntry> items)
        {
            treeView.BackColor = System.Drawing.Color.FromArgb(230, 230, 210); ;// System.Drawing.Color.FromArgb(20, 20, 10);
            treeView.ForeColor = System.Drawing.Color.Green;// System.Drawing.Color.Green;
            treeView.Font = new System.Drawing.Font("Courier New", 8, FontStyle.Regular);
            treeView.Visible = false;
            treeView.Nodes.Clear();


            uiCollapseExpandeAll_Click(null, null);


            treeView.Visible = true;
        }
        #endregion        

        private void addThisSystemIdToIgnoreListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!uiIgnoredSystemIds.Items.Contains(TreeOfGroups.SelectedNode.Tag.ToString()))
            {
                if (TreeOfGroups.SelectedNode.Tag.GetType() == typeof(Dimension15.Services.Logging.LogEntry))
                {
                    uiIgnoredSystemIds.Items.Add(((Dimension15.Services.Logging.LogEntry)TreeOfGroups.SelectedNode.Tag).SystemID);
                }
            }
        }
    }
}

