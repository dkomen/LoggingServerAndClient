﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Dimension15.Services.Logging;

namespace Dimension15.Logging.UI
{
    public partial class LogView : Form
    {

        #region Fields
        private Dimension15.Services.Logging.RetrievalFilter _filter = new Dimension15.Services.Logging.RetrievalFilter();
        private List<Dimension15.Services.Logging.LogEntry> _retrievedLogEntries = new List<Services.Logging.LogEntry>();
        private LiveLogView _liveWindow = null;
        private MVP.LogView _presenter = null;
        #endregion

        #region Properties
        public string ServerUrl 
        {
            get
            {
                return uiServerUrl.Text;
            }
            private set
            {
                uiServerUrl.Text = value;
            }
        }
        public static List<Dimension15.Services.Logging.LogPersistance.SystemId> AllSystemIds { get; set; }  
        #endregion

        #region Constructors
        public LogView()
        {
            InitializeComponent();
            _presenter = new MVP.LogView(uxLogEntryReport, uiCollapseExpandeAll);
            _presenter.CollapseNodes = false;
        }
        #endregion

        #region Form events
        private void LogView_Load(object sender, EventArgs e)
        {            
            uxTimeStart.Value = uxTimeStart.Value.AddMinutes(-5);
            uxTimeEnd.Value = uxTimeEnd.Value.AddMinutes(60);
            ServerUrl = Prompt.ShowDialog("Enter the Uri to the logging server", "Logging server communication", Dimension15.Settings.Registry.LocalEnvironmentConfiguration.GetLoggingServiceEndPoint());

            try
            {
                //Get all systemId's
                Dimension15.Services.Logging.WcfService.ILoggingService service = Dimension15.Services.Logging.WcfService.ServiceClient.GetServiceClient(new System.ServiceModel.EndpointAddress(new Uri(ServerUrl)), true);
                AllSystemIds = service.GetAllSystemIds();

                foreach(Services.Logging.LogPersistance.SystemId systemId in AllSystemIds)
                {
                    ListOfAllSystemIds.Items.Add(systemId.ToString());
                }

            }
            catch (System.ServiceModel.EndpointNotFoundException ex)
            {
                MessageBox.Show("The Logging Service could not be connected to, exit this program, start the Logging Service and then start this program again!");
                Application.Exit();
            }
        }                
        
        private void uxRetrieveLogs_Click(object sender, EventArgs e)
        {            
            Generate();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void uxBuildFilter_Click(object sender, EventArgs e)
        {
            FilterBuilder builder = new FilterBuilder();
            builder.filterItems = _filter;
            if (builder.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _filter = builder.filterItems;

                Generate();
                
            }
        }        

        private void uxSearch_Click(object sender, EventArgs e)
        {
            uxLogEntryReport.Visible = false;
            uxLogEntryReport.BeginUpdate();
            foreach (TreeNode parentNode in uxLogEntryReport.Nodes)
            {
                bool matched = parentNode.Text.ToLower().Contains(uxSearchText.Text.ToLower()) && uxSearchText.Text.Length > 0;
                if (matched)
                {
                    SetNodeToSelectedFlipFlop(parentNode, matched);
                    uxLogEntryReport.SelectedNode = parentNode;
                    parentNode.EnsureVisible();
                }
                foreach (TreeNode childNode in parentNode.Nodes)
                {
                    matched = childNode.Text.ToLower().Contains(uxSearchText.Text) && uxSearchText.Text.Length > 0;
                    if (matched)
                    {
                        SetNodeToSelectedFlipFlop(childNode, matched);
                        uxLogEntryReport.SelectedNode = childNode;
                        childNode.EnsureVisible();
                    }
                }
            }
            uxLogEntryReport.Visible = true;
            uxLogEntryReport.EndUpdate();
        }        

        private void uxSelectLogFile_Click(object sender, EventArgs e)
        {
            if (uiOpenLogFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                uxPathToLogFile.Text = uiOpenLogFile.FileName;
            }
        }

        private void uiCollapseExpandeAll_Click(object sender, EventArgs e)
        {
            _presenter.CollapseNodes = !_presenter.CollapseNodes;
        }

        private void uiSendTestMessage_Click(object sender, EventArgs e)
        {            
            Dimension15.Services.Logging.WcfService.ILoggingService service = Dimension15.Services.Logging.WcfService.ServiceClient.GetServiceClient(new System.ServiceModel.EndpointAddress(new Uri(ServerUrl)), true);
            service.Add(Dimension15.Services.Logging.LogEntry.CreateLogEntry(Services.Logging.Enumerations.LogSeverity.Information, "!!!!!!!!!!!TEST MESSAGE FROM LOG VIEWER!!!!!!!!!!!", "", (int)Dimension15.Services.Logging.Enumerations.SystemID.Dimension15Logging));
        }

        private void uxLiveView_Click(object sender, EventArgs e)
        {
            if (_liveWindow == null)
            {
                Uri u = new Uri(ServerUrl);
                string subscriberUrl = ServerUrl.Replace("http://", "net.tcp://").Replace(":" + u.Port, ":" + (u.Port + 2)).Replace(u.LocalPath, "/LoggingServiceSubscriber");
                _liveWindow = new LiveLogView(subscriberUrl);
            }
            _liveWindow.Visible = true;
            _liveWindow.Show();
        }

        private void uiIgnoredSystemIds_DoubleClick(object sender, EventArgs e)
        {            
            uiIgnoredSystemIds.Items.Remove(uiIgnoredSystemIds.SelectedItem);
        }

        private void uiExportLog_Click(object sender, EventArgs e)
        {
            _presenter.ExportLogToFile();
        }
        #endregion

        #region Private Functions
        private List<Services.Logging.Interfaces.ILogEntry> RetrieveLogEntries(RetrievalFilter filter, string pathToLogFile)
        {
            List<Services.Logging.Interfaces.ILogEntry> retrievedLogEntries = (new Dimension15.Services.Logging.LogPersistance.LogPersistorFlatFile(pathToLogFile)).RetreiveAllByDateRange(filter.StartDate, filter.EndDate);
            retrievedLogEntries = filter.ApplyFilter(retrievedLogEntries);
            return retrievedLogEntries;
        }

        private void SetNodeToSelectedFlipFlop(TreeNode nodeToSelect, bool select)
        {
            if (select)
            {

                nodeToSelect.BackColor = System.Drawing.Color.Navy;
                nodeToSelect.ForeColor = System.Drawing.Color.Gold;
            }
            else
            {
                if (nodeToSelect.Tag != null)
                {
                    nodeToSelect.ForeColor = (System.Drawing.Color)nodeToSelect.Tag;
                }
                nodeToSelect.BackColor = uxLogEntryReport.BackColor;
            }
        }

        private void Generate()
        {
            _filter.StartDate = uxDateStart.Value.Date.Add(uxTimeStart.Value.TimeOfDay);
            _filter.EndDate = uxDateEnd.Value.Date.Add(uxTimeEnd.Value.TimeOfDay);

            Dimension15.Services.Logging.WcfService.ILoggingService service = Dimension15.Services.Logging.WcfService.ServiceClient.GetServiceClient(new System.ServiceModel.EndpointAddress(new Uri(ServerUrl)), true);

            try
            {
                List<Services.Logging.LogEntry> retrievedLogEntriesToShow = new List<Services.Logging.LogEntry>();
                if (uiUselocalMemoryCache.Checked)
                {
                    List<Dimension15.Services.Logging.Interfaces.ILogEntry> inter = new List<Services.Logging.Interfaces.ILogEntry>(_retrievedLogEntries);
                    retrievedLogEntriesToShow = _filter.ApplyFilter(inter).ConvertAll(t => (Dimension15.Services.Logging.LogEntry)t);
                }
                else
                {
                    if (uiUseLogFile.Checked)
                    {
                        if (System.IO.File.Exists(uxPathToLogFile.Text))
                        {
                            _retrievedLogEntries = (RetrieveLogEntries(_filter, uxPathToLogFile.Text)).ConvertAll(t => (Dimension15.Services.Logging.LogEntry)t);
                            _retrievedLogEntries.Reverse();
                        }
                        else
                        {
                            MessageBox.Show("The specified log file doesn't exist", "File not found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                    }
                    else
                    {
                        if (uiUseCachedData.Checked)
                        {
                            _retrievedLogEntries = service.RetrieveLogEntries(_filter, Dimension15.Services.Logging.Enumerations.PersistanceDataStore.LocalCache);
                        }
                        else
                        {
                            _retrievedLogEntries = service.RetrieveLogEntries(_filter, Dimension15.Services.Logging.Enumerations.PersistanceDataStore.ExternalDataStore);
                        }
                    }
                    retrievedLogEntriesToShow = _retrievedLogEntries;
                    retrievedLogEntriesToShow.Reverse();
                }

                if (_retrievedLogEntries.Count > 0)
                {
                    TimeSpan span = _retrievedLogEntries[0].DateStamp.Value - _retrievedLogEntries[_retrievedLogEntries.Count - 1].DateStamp.Value;
                    if (span.Minutes > 0)
                    {
                        uiStatsLabel.Text = (_retrievedLogEntries.Count).ToString() + " entries returned which span " + span.Minutes.ToString() + " minutes";
                    }
                    else if (span.Seconds > 15)
                    {
                        uiStatsLabel.Text = (_retrievedLogEntries.Count).ToString() + " entries returned which span " + span.Seconds.ToString() + " seconds";
                    }
                    else
                    {
                        uiStatsLabel.Text = (_retrievedLogEntries.Count).ToString() + " entries returned which span " + span.Milliseconds.ToString() + " milliseconds";
                    }
                }
                else
                {
                    uiStatsLabel.Text = "No entries found";
                }
                ShowItems(uxLogEntryReport, retrievedLogEntriesToShow);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                //                service.Close();
            }
        }

        private void AddNewLog(Dimension15.Services.Logging.LogEntry newLog)
        {
            if (!uiIgnoredSystemIds.Items.Contains(newLog.SystemID))
            {
                int lineCounter = uxLogEntryReport.Nodes.Count + 1;
                uxLogEntryReport.Invoke((MethodInvoker)(() =>
                {
                    uxLogEntryReport.Nodes.Add(TreeNodeBuilder.CreateNode(lineCounter, newLog));
                    uxLogEntryReport.Nodes[lineCounter - 1].Tag = newLog;
                    uxLogEntryReport.Nodes[lineCounter - 1].EnsureVisible();
                    //#region Dont allow more than 1000 items in list
                    //if (uxLogEntryReport.Nodes.Count > 5000)
                    //{
                    //    uxLogEntryReport.Nodes.RemoveAt(0);
                    //}
                    //#endregion
                }));
            }
        }

        private void ShowItems(TreeView treeView, List<Dimension15.Services.Logging.LogEntry> items)
        {

            List<Dimension15.Services.Logging.LogEntry> logsToShow = new List<Services.Logging.LogEntry>();
            #region Add all logs and check if we should remove Watchdog entries
            foreach (Dimension15.Services.Logging.LogEntry entry in items)
            {
                string isRunningMessage = Dimension15.Services.Logging.Events.Handlers.WatchDogMessages.GetIsRunningMessageForSystem(entry.SystemID.Value);
                string notRunningMessage = Dimension15.Services.Logging.Events.Handlers.WatchDogMessages.GetIsNotRunningMessageForSystem(entry.SystemID.Value);
                if (isRunningMessage.Trim() != string.Empty)
                {
                    if (entry.Message == notRunningMessage || entry.Message == isRunningMessage) //DEANK: I am not doing a case conversion... I know
                    {
                        if (uxShowOnlyWatchDogLogs.Checked)
                        {
                            logsToShow.Add(entry);
                        }
                    }                    
                }
                else
                {
                    logsToShow.Add(entry);
                }
            }
            #endregion

            #region Show the logs
            if (uxCreateNewGroupingWindow.Checked)
            {
                GroupingTree groupView = new GroupingTree();
                groupView.LogEntries = logsToShow;
                groupView.Show();
            }
            else
            {
                treeView.Visible = false;
                treeView.BeginUpdate();

                treeView.BackColor = System.Drawing.Color.FromArgb(248, 240, 214);// System.Drawing.Color.FromArgb(20, 20, 10);
                treeView.ForeColor = System.Drawing.Color.Green;// System.Drawing.Color.Green;
                treeView.Font = new System.Drawing.Font("Courier New", 8, FontStyle.Regular);
                if (!uxCreateNewGroupingWindow.Checked) uxLogEntryReport.Nodes.Clear();

                int lineCounter = 0;
                bool showEntry = true;

                foreach (Dimension15.Services.Logging.LogEntry entry in logsToShow)
                {
                    showEntry = true;
                    lineCounter++;
                    if (showEntry)
                    {
                        if (!uxCreateNewGroupingWindow.Checked)
                        {
                            _presenter.AddNewLog(entry, AddNewLog);
                        }
                    }
                }
                _presenter.CollapseNodes = _presenter.CollapseNodes;
                logTimeLine1.LogEntries = items;
            }
            #endregion

            treeView.EndUpdate();
            treeView.Visible = true;                     
        }

        #endregion                       

        private void uxLogEntryReport_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (e.Shift == true && uxLogEntryReport.SelectedNode.Parent == null)
                {
                    uxLogEntryReport.SelectedNode.Remove();
                }
            }
        }

        private void uxLogEntryReport_DoubleClick(object sender, EventArgs e)
        {
            if (uxLogEntryReport.SelectedNode.Parent == null && !uiIgnoredSystemIds.Items.Contains(uxLogEntryReport.SelectedNode.Tag.ToString())) ;
            {
                uiIgnoredSystemIds.Items.Add(((Dimension15.Services.Logging.LogEntry)uxLogEntryReport.SelectedNode.Tag).SystemID);
            }
        }

        private void uxLogEntryReport_AfterSelect(object sender, TreeViewEventArgs e)
        {
            uxSelectedNodeText.Text = uxLogEntryReport.SelectedNode.Text;
        }

        private void toggleLogSelectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int fontSize = 12;
            FontStyle boldness = FontStyle.Bold;

            if (uxLogEntryReport.SelectedNode.NodeFont != null && uxLogEntryReport.SelectedNode.NodeFont.Size == 12)
            {
                fontSize = 8;
                boldness = FontStyle.Regular;
            }

            if (uxLogEntryReport.SelectedNode.Parent == null)
            {
                Font f = new Font("Courier New", fontSize, boldness);
                uxLogEntryReport.SelectedNode.NodeFont = f;
                uxLogEntryReport.SelectedNode.FirstNode.NodeFont = uxLogEntryReport.SelectedNode.NodeFont;

                uxLogEntryReport.SelectedNode.FirstNode.Text = uxLogEntryReport.SelectedNode.FirstNode.Text;
                uxLogEntryReport.SelectedNode.Text = uxLogEntryReport.SelectedNode.Text;
            }
            else
            {
                Font f = new Font("Courier New", fontSize, boldness);
                uxLogEntryReport.SelectedNode.NodeFont = f;
                uxLogEntryReport.SelectedNode.Parent.NodeFont = uxLogEntryReport.SelectedNode.Parent.NodeFont;

                uxLogEntryReport.SelectedNode.Parent.Text = uxLogEntryReport.SelectedNode.Parent.Text;
                uxLogEntryReport.SelectedNode.Text = uxLogEntryReport.SelectedNode.Text;
            }
        }

        private void deleteLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (uxLogEntryReport.SelectedNode.Parent == null)
            {
                uxLogEntryReport.SelectedNode.Remove();
            }
            else
            {
                uxLogEntryReport.SelectedNode.Parent.Remove();
            }
        }

        private void filterOutLogOnNextRetrieveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (uxLogEntryReport.SelectedNode.Parent == null)
            {
                uiIgnoredSystemIds.Items.Add(((Dimension15.Services.Logging.LogEntry)uxLogEntryReport.SelectedNode.Tag).SystemID);
            }
            else
            {
                uiIgnoredSystemIds.Items.Add(((Dimension15.Services.Logging.LogEntry)uxLogEntryReport.SelectedNode.Parent.Tag).SystemID);
            }
        }
    }
}
