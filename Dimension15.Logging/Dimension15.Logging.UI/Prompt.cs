﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Logging.UI
{
    public static class Prompt
    {
        public static string ShowDialog(string text, string caption, string defaultText)
        {//8992
            System.Windows.Forms.Form prompt = new System.Windows.Forms.Form();
            prompt.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            prompt.Width = 500;
            prompt.Height = 150;
            prompt.Text = caption;
            System.Windows.Forms.Label textLabel = new System.Windows.Forms.Label() { Left = 50, Top = 20, Text = text, Width = 380 };
            System.Windows.Forms.TextBox textBox = new System.Windows.Forms.TextBox() { Left = 50, Top = 50, Width = 400 };
            textBox.Text = defaultText;
            System.Windows.Forms.Button confirmation = new System.Windows.Forms.Button() { Text = "Ok", Left = 350, Width = 100, Top = 70 };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.Controls.Add(textBox);
            prompt.ShowDialog();
            return textBox.Text;
        }
    }
}
