﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Dimension15.Logging.UI
{
    public partial class LogTimeLine : UserControl
    {
        #region Fields
        private List<Dimension15.Services.Logging.LogEntry> _logEntries;
        DateTime _endDateStamp;
        private TimeSpan _startTimeSpan;
        private TimeSpan _logTimespan;
        private double _logTimeSpanMappedToCanvasPixels = 0;
        private Dictionary<int, long> _logEntriesOnCanvas;
        private Graphics _graphicsCanvas;
        #endregion

        #region Properties
        public List<Dimension15.Services.Logging.LogEntry> LogEntries
        {
            get
            {
                return _logEntries;
            }
            set
            {
                _logEntries = value;
                RenderLogEntries(uiCanvas, _logEntries);
            }
        }
        #endregion

        public LogTimeLine()
        {
            InitializeComponent();
        }

        private void LogTimeLine_Load(object sender, EventArgs e)
        {
           
        }

        #region Private Functions
        private void RenderLogEntries(PictureBox canvas, List<Dimension15.Services.Logging.LogEntry> logEntries)
        {
            
            _logEntriesOnCanvas = new Dictionary<int, long>();
            if (logEntries!=null && logEntries.Count >= 2)
            {
                Bitmap bmp = new Bitmap(uiCanvas.Width, uiCanvas.Height);
                logEntries.Reverse();
                int topY = 4;
                TimeSpan startTimeSpan = logEntries[0].DateStamp.Value.TimeOfDay;
                _endDateStamp = logEntries[0].DateStamp.Value;
                _logTimespan = startTimeSpan.Subtract(logEntries[logEntries.Count - 1].DateStamp.Value.TimeOfDay);
                _logTimeSpanMappedToCanvasPixels = (canvas.Width-2) / _logTimespan.TotalMilliseconds; //Millisecond to pixel

                _graphicsCanvas = Graphics.FromImage(bmp);
                _graphicsCanvas.Clear(canvas.BackColor);
                Pen pen = new Pen(new SolidBrush(Color.FromArgb(130, 220, 130)),3);
                Pen markerPen = new Pen(new SolidBrush(Color.FromArgb(230,215,200)), 1);
                _graphicsCanvas.DrawLine(pen, new Point(0, topY), new Point(canvas.Width, topY));

                foreach (Dimension15.Services.Logging.LogEntry logEntry in logEntries)
                {
                    double mappedPixelPos = startTimeSpan.Subtract(logEntry.DateStamp.Value.TimeOfDay).TotalMilliseconds*_logTimeSpanMappedToCanvasPixels;
                    if (double.IsNaN(mappedPixelPos))
                    {
                        mappedPixelPos = 1;
                    }
                    if (!_logEntriesOnCanvas.Keys.Contains((int)mappedPixelPos))
                    {
                        _logEntriesOnCanvas.Add((int)mappedPixelPos, logEntry.Id);
                    }
                    _graphicsCanvas.DrawLine(markerPen, new Point((int)mappedPixelPos, topY+2), new Point((int)mappedPixelPos, topY+ 12));
                }
                uiCanvas.Image = bmp;
            } 
        }
        #endregion

        private void uiCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (_logEntries != null && (_logEntries.Count>=2))
            {
                long approximateLogId = GetClosestLogEntryId(e.X, 0,_logEntriesOnCanvas);
                KeyValuePair<int, Services.Logging.LogEntry> logEntryPosition = GetLogEntryIndexOf(approximateLogId,_logEntries);
                if (logEntryPosition.Key != -1)
                {
                    uiCurrentTimeMarker.Text = _endDateStamp.AddMilliseconds(-1 * (e.X / _logTimeSpanMappedToCanvasPixels)).ToString("dd MMM yyyy HH:mm:ss") + ", Id:" + (approximateLogId).ToString() + ", log entry: " + logEntryPosition.Key + ", description: " + logEntryPosition.Value.Message;
                }
                else
                {
                    uiCurrentTimeMarker.Text = _endDateStamp.AddMilliseconds(e.X / _logTimeSpanMappedToCanvasPixels).ToString("dd MMM yyyy HH:mm:ss");
                }
            }
        }

        private KeyValuePair<int, Services.Logging.LogEntry> GetLogEntryIndexOf(long logId, List<Dimension15.Services.Logging.LogEntry> logEntries)
        {
            for (int logEntryCounter = 0; logEntryCounter < LogEntries.Count;logEntryCounter++ )
            {
                if (logEntries[logEntryCounter].Id == logId)
                {
                    return new KeyValuePair<int, Services.Logging.LogEntry>(logEntries.Count()-(logEntryCounter), logEntries[logEntryCounter]);
                }
            }
            return new KeyValuePair<int,Services.Logging.LogEntry>(-1,null);
        }

        private long GetClosestLogEntryId(int entryPosition, int searchOffset, Dictionary<int, long> logEntrysOnCanvas)
        {
            int maximimOffsetScanAllowed = 10;
            foreach (KeyValuePair<int, long> item in logEntrysOnCanvas)
            {
                if ((item.Key == entryPosition + searchOffset) || (item.Key == entryPosition - searchOffset))
                {
                    return item.Value;
                }
            }
            if (maximimOffsetScanAllowed >= searchOffset)
            {
                return GetClosestLogEntryId(entryPosition, searchOffset + 1, logEntrysOnCanvas);
            }
            else
            {
                return 0;
            }
        }

        private void uiCanvas_Paint(object sender, PaintEventArgs e)
        {
            
        }

    }
}
