﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Dimension15.Logging.UI
{
    /// <summary>
    /// Given a LogEntry create a treeNode
    /// </summary>
    public static class TreeNodeBuilder
    {
        public static TreeNode CreateNode(int nodeId, Dimension15.Services.Logging.LogEntry logEntry)
        {
            string parentText = "(threadId:" + logEntry.ThreadID.ToString() + ") " + 
                nodeId.ToString().PadRight(6, ' ') + "-" + 
                ((logEntry.DateStamp.Value.ToString("yyyy/MM/dd HH:mm:ss") + 
                ": <" + logEntry.FunctionName + "> ").PadRight(10, ' ') + 
                logEntry.Message);
            string childText = logEntry.LogSeverity.ToString() + 
                " <" + logEntry.SystemID + "> <" + 
                logEntry.ModuleName + ":" + logEntry.FunctionName + 
                "> traceId:" + logEntry.TraceID + 
                " <" + logEntry.Id.ToString() + ">";

            TreeNode parentNode = new TreeNode(parentText);
            parentNode.ExpandAll();

            TreeNode messageNode = new TreeNode(childText);
            messageNode.NodeFont = new System.Drawing.Font("Courier new", 8, FontStyle.Bold);
            if (logEntry.Message.StartsWith(Dimension15.Services.Logging.Events.Handlers.WatchDogMessages.PrefixIsRunning))
            {
                parentNode.ForeColor = System.Drawing.Color.FromArgb(138, 138, 138);
                messageNode.ForeColor = System.Drawing.Color.FromArgb(138, 138, 138);
            }
            else if (logEntry.Message.StartsWith(Dimension15.Services.Logging.Events.Handlers.WatchDogMessages.PrefixNotRunning))
            {
                parentNode.ForeColor = System.Drawing.Color.FromArgb(195, 10, 110);
                messageNode.ForeColor = System.Drawing.Color.FromArgb(195, 10, 110);
            }
            else if (logEntry.LogSeverity == Services.Logging.Enumerations.LogSeverity.Critical)
            {
                parentNode.ForeColor = System.Drawing.Color.FromArgb(254, 75, 75);
                messageNode.ForeColor = System.Drawing.Color.FromArgb(254, 75, 75);
            }
            else if (logEntry.LogSeverity == Services.Logging.Enumerations.LogSeverity.Error)
            {
                parentNode.ForeColor = System.Drawing.Color.FromArgb(240, 30, 30);
                messageNode.ForeColor = System.Drawing.Color.FromArgb(240, 30, 30);
            }
            else if (logEntry.LogSeverity == Services.Logging.Enumerations.LogSeverity.Warning)
            {
                parentNode.ForeColor = System.Drawing.Color.FromArgb(215, 30, 205);
                messageNode.ForeColor = System.Drawing.Color.FromArgb(215, 30, 205);
            }            
            else
            {
                parentNode.ForeColor = System.Drawing.Color.FromArgb(55, 0, 0);//(150,150,250);
            }
            parentNode.Nodes.Add(messageNode);
            

            parentNode.Tag = parentNode.ForeColor;
            messageNode.Tag = messageNode.ForeColor;

            return parentNode;
        }
    }
}
