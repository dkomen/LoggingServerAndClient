﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Logging.UI
{
    public class FilterBuilderFieldItem
    {
        #region Properties
        public string Name { get; set;}
        public Type ValueType { get; set; }
        public object Value { get; set; }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return Value.ToString();
        }
        #endregion
    }
}
