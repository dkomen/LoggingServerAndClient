﻿namespace Dimension15.Logging.UI
{
    partial class LogView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogView));
            this.uxRetrieveLogs = new System.Windows.Forms.Button();
            this.uxDateStart = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uxDateEnd = new System.Windows.Forms.DateTimePicker();
            this.uxTimeStart = new System.Windows.Forms.DateTimePicker();
            this.uxTimeEnd = new System.Windows.Forms.DateTimePicker();
            this.uxBuildFilter = new System.Windows.Forms.Button();
            this.uiStatsLabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.uxSearchText = new System.Windows.Forms.TextBox();
            this.uxSearch = new System.Windows.Forms.Button();
            this.uiOpenLogFile = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.uiExportLog = new System.Windows.Forms.Button();
            this.uxLiveView = new System.Windows.Forms.Button();
            this.uiSendTestMessage = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.uiServerUrl = new System.Windows.Forms.TextBox();
            this.uiUselocalMemoryCache = new System.Windows.Forms.RadioButton();
            this.uxCreateNewGroupingWindow = new System.Windows.Forms.CheckBox();
            this.uxSelectLogFile = new System.Windows.Forms.Button();
            this.uxPathToLogFile = new System.Windows.Forms.TextBox();
            this.uiUseLogFile = new System.Windows.Forms.RadioButton();
            this.uiUseEntireLog = new System.Windows.Forms.RadioButton();
            this.uiUseCachedData = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.uxShowOnlyWatchDogLogs = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.uiIgnoredSystemIds = new System.Windows.Forms.ListBox();
            this.uxLogEntryReport = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toggleLogSelectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterOutLogOnNextRetrieveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListOfAllSystemIds = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.uiCollapseExpandeAll = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.uxSplitContainerLogView = new System.Windows.Forms.SplitContainer();
            this.uxSelectedNodeText = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.logTimeLine1 = new Dimension15.Logging.UI.LogTimeLine();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uxSplitContainerLogView)).BeginInit();
            this.uxSplitContainerLogView.Panel1.SuspendLayout();
            this.uxSplitContainerLogView.Panel2.SuspendLayout();
            this.uxSplitContainerLogView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // uxRetrieveLogs
            // 
            this.uxRetrieveLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxRetrieveLogs.BackColor = System.Drawing.Color.LightSalmon;
            this.uxRetrieveLogs.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxRetrieveLogs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxRetrieveLogs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.uxRetrieveLogs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxRetrieveLogs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxRetrieveLogs.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.uxRetrieveLogs.Location = new System.Drawing.Point(7, 85);
            this.uxRetrieveLogs.Name = "uxRetrieveLogs";
            this.uxRetrieveLogs.Size = new System.Drawing.Size(75, 30);
            this.uxRetrieveLogs.TabIndex = 1;
            this.uxRetrieveLogs.Text = "Retrieve";
            this.toolTip1.SetToolTip(this.uxRetrieveLogs, "Retreive logs using current filter criteria");
            this.uxRetrieveLogs.UseVisualStyleBackColor = false;
            this.uxRetrieveLogs.Click += new System.EventHandler(this.uxRetrieveLogs_Click);
            // 
            // uxDateStart
            // 
            this.uxDateStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxDateStart.Location = new System.Drawing.Point(113, 44);
            this.uxDateStart.Name = "uxDateStart";
            this.uxDateStart.Size = new System.Drawing.Size(200, 20);
            this.uxDateStart.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "To";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // uxDateEnd
            // 
            this.uxDateEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxDateEnd.Location = new System.Drawing.Point(113, 67);
            this.uxDateEnd.Name = "uxDateEnd";
            this.uxDateEnd.Size = new System.Drawing.Size(200, 20);
            this.uxDateEnd.TabIndex = 4;
            // 
            // uxTimeStart
            // 
            this.uxTimeStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxTimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.uxTimeStart.Location = new System.Drawing.Point(319, 44);
            this.uxTimeStart.Name = "uxTimeStart";
            this.uxTimeStart.ShowUpDown = true;
            this.uxTimeStart.Size = new System.Drawing.Size(97, 20);
            this.uxTimeStart.TabIndex = 6;
            // 
            // uxTimeEnd
            // 
            this.uxTimeEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxTimeEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.uxTimeEnd.Location = new System.Drawing.Point(319, 67);
            this.uxTimeEnd.Name = "uxTimeEnd";
            this.uxTimeEnd.ShowUpDown = true;
            this.uxTimeEnd.Size = new System.Drawing.Size(97, 20);
            this.uxTimeEnd.TabIndex = 7;
            // 
            // uxBuildFilter
            // 
            this.uxBuildFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxBuildFilter.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxBuildFilter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxBuildFilter.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uxBuildFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxBuildFilter.ForeColor = System.Drawing.Color.Navy;
            this.uxBuildFilter.Location = new System.Drawing.Point(14, 13);
            this.uxBuildFilter.Name = "uxBuildFilter";
            this.uxBuildFilter.Size = new System.Drawing.Size(240, 23);
            this.uxBuildFilter.TabIndex = 8;
            this.uxBuildFilter.Text = "Build filter";
            this.toolTip1.SetToolTip(this.uxBuildFilter, "Advanced filter options");
            this.uxBuildFilter.UseVisualStyleBackColor = true;
            this.uxBuildFilter.Click += new System.EventHandler(this.uxBuildFilter_Click);
            // 
            // uiStatsLabel
            // 
            this.uiStatsLabel.AutoSize = true;
            this.uiStatsLabel.Location = new System.Drawing.Point(8, 1);
            this.uiStatsLabel.Name = "uiStatsLabel";
            this.uiStatsLabel.Size = new System.Drawing.Size(105, 13);
            this.uiStatsLabel.TabIndex = 10;
            this.uiStatsLabel.Text = "No entries requested";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Search";
            // 
            // uxSearchText
            // 
            this.uxSearchText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxSearchText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(240)))));
            this.uxSearchText.Location = new System.Drawing.Point(54, 94);
            this.uxSearchText.Name = "uxSearchText";
            this.uxSearchText.Size = new System.Drawing.Size(277, 20);
            this.uxSearchText.TabIndex = 12;
            // 
            // uxSearch
            // 
            this.uxSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uxSearch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxSearch.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxSearch.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uxSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxSearch.ForeColor = System.Drawing.Color.Navy;
            this.uxSearch.Location = new System.Drawing.Point(337, 93);
            this.uxSearch.Name = "uxSearch";
            this.uxSearch.Size = new System.Drawing.Size(75, 22);
            this.uxSearch.TabIndex = 13;
            this.uxSearch.Text = "Search";
            this.uxSearch.UseVisualStyleBackColor = true;
            this.uxSearch.Click += new System.EventHandler(this.uxSearch_Click);
            // 
            // uiOpenLogFile
            // 
            this.uiOpenLogFile.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.uiExportLog);
            this.groupBox1.Controls.Add(this.uxLiveView);
            this.groupBox1.Controls.Add(this.uiSendTestMessage);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.uiServerUrl);
            this.groupBox1.Controls.Add(this.uiUselocalMemoryCache);
            this.groupBox1.Controls.Add(this.uxCreateNewGroupingWindow);
            this.groupBox1.Controls.Add(this.uxSelectLogFile);
            this.groupBox1.Controls.Add(this.uxPathToLogFile);
            this.groupBox1.Controls.Add(this.uiUseLogFile);
            this.groupBox1.Controls.Add(this.uiUseEntireLog);
            this.groupBox1.Controls.Add(this.uiUseCachedData);
            this.groupBox1.Controls.Add(this.uxRetrieveLogs);
            this.groupBox1.Location = new System.Drawing.Point(7, 481);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(595, 120);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data source";
            // 
            // uiExportLog
            // 
            this.uiExportLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiExportLog.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uiExportLog.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uiExportLog.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uiExportLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uiExportLog.Location = new System.Drawing.Point(335, 87);
            this.uiExportLog.Name = "uiExportLog";
            this.uiExportLog.Size = new System.Drawing.Size(83, 28);
            this.uiExportLog.TabIndex = 23;
            this.uiExportLog.Text = "Export log";
            this.toolTip1.SetToolTip(this.uiExportLog, "Export the current log view to a file on disk");
            this.uiExportLog.UseVisualStyleBackColor = true;
            this.uiExportLog.Click += new System.EventHandler(this.uiExportLog_Click);
            // 
            // uxLiveView
            // 
            this.uxLiveView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uxLiveView.BackColor = System.Drawing.Color.LightSalmon;
            this.uxLiveView.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxLiveView.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxLiveView.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed;
            this.uxLiveView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxLiveView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxLiveView.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.uxLiveView.Location = new System.Drawing.Point(513, 88);
            this.uxLiveView.Name = "uxLiveView";
            this.uxLiveView.Size = new System.Drawing.Size(75, 27);
            this.uxLiveView.TabIndex = 22;
            this.uxLiveView.Text = "Live view";
            this.toolTip1.SetToolTip(this.uxLiveView, "Open a new form that will show a live log feed");
            this.uxLiveView.UseVisualStyleBackColor = false;
            this.uxLiveView.Click += new System.EventHandler(this.uxLiveView_Click);
            // 
            // uiSendTestMessage
            // 
            this.uiSendTestMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiSendTestMessage.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uiSendTestMessage.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uiSendTestMessage.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uiSendTestMessage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uiSendTestMessage.Location = new System.Drawing.Point(424, 87);
            this.uiSendTestMessage.Name = "uiSendTestMessage";
            this.uiSendTestMessage.Size = new System.Drawing.Size(83, 28);
            this.uiSendTestMessage.TabIndex = 21;
            this.uiSendTestMessage.Text = "Test message";
            this.toolTip1.SetToolTip(this.uiSendTestMessage, "Send a test message to the logger service");
            this.uiSendTestMessage.UseVisualStyleBackColor = true;
            this.uiSendTestMessage.Click += new System.EventHandler(this.uiSendTestMessage_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(328, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Server Url";
            // 
            // uiServerUrl
            // 
            this.uiServerUrl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiServerUrl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(240)))));
            this.uiServerUrl.Enabled = false;
            this.uiServerUrl.Location = new System.Drawing.Point(388, 14);
            this.uiServerUrl.Name = "uiServerUrl";
            this.uiServerUrl.Size = new System.Drawing.Size(200, 20);
            this.uiServerUrl.TabIndex = 18;
            this.toolTip1.SetToolTip(this.uiServerUrl, "The url to the logging server to connect to");
            // 
            // uiUselocalMemoryCache
            // 
            this.uiUselocalMemoryCache.AutoSize = true;
            this.uiUselocalMemoryCache.Location = new System.Drawing.Point(144, 16);
            this.uiUselocalMemoryCache.Name = "uiUselocalMemoryCache";
            this.uiUselocalMemoryCache.Size = new System.Drawing.Size(146, 17);
            this.uiUselocalMemoryCache.TabIndex = 17;
            this.uiUselocalMemoryCache.Text = "Re-use last retrieved data";
            this.uiUselocalMemoryCache.UseVisualStyleBackColor = true;
            // 
            // uxCreateNewGroupingWindow
            // 
            this.uxCreateNewGroupingWindow.AutoSize = true;
            this.uxCreateNewGroupingWindow.Location = new System.Drawing.Point(89, 100);
            this.uxCreateNewGroupingWindow.Name = "uxCreateNewGroupingWindow";
            this.uxCreateNewGroupingWindow.Size = new System.Drawing.Size(175, 17);
            this.uxCreateNewGroupingWindow.TabIndex = 16;
            this.uxCreateNewGroupingWindow.Text = "Open in new grouping window?";
            this.toolTip1.SetToolTip(this.uxCreateNewGroupingWindow, "In grouping windows you can write custom C# queries to group logs with");
            this.uxCreateNewGroupingWindow.UseVisualStyleBackColor = true;
            // 
            // uxSelectLogFile
            // 
            this.uxSelectLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uxSelectLogFile.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uxSelectLogFile.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uxSelectLogFile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uxSelectLogFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uxSelectLogFile.ForeColor = System.Drawing.Color.Navy;
            this.uxSelectLogFile.Location = new System.Drawing.Point(554, 61);
            this.uxSelectLogFile.Name = "uxSelectLogFile";
            this.uxSelectLogFile.Size = new System.Drawing.Size(34, 21);
            this.uxSelectLogFile.TabIndex = 15;
            this.uxSelectLogFile.Text = "...";
            this.uxSelectLogFile.UseVisualStyleBackColor = true;
            this.uxSelectLogFile.Click += new System.EventHandler(this.uxSelectLogFile_Click);
            // 
            // uxPathToLogFile
            // 
            this.uxPathToLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uxPathToLogFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(255)))), ((int)(((byte)(240)))));
            this.uxPathToLogFile.Location = new System.Drawing.Point(155, 61);
            this.uxPathToLogFile.Name = "uxPathToLogFile";
            this.uxPathToLogFile.Size = new System.Drawing.Size(397, 20);
            this.uxPathToLogFile.TabIndex = 14;
            // 
            // uiUseLogFile
            // 
            this.uiUseLogFile.AutoSize = true;
            this.uiUseLogFile.Location = new System.Drawing.Point(7, 62);
            this.uiUseLogFile.Name = "uiUseLogFile";
            this.uiUseLogFile.Size = new System.Drawing.Size(111, 17);
            this.uiUseLogFile.TabIndex = 4;
            this.uiUseLogFile.Text = "Query local log file";
            this.uiUseLogFile.UseVisualStyleBackColor = true;
            // 
            // uiUseEntireLog
            // 
            this.uiUseEntireLog.AutoSize = true;
            this.uiUseEntireLog.Location = new System.Drawing.Point(7, 39);
            this.uiUseEntireLog.Name = "uiUseEntireLog";
            this.uiUseEntireLog.Size = new System.Drawing.Size(99, 17);
            this.uiUseEntireLog.TabIndex = 3;
            this.uiUseEntireLog.Text = "Query entire log";
            this.uiUseEntireLog.UseVisualStyleBackColor = true;
            // 
            // uiUseCachedData
            // 
            this.uiUseCachedData.AutoSize = true;
            this.uiUseCachedData.Checked = true;
            this.uiUseCachedData.Location = new System.Drawing.Point(7, 16);
            this.uiUseCachedData.Name = "uiUseCachedData";
            this.uiUseCachedData.Size = new System.Drawing.Size(131, 17);
            this.uiUseCachedData.TabIndex = 2;
            this.uiUseCachedData.TabStop = true;
            this.uiUseCachedData.Text = "Only query cached log";
            this.uiUseCachedData.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.uxShowOnlyWatchDogLogs);
            this.groupBox2.Controls.Add(this.uxSearch);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.uxSearchText);
            this.groupBox2.Controls.Add(this.uxTimeStart);
            this.groupBox2.Controls.Add(this.uxDateStart);
            this.groupBox2.Controls.Add(this.uxTimeEnd);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.uxDateEnd);
            this.groupBox2.Controls.Add(this.uxBuildFilter);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(608, 481);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(426, 120);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filter criteria";
            // 
            // uxShowOnlyWatchDogLogs
            // 
            this.uxShowOnlyWatchDogLogs.AutoSize = true;
            this.uxShowOnlyWatchDogLogs.Location = new System.Drawing.Point(268, 17);
            this.uxShowOnlyWatchDogLogs.Name = "uxShowOnlyWatchDogLogs";
            this.uxShowOnlyWatchDogLogs.Size = new System.Drawing.Size(152, 17);
            this.uxShowOnlyWatchDogLogs.TabIndex = 18;
            this.uxShowOnlyWatchDogLogs.Text = "Include the watchdog log?";
            this.toolTip1.SetToolTip(this.uxShowOnlyWatchDogLogs, "Return the watchdog logs as well");
            this.uxShowOnlyWatchDogLogs.UseVisualStyleBackColor = true;
            // 
            // uiIgnoredSystemIds
            // 
            this.uiIgnoredSystemIds.BackColor = System.Drawing.Color.Black;
            this.uiIgnoredSystemIds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiIgnoredSystemIds.ForeColor = System.Drawing.Color.Yellow;
            this.uiIgnoredSystemIds.FormattingEnabled = true;
            this.uiIgnoredSystemIds.IntegralHeight = false;
            this.uiIgnoredSystemIds.Location = new System.Drawing.Point(3, 16);
            this.uiIgnoredSystemIds.Name = "uiIgnoredSystemIds";
            this.uiIgnoredSystemIds.Size = new System.Drawing.Size(150, 336);
            this.uiIgnoredSystemIds.TabIndex = 28;
            this.toolTip1.SetToolTip(this.uiIgnoredSystemIds, "A quick \'client-side\' filter of SystemId\'s to ignore (Double-Click an item to rem" +
        "ove)");
            this.uiIgnoredSystemIds.DoubleClick += new System.EventHandler(this.uiIgnoredSystemIds_DoubleClick);
            // 
            // uxLogEntryReport
            // 
            this.uxLogEntryReport.BackColor = System.Drawing.Color.Black;
            this.uxLogEntryReport.ContextMenuStrip = this.contextMenuStrip1;
            this.uxLogEntryReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxLogEntryReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uxLogEntryReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.uxLogEntryReport.HideSelection = false;
            this.uxLogEntryReport.ItemHeight = 12;
            this.uxLogEntryReport.Location = new System.Drawing.Point(0, 0);
            this.uxLogEntryReport.Name = "uxLogEntryReport";
            this.uxLogEntryReport.Size = new System.Drawing.Size(778, 336);
            this.uxLogEntryReport.TabIndex = 10;
            this.toolTip1.SetToolTip(this.uxLogEntryReport, "List of logs (Double-Click an item to add SystemId to ignore filter, SHFT-DEL to " +
        "delete a node)");
            this.uxLogEntryReport.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.uxLogEntryReport_AfterSelect);
            this.uxLogEntryReport.DoubleClick += new System.EventHandler(this.uxLogEntryReport_DoubleClick);
            this.uxLogEntryReport.KeyUp += new System.Windows.Forms.KeyEventHandler(this.uxLogEntryReport_KeyUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toggleLogSelectionToolStripMenuItem,
            this.deleteLogToolStripMenuItem,
            this.filterOutLogOnNextRetrieveToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(226, 70);
            // 
            // toggleLogSelectionToolStripMenuItem
            // 
            this.toggleLogSelectionToolStripMenuItem.Name = "toggleLogSelectionToolStripMenuItem";
            this.toggleLogSelectionToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.toggleLogSelectionToolStripMenuItem.Text = "Toggle log selection";
            this.toggleLogSelectionToolStripMenuItem.Click += new System.EventHandler(this.toggleLogSelectionToolStripMenuItem_Click);
            // 
            // deleteLogToolStripMenuItem
            // 
            this.deleteLogToolStripMenuItem.Name = "deleteLogToolStripMenuItem";
            this.deleteLogToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.deleteLogToolStripMenuItem.Text = "Delete log";
            this.deleteLogToolStripMenuItem.Click += new System.EventHandler(this.deleteLogToolStripMenuItem_Click);
            // 
            // filterOutLogOnNextRetrieveToolStripMenuItem
            // 
            this.filterOutLogOnNextRetrieveToolStripMenuItem.Name = "filterOutLogOnNextRetrieveToolStripMenuItem";
            this.filterOutLogOnNextRetrieveToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.filterOutLogOnNextRetrieveToolStripMenuItem.Text = "Filter out log on next retrieve";
            this.filterOutLogOnNextRetrieveToolStripMenuItem.Click += new System.EventHandler(this.filterOutLogOnNextRetrieveToolStripMenuItem_Click);
            // 
            // ListOfAllSystemIds
            // 
            this.ListOfAllSystemIds.BackColor = System.Drawing.Color.Black;
            this.ListOfAllSystemIds.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListOfAllSystemIds.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListOfAllSystemIds.ForeColor = System.Drawing.Color.Yellow;
            this.ListOfAllSystemIds.FormattingEnabled = true;
            this.ListOfAllSystemIds.IntegralHeight = false;
            this.ListOfAllSystemIds.Location = new System.Drawing.Point(0, 0);
            this.ListOfAllSystemIds.Name = "ListOfAllSystemIds";
            this.ListOfAllSystemIds.Size = new System.Drawing.Size(158, 336);
            this.ListOfAllSystemIds.TabIndex = 29;
            this.toolTip1.SetToolTip(this.ListOfAllSystemIds, "A list of all SystemId\'s");
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.splitContainer2);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(946, 355);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "log view";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(3, 16);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.uiCollapseExpandeAll);
            this.splitContainer2.Panel1.Controls.Add(this.uxLogEntryReport);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.ListOfAllSystemIds);
            this.splitContainer2.Size = new System.Drawing.Size(940, 336);
            this.splitContainer2.SplitterDistance = 778;
            this.splitContainer2.TabIndex = 12;
            // 
            // uiCollapseExpandeAll
            // 
            this.uiCollapseExpandeAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiCollapseExpandeAll.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.uiCollapseExpandeAll.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.uiCollapseExpandeAll.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Silver;
            this.uiCollapseExpandeAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uiCollapseExpandeAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiCollapseExpandeAll.ForeColor = System.Drawing.Color.Navy;
            this.uiCollapseExpandeAll.Location = new System.Drawing.Point(699, 314);
            this.uiCollapseExpandeAll.Name = "uiCollapseExpandeAll";
            this.uiCollapseExpandeAll.Size = new System.Drawing.Size(79, 22);
            this.uiCollapseExpandeAll.TabIndex = 13;
            this.uiCollapseExpandeAll.Text = "Collapse nodes";
            this.uiCollapseExpandeAll.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.uiCollapseExpandeAll.UseVisualStyleBackColor = true;
            this.uiCollapseExpandeAll.Click += new System.EventHandler(this.uiCollapseExpandeAll_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.uiIgnoredSystemIds);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(156, 355);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ignored systems";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(6, 60);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.uxSplitContainerLogView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.uxSelectedNodeText);
            this.splitContainer1.Size = new System.Drawing.Size(1106, 415);
            this.splitContainer1.SplitterDistance = 355;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 21;
            // 
            // uxSplitContainerLogView
            // 
            this.uxSplitContainerLogView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSplitContainerLogView.Location = new System.Drawing.Point(0, 0);
            this.uxSplitContainerLogView.Name = "uxSplitContainerLogView";
            // 
            // uxSplitContainerLogView.Panel1
            // 
            this.uxSplitContainerLogView.Panel1.Controls.Add(this.groupBox3);
            // 
            // uxSplitContainerLogView.Panel2
            // 
            this.uxSplitContainerLogView.Panel2.Controls.Add(this.groupBox4);
            this.uxSplitContainerLogView.Size = new System.Drawing.Size(1106, 355);
            this.uxSplitContainerLogView.SplitterDistance = 156;
            this.uxSplitContainerLogView.TabIndex = 11;
            // 
            // uxSelectedNodeText
            // 
            this.uxSelectedNodeText.BackColor = System.Drawing.Color.Black;
            this.uxSelectedNodeText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uxSelectedNodeText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.uxSelectedNodeText.Location = new System.Drawing.Point(0, 0);
            this.uxSelectedNodeText.Multiline = true;
            this.uxSelectedNodeText.Name = "uxSelectedNodeText";
            this.uxSelectedNodeText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.uxSelectedNodeText.Size = new System.Drawing.Size(1106, 57);
            this.uxSelectedNodeText.TabIndex = 24;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1040, 558);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(73, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // logTimeLine1
            // 
            this.logTimeLine1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTimeLine1.Location = new System.Drawing.Point(7, 17);
            this.logTimeLine1.LogEntries = null;
            this.logTimeLine1.Name = "logTimeLine1";
            this.logTimeLine1.Size = new System.Drawing.Size(1105, 45);
            this.logTimeLine1.TabIndex = 17;
            // 
            // LogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1119, 606);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.logTimeLine1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.uiStatsLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LogView";
            this.Text = "Log Viewer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.LogView_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.uxSplitContainerLogView.Panel1.ResumeLayout(false);
            this.uxSplitContainerLogView.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uxSplitContainerLogView)).EndInit();
            this.uxSplitContainerLogView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button uxRetrieveLogs;
        private System.Windows.Forms.DateTimePicker uxDateStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker uxDateEnd;
        private System.Windows.Forms.DateTimePicker uxTimeStart;
        private System.Windows.Forms.DateTimePicker uxTimeEnd;
        private System.Windows.Forms.Button uxBuildFilter;
        private System.Windows.Forms.Label uiStatsLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox uxSearchText;
        private System.Windows.Forms.Button uxSearch;
        private System.Windows.Forms.OpenFileDialog uiOpenLogFile;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton uiUseEntireLog;
        private System.Windows.Forms.RadioButton uiUseCachedData;
        private System.Windows.Forms.RadioButton uiUseLogFile;
        private System.Windows.Forms.Button uxSelectLogFile;
        private System.Windows.Forms.TextBox uxPathToLogFile;
        private System.Windows.Forms.CheckBox uxCreateNewGroupingWindow;
        private LogTimeLine logTimeLine1;
        private System.Windows.Forms.RadioButton uiUselocalMemoryCache;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox uxShowOnlyWatchDogLogs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox uiServerUrl;
        private System.Windows.Forms.Button uiSendTestMessage;
        private System.Windows.Forms.Button uxLiveView;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button uiExportLog;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox uiIgnoredSystemIds;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer uxSplitContainerLogView;
        private System.Windows.Forms.TextBox uxSelectedNodeText;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView uxLogEntryReport;
        private System.Windows.Forms.ListBox ListOfAllSystemIds;
        private System.Windows.Forms.Button uiCollapseExpandeAll;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toggleLogSelectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filterOutLogOnNextRetrieveToolStripMenuItem;
    }
}

