﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dimension15.Logging.UI
{
    [Serializable]
    public class LogEntry : Dimension15.Services.Logging.Interfaces.ILogEntry
    {
        public DateTime? DateStamp
        { get; set; }

        public long Id
        { get; set; }

        public Dimension15.Services.Logging.Enumerations.LogSeverity? LogSeverity
        { get; set; }

        public int? SystemID
        { get; set; }

        public string ModuleName
        { get; set; }

        public string FunctionName
        { get; set; }

        public int? ThreadID
        { get; set; }

        public string TraceID
        { get; set; }

        public string Message
        { get; set; }

    }
}