﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace Dimension15.Settings.Registry
{
    public abstract class BaseLocalEnvironment
    {
        public static object GetSetting(string payM8SubKeyPath, string settingKey)
        {
            try
            {
                payM8SubKeyPath = payM8SubKeyPath.Trim();
                if (payM8SubKeyPath.Length > 0)
                {
                    if (!payM8SubKeyPath.StartsWith(@"\"))
                    {
                        payM8SubKeyPath = @"\" + payM8SubKeyPath;
                    }
                }

                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var key = hklm.OpenSubKey(@"SOFTWARE\Dimension15" + payM8SubKeyPath))
                {
                    return key.GetValue(settingKey);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error retrieving connection information for the operations database, see inner exception for more information", ex);
            }
        }
    }    
}
