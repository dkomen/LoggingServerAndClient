﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dimension15.Settings.Registry
{
    public class LocalEnvironmentConfiguration: BaseLocalEnvironment
    {
        public static object GetSettingFromConfigurationSubKey(string settingKey)
        {
            return GetSetting("Logging", settingKey);
        }
        /// <summary>
        /// Get the current environments connection string to the Operations database
        /// </summary>
        /// <returns></returns>
        /// <author name="Dean Komen" date="10/03/2012" />
        public static string GetDatabaseConnectionString()
        {
            try
            {
                return GetSettingFromConfigurationSubKey("DatabaseConnectionString").ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Error retrieving connection information for the operations database from the registry, see inner exception for more information", ex);
            }
        }
        /// <summary>
        /// Get the current environments PayM8 Logging Service Endpoint information
        /// </summary>
        /// <returns></returns>
        /// <author name="Dean Komen" date="10/03/2012" />
        public static string GetLoggingServiceEndPoint()
        {
            try
            {
                return "http://127.0.0.1:5050/LoggingService";// GetSettingFromConfigurationSubKey("ServiceEndpoint").ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Error retrieving the Dimension Logging Service endpoint from the registry, see inner exception for more information", ex);
            }
        }
    }
}
