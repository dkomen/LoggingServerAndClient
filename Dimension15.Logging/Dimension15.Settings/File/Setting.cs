﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Settings.File
{
    /// <summary>
    /// 
    /// </summary>
    /// <author name="Dean Komen" date="" />
    public class Setting
    {
        public Setting(string name, string value)
        {
            Name = name;
            Value = value;
        }

        #region Properties
        public string Name { get; set; }
        public string Value { get; set; }
        #endregion
    }
}
