﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Dimension15.Settings.File.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DefaultSettingValueAttribute : System.Attribute
    {
        public DefaultSettingValueAttribute(string value)
        {
            Value = value;
        }
        public string Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Does the supplied property have this attribute applied to it?
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool PropertyHasThisAttribute(PropertyInfo property)
        {
            object[] attribute = property.GetCustomAttributes(typeof(DefaultSettingValueAttribute), false);
            return (attribute.Length > 0);
        }

        public static string DefaultPropertyValue(PropertyInfo property)
        {
            if (PropertyHasThisAttribute(property))
            {
                return ((DefaultSettingValueAttribute)property.GetCustomAttributes(typeof(DefaultSettingValueAttribute), false)[0]).Value;
            }
            else
            {
                throw new System.Exception("Could not get the properties default value as the  supplied property does not have 'DefaultSettingValueAttribute' attribute applied to it");
            }
        }
    }
}
