﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Settings.File
{
    /// <summary>
    /// 
    /// </summary>
    /// <author name="Dean Komen" date="" />
    public class Manager
    {
        #region Fields
        private static Manager _managerInstance;
        private Dictionary<string, string> _settings = new Dictionary<string, string>();
        private readonly string _pathToSettingsFile = string.Empty;
        #endregion

        #region Constructors
        public Manager()
        {
            if (System.Reflection.Assembly.GetEntryAssembly() != null)
            {
                _pathToSettingsFile = System.IO.Path.Combine(System.Environment.CurrentDirectory, System.Reflection.Assembly.GetEntryAssembly().ManifestModule + ".set");
            }
            GetAllSettings();
        }
        #endregion

        #region Properties
        /// <summary>
        /// A shallow copy of the settings
        /// </summary>
        public Dictionary<string, string> Settings
        {
            get
            {
                return _settings;
            }
        }
        #endregion

        #region Public Functions
        public static Manager GetInstance()
        {
            if (_managerInstance == null)
            {
                _managerInstance = new Manager();
            }
            return _managerInstance;
        }
        public void GetAllSettings()
        {
            System.Xml.XmlDocument settingsDocument = new System.Xml.XmlDocument();
            if (System.IO.File.Exists(_pathToSettingsFile))
            {
                settingsDocument.Load(_pathToSettingsFile);
                System.Xml.XmlNodeList settingsNodes = settingsDocument.SelectNodes("settings/setting");
                if (settingsNodes != null)
                {
                    foreach (System.Xml.XmlNode setting in settingsNodes)
                    {
                        _settings.Add(setting.Attributes["key"].Value, setting.Attributes["value"].Value);
                    }
                }
            }
        }
        public void SaveAllSettings(Type typeOfObjetWithSettingsToSave)
        {
            try
            {

                System.Reflection.PropertyInfo[] propertiesToSave = typeOfObjetWithSettingsToSave.GetProperties();
                foreach(System.Reflection.PropertyInfo property in propertiesToSave)
                {
                    string value = property.GetValue(typeOfObjetWithSettingsToSave,null).ToString();
                    if(!_settings.ContainsKey(property.Name.ToLower()))
                    {
                        _settings.Add(property.Name.ToLower(), value);
                    }
                }


                System.Xml.XmlDocument settingsDocument = new System.Xml.XmlDocument();
                if (System.IO.File.Exists(_pathToSettingsFile))
                {
                    settingsDocument.Load(_pathToSettingsFile);
                }

                System.Xml.XmlNode settingsNode = settingsDocument.SelectSingleNode("settings");
                if (settingsNode == null)
                {
                    settingsNode = settingsDocument.CreateElement("settings");
                    settingsDocument.AppendChild(settingsNode);
                }


                //Go through all the settings in memory
                foreach (KeyValuePair<string, string> setting in _settings)
                {
                    bool foundSetting = false;
                    //Go though all the settings in the xml file
                    foreach (System.Xml.XmlNode xmlNode in settingsNode)
                    {
                        if (xmlNode.Attributes["key"].Value == setting.Key)//If the setting exists in xml already update its value and then exit
                        {
                            foundSetting = true;
                            xmlNode.Attributes["value"].Value = setting.Value;
                            break;
                        }
                    }
                    if (foundSetting == false)//Setting wasnt in xml file so add it
                    {
                        System.Xml.XmlNode newNode = settingsDocument.CreateElement("setting");
                        System.Xml.XmlAttribute attrKey = settingsDocument.CreateAttribute("key");
                        attrKey.Value = setting.Key.ToLower();
                        System.Xml.XmlAttribute attrValue = settingsDocument.CreateAttribute("value");
                        attrValue.Value = setting.Value;
                        newNode.Attributes.Append(attrKey);
                        newNode.Attributes.Append(attrValue);
                        settingsNode.AppendChild(newNode);
                    }
                }

                System.IO.File.Delete(_pathToSettingsFile);
                settingsDocument.Save(_pathToSettingsFile);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string GetSettingValue(string settingName)
        {
            string setting = string.Empty;
            if (_settings.ContainsKey(settingName.ToLower()))
            {
                setting = _settings[settingName.ToLower()];
            }
            return setting;
        }
        public void SetSettingValue(string settingName, string settingValue)
        {
            string setting = string.Empty;
            if (_settings.ContainsKey(settingName.ToLower()))
            {
                _settings[settingName.ToLower()] = settingValue;
            }
            else
            {
                _settings.Add(settingName.ToLower(), settingValue);
            }
        }
        /// <summary>
        /// Does the setting exist in the collection of settings?
        /// </summary>
        /// <param name="settingName"></param>
        /// <returns></returns>
        public bool SettingExists(string settingName)
        {
            return _settings.ContainsKey(settingName.ToLower());
        }
        #endregion

    }
}
