﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dimension15.Settings.File
{
    public abstract class BaseManager
    {
        #region Fields
        public static string SettingNameWebserviceServer = "WebserviceServer";
        protected static Manager _manager = Manager.GetInstance();
        #endregion

        #region Constructors
        static BaseManager()
        {

        }
        #endregion        

        #region Public Functions
        public static void Save(Type typeOfObjetWithSettingsToSave)
        {
            _manager.SaveAllSettings(typeOfObjetWithSettingsToSave);
        }
        #endregion
    }
}
